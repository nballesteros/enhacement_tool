% Function to change the folder of the original image to the current one
%...................................................................................................................
% INPUT PARAMETERS
% pathOriginalImage: path in which the original image is saved. It has to
%                    be found in 'No_Processed_Images' as the last folder in the path.
%...................................................................................................................
% OUTPUT PARAMETERS
% newPath: path with a changed root (adapted to the current computer)
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function newPath = changePath (pathOriginalImage)

% The structure of paths has to be the same for all the users.
% We can change the root path of the user that processed the image by the root path of the current user
ind_init = strfind(pathOriginalImage,'\No_Processed_Images');
newPath = [pwd pathOriginalImage(ind_init:end)];
