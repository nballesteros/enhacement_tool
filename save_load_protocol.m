function varargout = save_load_protocol(varargin)
% SAVE_LOAD_PROTOCOL MATLAB code for save_load_protocol.fig
%      SAVE_LOAD_PROTOCOL, by itself, creates a new SAVE_LOAD_PROTOCOL or raises the existing
%      singleton*.
%
%      H = SAVE_LOAD_PROTOCOL returns the handle to a new SAVE_LOAD_PROTOCOL or the handle to
%      the existing singleton*.
%
%      SAVE_LOAD_PROTOCOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SAVE_LOAD_PROTOCOL.M with the given input arguments.
%
%      SAVE_LOAD_PROTOCOL('Property','Value',...) creates a new SAVE_LOAD_PROTOCOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before save_load_protocol_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to save_load_protocol_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help save_load_protocol

% Last Modified by GUIDE v2.5 30-Sep-2016 17:32:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @save_load_protocol_OpeningFcn, ...
                   'gui_OutputFcn',  @save_load_protocol_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before save_load_protocol is made visible.
function save_load_protocol_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to save_load_protocol (see VARARGIN)

% Choose default command line output for comments
handles.output = hObject;

% Choose default command line output for save_load_protocol
action = getappdata(0,'type');

% We use the same window to save and load protocols. Several elements of
% the interface have to be enabled or disabled when we use one or other option.
switch(action)
    case('save')
        set (handles.pushbutton_saveload, 'String', 'SAVE PROTOCOL')
        set (handles.pushbutton_saveload, 'Position', [46.6 2.077 40.2 3.923])
        set (handles.pushbutton_all, 'Visible', 'off')
        set (gcf, 'Name', 'Please, select the options that define the protocol to be saved')
    case('load')
        set (handles.pushbutton_saveload, 'String', 'LOAD FILTERED PROTOCOL')
        set (handles.pushbutton_saveload, 'Position', [15.2 2.077 52.2 3.923])
        set (handles.pushbutton_all, 'Visible', 'on')
        set (gcf, 'Name', 'Please, select the options that define the protocol to be loaded')
end

% Features initialization
handles.output = hObject;
handles.animal = 0;
handles.size = 0;
handles.effect = [0 0 0];
handles.body = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes save_load_protocol wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = save_load_protocol_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure

varargout{1} = handles.output;
% Send the protocol features to the main window
setappdata (0,'animal', handles.animal);
setappdata (0,'size', handles.size);
setappdata (0,'effect', handles.effect);
setappdata (0,'body',handles.body);
setappdata (0,'exotic',get(handles.edit_exotic,'String'));
setappdata (0,'other', get(handles.edit_otherAnimal,'String'));
setappdata (0,'otherPart', get(handles.edit_body,'String'));
setappdata (0,'savedProtocol', 0);


% --- Executes on button press in radiobutton_big.
function radiobutton_big_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_big (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_big

if (get(handles.radiobutton_small, 'Value') == 1)
    set(handles.radiobutton_small, 'Value', 0)
end

if (get(handles.radiobutton_medium, 'Value') == 1)
    set(handles.radiobutton_medium, 'Value', 0)
end
handles.size = 3;

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_small.
function radiobutton_small_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_small (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_small

if (get(handles.radiobutton_big, 'Value') == 1)
    set(handles.radiobutton_big, 'Value', 0)
end

if (get(handles.radiobutton_medium, 'Value') == 1)
    set(handles.radiobutton_medium, 'Value', 0)
end
handles.size = 1;

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_medium.
function radiobutton_medium_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_medium (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_medium

if (get(handles.radiobutton_small, 'Value') == 1)
    set(handles.radiobutton_small, 'Value', 0)
end

if (get(handles.radiobutton_big, 'Value') == 1)
    set(handles.radiobutton_big, 'Value', 0)
end
handles.size = 2;


% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_other.
function radiobutton_other_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_other (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_other

if (get(handles.radiobutton_dog, 'Value') == 1)
    set(handles.radiobutton_dog, 'Value', 0)
end

if (get(handles.radiobutton_cat, 'Value') == 1)
    set(handles.radiobutton_cat, 'Value', 0)
end

if (get(handles.radiobutton_horse, 'Value') == 1)
    set(handles.radiobutton_horse, 'Value', 0)
end

if (get(handles.radiobutton_exotic, 'Value') == 1)
    set(handles.radiobutton_exotic, 'Value', 0)
    set(handles.edit_exotic, 'Enable', 'off')
end

if (get(handles.radiobutton_other, 'Value') == 0)
    set(handles.edit_otherAnimal, 'Enable', 'off')
else
    set(handles.edit_otherAnimal, 'Enable', 'on')
end
handles.animal = 5;

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_dog.
function radiobutton_dog_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_dog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_dog

if (get(handles.radiobutton_other, 'Value') == 1)
    set(handles.radiobutton_other, 'Value', 0)
end

if (get(handles.radiobutton_cat, 'Value') == 1)
    set(handles.radiobutton_cat, 'Value', 0)
end

if (get(handles.radiobutton_horse, 'Value') == 1)
    set(handles.radiobutton_horse, 'Value', 0)
end

if (get(handles.radiobutton_exotic, 'Value') == 1)
    set(handles.radiobutton_exotic, 'Value', 0)
end

set(handles.edit_otherAnimal, 'Enable', 'off')
set(handles.edit_exotic, 'Enable', 'off')

handles.animal = 1;


% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_cat.
function radiobutton_cat_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_cat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_cat

if (get(handles.radiobutton_other, 'Value') == 1)
    set(handles.radiobutton_other, 'Value', 0)
end

if (get(handles.radiobutton_dog, 'Value') == 1)
    set(handles.radiobutton_dog, 'Value', 0)
end

if (get(handles.radiobutton_horse, 'Value') == 1)
    set(handles.radiobutton_horse, 'Value', 0)
end

if (get(handles.radiobutton_exotic, 'Value') == 1)
    set(handles.radiobutton_exotic, 'Value', 0)
end

set(handles.edit_otherAnimal, 'Enable', 'off')
set(handles.edit_exotic, 'Enable', 'off')

handles.animal = 2;


% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_horse.
function radiobutton_horse_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_horse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_horse

if (get(handles.radiobutton_other, 'Value') == 1)
    set(handles.radiobutton_other, 'Value', 0)
end

if (get(handles.radiobutton_dog, 'Value') == 1)
    set(handles.radiobutton_dog, 'Value', 0)
end

if (get(handles.radiobutton_cat, 'Value') == 1)
    set(handles.radiobutton_cat, 'Value', 0)
end

if (get(handles.radiobutton_exotic, 'Value') == 1)
    set(handles.radiobutton_exotic, 'Value', 0)
end

set(handles.edit_otherAnimal, 'Enable', 'off')
set(handles.edit_exotic, 'Enable', 'off')

handles.animal = 3;


% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_exotic.
function radiobutton_exotic_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_exotic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_exotic
if (get(handles.radiobutton_dog, 'Value') == 1)
    set(handles.radiobutton_dog, 'Value', 0)
end

if (get(handles.radiobutton_cat, 'Value') == 1)
    set(handles.radiobutton_cat, 'Value', 0)
end

if (get(handles.radiobutton_horse, 'Value') == 1)
    set(handles.radiobutton_horse, 'Value', 0)
end

if (get(handles.radiobutton_other, 'Value') == 1)
    set(handles.radiobutton_other, 'Value', 0)
    set(handles.edit_otherAnimal, 'Enable', 'off')
end

if (get(handles.radiobutton_exotic, 'Value') == 0)
    set(handles.edit_exotic, 'Enable', 'off')
else
    set(handles.edit_exotic, 'Enable', 'on')
end
handles.animal = 4;


% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in pushbutton_saveload.
function pushbutton_saveload_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Send the protocol features to the main window
setappdata (0,'animal', handles.animal);
setappdata (0,'size', handles.size);
setappdata (0,'effect', handles.effect);
setappdata (0,'body',handles.body);
setappdata (0,'exotic',get(handles.edit_exotic,'String'));
setappdata (0,'other', get(handles.edit_otherAnimal,'String'));
setappdata (0,'otherPart', get(handles.edit_body,'String'));
setappdata (0,'savedProtocol', 1);
close


% --- Executes on button press in pushbutton_all.
function pushbutton_all_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% We impose a high number (for example, 100) to recognize we want all the
% protocols (no a specific one)
setappdata(0,'animal', 100);
setappdata(0,'size', 100);
setappdata(0,'effect', 100);
setappdata(0,'body',100);
close


% --- Executes on button press in radiobutton_contrast.
function radiobutton_contrast_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_contrast
if (get(handles.radiobutton_contrast, 'Value') == 1)
    handles.effect(1,3) = 1;
else
    handles.effect(1,3) = 0;
end

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_noise.
function radiobutton_noise_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_noise

if (get(handles.radiobutton_noise, 'Value') == 1)
    handles.effect(1,1) = 1;
else
    handles.effect(1,1) = 0;
end

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_definition.
function radiobutton_definition_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_definition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_definition

if (get(handles.radiobutton_definition, 'Value') == 1)
    handles.effect(1,2) = 1;
else
    handles.effect(1,2) = 0;
end

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_chest.
function radiobutton_chest_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_chest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_chest

if (get(handles.radiobutton_abdomen, 'Value') == 1)
    set(handles.radiobutton_abdomen, 'Value', 0)
end

if (get(handles.radiobutton_body, 'Value') == 1)
    set(handles.radiobutton_body, 'Value', 0)
end

if (get(handles.radiobutton_head, 'Value') == 1)
    set(handles.radiobutton_head, 'Value', 0)
end

if (get(handles.radiobutton_limbs, 'Value') == 1)
    set(handles.radiobutton_limbs, 'Value', 0)
end

if (get(handles.radiobutton_spine, 'Value') == 1)
    set(handles.radiobutton_spine, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'off')
handles.body = 1;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_abdomen.
function radiobutton_abdomen_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_abdomen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_abdomen

if (get(handles.radiobutton_chest, 'Value') == 1)
    set(handles.radiobutton_chest, 'Value', 0)
end

if (get(handles.radiobutton_body, 'Value') == 1)
    set(handles.radiobutton_body, 'Value', 0)
end

if (get(handles.radiobutton_head, 'Value') == 1)
    set(handles.radiobutton_head, 'Value', 0)
end

if (get(handles.radiobutton_limbs, 'Value') == 1)
    set(handles.radiobutton_limbs, 'Value', 0)
end

if (get(handles.radiobutton_spine, 'Value') == 1)
    set(handles.radiobutton_spine, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'off')
handles.body = 2;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_head.
function radiobutton_head_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_head (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_head
if (get(handles.radiobutton_chest, 'Value') == 1)
    set(handles.radiobutton_chest, 'Value', 0)
end

if (get(handles.radiobutton_body, 'Value') == 1)
    set(handles.radiobutton_body, 'Value', 0)
end

if (get(handles.radiobutton_abdomen, 'Value') == 1)
    set(handles.radiobutton_abdomen, 'Value', 0)
end

if (get(handles.radiobutton_limbs, 'Value') == 1)
    set(handles.radiobutton_limbs, 'Value', 0)
end

if (get(handles.radiobutton_spine, 'Value') == 1)
    set(handles.radiobutton_spine, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'off')
handles.body = 3;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_limbs.
function radiobutton_limbs_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_limbs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_limbs
if (get(handles.radiobutton_chest, 'Value') == 1)
    set(handles.radiobutton_chest, 'Value', 0)
end

if (get(handles.radiobutton_body, 'Value') == 1)
    set(handles.radiobutton_body, 'Value', 0)
end

if (get(handles.radiobutton_abdomen, 'Value') == 1)
    set(handles.radiobutton_abdomen, 'Value', 0)
end

if (get(handles.radiobutton_head, 'Value') == 1)
    set(handles.radiobutton_head, 'Value', 0)
end

if (get(handles.radiobutton_spine, 'Value') == 1)
    set(handles.radiobutton_spine, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'off')
handles.body = 4;

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton_spine.
function radiobutton_spine_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_spine (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_spine

if (get(handles.radiobutton_chest, 'Value') == 1)
    set(handles.radiobutton_chest, 'Value', 0)
end

if (get(handles.radiobutton_body, 'Value') == 1)
    set(handles.radiobutton_body, 'Value', 0)
end

if (get(handles.radiobutton_abdomen, 'Value') == 1)
    set(handles.radiobutton_abdomen, 'Value', 0)
end

if (get(handles.radiobutton_head, 'Value') == 1)
    set(handles.radiobutton_head, 'Value', 0)
end

if (get(handles.radiobutton_limbs, 'Value') == 1)
    set(handles.radiobutton_limbs, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'off')
handles.body = 5;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton_body.
function radiobutton_body_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton_body (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton_body
if (get(handles.radiobutton_chest, 'Value') == 1)
    set(handles.radiobutton_chest, 'Value', 0)
end

if (get(handles.radiobutton_abdomen, 'Value') == 1)
    set(handles.radiobutton_abdomen, 'Value', 0)
end

if (get(handles.radiobutton_head, 'Value') == 1)
    set(handles.radiobutton_head, 'Value', 0)
end

if (get(handles.radiobutton_limbs, 'Value') == 1)
    set(handles.radiobutton_limbs, 'Value', 0)
end

if (get(handles.radiobutton_spine, 'Value') == 1)
    set(handles.radiobutton_spine, 'Value', 0)
end

set(handles.edit_body, 'Enable', 'on')
handles.body = 6;

% Update handles structure
guidata(handles.figure1,handles);
