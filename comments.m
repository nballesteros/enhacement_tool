function varargout = comments(varargin)
% COMMENTS MATLAB code for comments.fig
%      COMMENTS, by itself, creates a new COMMENTS or raises the existing
%      singleton*.
%
%      H = COMMENTS returns the handle to a new COMMENTS or the handle to
%      the existing singleton*.
%
%      COMMENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMMENTS.M with the given input arguments.
%
%      COMMENTS('Property','Value',...) creates a new COMMENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before comments_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to comments_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help comments

% Last Modified by GUIDE v2.5 29-Sep-2016 15:43:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @comments_OpeningFcn, ...
                   'gui_OutputFcn',  @comments_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before comments is made visible.
function comments_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to comments (see VARARGIN)

% Choose default command line output for comments
handles.output = hObject;

% First, all the options are unselected
handles.initImage = [0 0 0];
handles.improvement = [0 0 0 0];
handles.author = [0 0 0];
handles.checkProt = 0;

% Function to pass the comments to the main gui
setappdata(0,'HandlesCommentsGUI',hObject);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes comments wait for user response (see UIRESUME)
% uiwait(handles.figure2);


% --- Outputs from this function are returned to the command line.
function varargout = comments_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% functions to pass the comments to the main gui

% Get default command line output from handles structure
varargout{1} = handles.output;

setappdata(0,'initImage',handles.initImage)
setappdata(0,'improvement',handles.improvement)
setappdata(0,'comments',get (handles.edit_comments, 'String'))
setappdata(0, 'author', handles.author)
setappdata(0,'checkProtocol', handles.checkProt);

% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB

%First, we check if there is an incorrect field
noMark = [0 0 0];
noMark2 = [0 0 0 0];

if isequal(handles.author,noMark)
    msgbox('You have to select the author of the processing', 'Information')
    return
end

if isequal(handles.initImage,noMark)
    msgbox('You have to select the quality of the initial image', 'Information')
    return
end

if isequal(handles.improvement,noMark2)
    msgbox('You have to select the quality improvement after the processing', 'Information')
    return
end

% Functions to send the comments to the main gui
setappdata(0,'initImage',handles.initImage)
setappdata(0,'improvement',handles.improvement)
setappdata(0,'comments',get (handles.edit_comments, 'String'))
setappdata(0, 'author', handles.author)
setappdata(0,'checkProtocol', handles.checkProt);

close

% --- Executes on button press in isabel.
function isabel_Callback(hObject, eventdata, handles)
% hObject    handle to isabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of isabel

isabelOp = handles.author(1);

if isabelOp == 0
    handles.author= [1 0 0];
    set(handles.isabel, 'value', 1)
    set(handles.alicia, 'value', 0)
    set(handles.jesus, 'value', 0)
else
    handles.author= [0 0 0];
    set(handles.isabel, 'value', 0)
    set(handles.alicia, 'value', 0)
    set(handles.jesus, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in alicia.
function alicia_Callback(hObject, eventdata, handles)
% hObject    handle to alicia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of alicia

aliciaOp = handles.author(2);

if aliciaOp == 0
    handles.author= [0 1 0];
    set(handles.isabel, 'value', 0)
    set(handles.alicia, 'value', 1)
    set(handles.jesus, 'value', 0)
else
    handles.author= [0 0 0];
    set(handles.isabel, 'value', 0)
    set(handles.alicia, 'value', 0)
    set(handles.jesus, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in jesus.
function jesus_Callback(hObject, eventdata, handles)
% hObject    handle to jesus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of jesus

jesusOp = handles.author(3);

if jesusOp == 0
    handles.author= [0 0 1];
    set(handles.isabel, 'value', 0)
    set(handles.alicia, 'value', 0)
    set(handles.jesus, 'value', 1)
else
    handles.author= [0 0 0];
    set(handles.isabel, 'value', 0)
    set(handles.alicia, 'value', 0)
    set(handles.jesus, 'value', 0)
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in bad.
function bad_Callback(hObject, eventdata, handles)
% hObject    handle to bad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bad
badOp = handles.initImage(1);

if badOp == 0
    handles.initImage= [1 0 0];
    set(handles.bad, 'value', 1)
    set(handles.normal, 'value', 0)
    set(handles.excelent, 'value', 0)
else
    handles.initImage= [0 0 0];
    set(handles.bad, 'value', 0)
    set(handles.normal, 'value', 0)
    set(handles.excelent, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in normal.
function normal_Callback(hObject, eventdata, handles)
% hObject    handle to normal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of normal
normalOp = handles.initImage(2);

if normalOp == 0
    handles.initImage= [0 1 0];
    set(handles.bad, 'value', 0)
    set(handles.normal, 'value', 1)
    set(handles.excelent, 'value', 0)
else
    handles.initImage= [0 0 0];
    set(handles.bad, 'value', 0)
    set(handles.normal, 'value', 0)
    set(handles.excelent, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in excelent.
function excelent_Callback(hObject, eventdata, handles)
% hObject    handle to excelent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of excelent
excelentOp = handles.initImage(3);

if excelentOp == 0
    handles.initImage= [0 0 1];
    set(handles.bad, 'value', 0)
    set(handles.normal, 'value', 0)
    set(handles.excelent, 'value', 1)
else
    handles.initImage= [0 0 0];
    set(handles.bad, 'value', 0)
    set(handles.normal, 'value', 0)
    set(handles.excelent, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in nothing.
function nothing_Callback(hObject, eventdata, handles)
% hObject    handle to nothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of nothing
nothingOp = handles.improvement(1);

if nothingOp == 0
    handles.improvement= [1 0 0 0];
    set(handles.nothing, 'value', 1)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
else
    handles.improvement= [0 0 0 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
end


% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in irrelevant.
function irrelevant_Callback(hObject, eventdata, handles)
% hObject    handle to irrelevant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of irrelevant
irrelevantOp = handles.improvement(2);

if irrelevantOp == 0
    handles.improvement= [0 1 0 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 1)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
else
    handles.improvement= [0 0 0 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in substantial.
function substantial_Callback(hObject, eventdata, handles)
% hObject    handle to substantial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of substantial
substantialOp = handles.improvement(3);

if substantialOp == 0
    handles.improvement= [0 0 1 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 1)
    set(handles.clinical, 'value', 0)
else
    handles.improvement= [0 0 0 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in clinical.
function clinical_Callback(hObject, eventdata, handles)
% hObject    handle to clinical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of clinical
clinicalOp = handles.improvement(4);

if clinicalOp == 0
    handles.improvement= [0 0 0 1];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 1)
else
    handles.improvement= [0 0 0 0];
    set(handles.nothing, 'value', 0)
    set(handles.irrelevant, 'value', 0)
    set(handles.substantial, 'value', 0)
    set(handles.clinical, 'value', 0)
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox_protocol.
function checkbox_protocol_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_protocol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_protocol

mycheck = get (handles.checkbox_protocol, 'Value');
if mycheck == 1
    handles.checkProt = 1;
else
    handles.checkProt = 0;
end

% Update handles structure
guidata(hObject, handles);
