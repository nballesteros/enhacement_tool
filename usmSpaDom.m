% Algorithm to enhance the edges and details of images using the unsharp masking (USM) algorithm
% in the spatial domain
%..................................................................................................................
% INPUT PARAMETERS
% image: image to be enhanced
% radius: of the image's neighborhood where the filter is applied
% enhancement_factor: to control the level of enhancement (range 0-2, greater values provokes 
%                     undesirable effects in the output image)
%...................................................................................................................
% OUTPUT PARAMETERS
% enhancedImage: enhanced image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function enhancedImage = usmSpaDom(image, radius, enhancement_factor)

if enhancement_factor == 0 % We don't apply any filter if the user has set to 0 the factor
    enhancedImage = image;
else
    enhancedImage = usmFilter (image, radius, 2*enhancement_factor);
end

