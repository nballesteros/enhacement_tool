% Wavelet decomposition to denoise the image
%...................................................................................................................
% INPUT PARAMETERS
% image: to be processed
% level: of wavelet decomposition (default 2)
% wname: type of wavelet used to perform the decomposition ('dmey')
% factor: controls the level of denoising (it is related with the decomposition level)
% factorTh: controls the threshold value (default 'automatic th. from VS level-dependent algorithm')
%...................................................................................................................
% OUTPUT PARAMETERS
% c_denoised, s: wavelet coefficients without noise
% denoisedImage: image without noise
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [c_denoised, s, denoisedImage] = denoiseWavelet (image, level, wname, factor, factorTh)

if factor == 0
    % If the user has selected a factor of 0, he doesn't want the image to be denoised so we don't have to apply any processing
    c_denoised = [];
    s = [];
    denoisedImage = image;
else
    % Perform a wavelet decomposition of the image
    % at a specific level using the wavelet with 'wname'.
    [c,s] = wavedec2(image,level,wname);

   % Automatic threshold based on the algorithm level-dependent VisuShrink
   % with noise estimation based on the first level of decomposition (noisiest coefficients)
   % The factorTh controls this threshold by reducing (less noise
   % reduction) or rising it (more noise reduction but also blurring).
   threshold = thVSLvd (c,s,level, factorTh);
   
   % Function to apply the threshold (level dependent, soft)
   [denoisedImage,c_denoised, s_denoised, a, b] = wdencmp('lvd',c,s,wname,level,threshold,'s');
   denoisedImage = uint16(denoisedImage);   
  
end
