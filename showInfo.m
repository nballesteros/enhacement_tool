% Function to show the DICOM information in a new window
%...................................................................................................................
% INPUT PARAMETERS
% info:  is a cellarray with strings, numbers and structs. We have to convert
% it in a cellarray of only string.
% The first column is composed by all the fields of the DICOM info so we
% don't have to modify anything. However, the second one is a mix of types of
% data so we have to unify them
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function infoDICOM = showInfo (info)

% Size of the data
sizeCell = size(info,1);

% We first convert all data into strings and we analyze how many structs there are in the cell array
count = 0;
numberStructs = 0;
for i = 1:sizeCell
    if ~isstr(info{i,2})&& ~ischar(info{i,2})
        if ~isstruct(info{i,2})
            info{i,2} = (info{i,2})'; %Transpose in case of column vector
            info{i,2} = num2str(info{i,2});         
        else
            count = count+size(struct2cell(info{i,2}),1);% We count the number of positions we'll need later (n�structs)
            numberStructs = numberStructs + 1;
        end
    end
end

infoDICOM = []; 
% We join the field's name and the content in the same cell:
index = 1;
for i = 1:sizeCell
    if (~isstruct(info{i,2}))
        a = ['  - ' info{i,1} ': ' info{i,2}];
        infoDICOM {index+1,1} = a;
        index = index+1;
    end
end

% Now, we have to create other array in case of having structs into the cell array
newCell = [];

for i = 1:sizeCell
    if (isstruct(info{i,2}))
        myStruct = info{i,2};
        namesStruct = fieldnames(myStruct);
        valuesStruct = struct2cell (myStruct);
        if(size(valuesStruct,1)>0)
            if isstruct(valuesStruct{1,1}) % Other level of structs
                myNewStruct = valuesStruct{1,1};
                namesNewStruct = fieldnames(myNewStruct);
                valuesNewStruct = struct2cell(myNewStruct);
                intermediateCell = cell(size(valuesNewStruct, 1),1);
                for m = 1:size(valuesNewStruct, 1)
                     if ~isstr( valuesNewStruct{m,1})&& ~ischar( valuesNewStruct{m,1})
                         valuesNewStruct{m,1} =  valuesNewStruct{m,1}'; % Transpose in case of column vector
                         valuesNewStruct{m,1} = num2str(valuesNewStruct{m,1});  
                     end
                     a = ['  - ' info{i,1} ' --> ' namesNewStruct{m,1} ': ' valuesNewStruct{m,1}];
                     intermediateCell {m,1} = a;
                end
            else 
                 intermediateCell = cell(size(valuesStruct, 1),1);
                 for k = 1:size(valuesStruct, 1)
                     b = ['  - ' info{i,1} ' --> ' namesStruct{k,1} ': ' valuesStruct{k,1}];
                     intermediateCell {k,1} = b;
                 end
            end
            newCell = [newCell ; intermediateCell];
        end
    end
end
infoDICOM = [infoDICOM; newCell; ' '];
