% Auxiliar function to apply an USM to an image
%...................................................................................................................
% INPUT PARAMETERS
% image: to be enhanced
% radius: of the gaussian filter used in this algorithm
% factor: enhancement factor that will be applied to the high frequency components (range 0-2, greater values provokes 
%         undesirable effects in the output image)
%...................................................................................................................
% OUTPUT PARAMETERS
% enhancedImage: image with edges enhancement
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function enhancedImage = usmFilter(image, radius, factor)

% USM is a filter in which we sum the original image and a high_pass
% filtered version of it. The result will be an image in which we will have
% enhanced the edges and details.
filtRadius = ceil(radius*2); % 2 Standard deviations include >95% of the area. 
filtSize = 2*filtRadius + 1;
filter = fspecial('gaussian',[filtSize filtSize],radius);
lpImage = imfilter(image, filter); % low frequency image
hpImage = image - lpImage; % high frequency image is the difference

enhancedImage = image + factor*hpImage; % resulting image
