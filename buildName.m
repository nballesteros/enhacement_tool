% Function to construct the name of the protocol according to the options selected by the user
%...................................................................................................................
% INPUT PARAMETERS
% animal, sizeAnimal, effect, body, exoticAnimal, otherAnimal, otherPart: results from the interface's buttons and blanks
% (they are coded in numbers so the function converts them to strings)
% savedProtocol: 0 loading protocol, 1 saving protocol
%...................................................................................................................
% OUTPUT PARAMETERS
% protocolName: automatic name created according to the options the user has selected.
% nAnimal, nSize, nBody, nEffect: protocol's parameters separately (strings). They will help us to filtrate the protocols based on these categories. 
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [protocolName, nAnimal, nSize, nBody, nEffect] = buildName (animal, sizeAnimal, effect, body, exoticAnimal, otherAnimal, otherPart, savedProtocol)

% Type of animal
if (animal == 100) % When button all is pressed
    protocolName = 'all'; %When we want to visualize all the protocols
    nAnimal = '';
    nSize = '';
    nBody = '';
    nEffect = '';
else
    switch(animal)
        case(1)
            nameAnimal = 'dog_';
            nAnimal = 'dog';
        case(2)
            nameAnimal = 'cat_';
            nAnimal = 'cat';
        case (3)
            nameAnimal = 'horse_';
            nAnimal = 'horse';
        case (4)
            if isempty(exoticAnimal)
                nameAnimal = 'exotic_';
                nAnimal = 'exotic';
            else
                nameAnimal = [exoticAnimal '_'];
                nAnimal = 'exoticAnimal';
            end
        case(5)
            if isempty(otherAnimal)
                nameAnimal = 'other_';
                nAnimal = 'other';
            else
                nameAnimal = [otherAnimal '_'];
                nAnimal = 'otherAnimal';
            end
        otherwise
            nameAnimal = '';
            nAnimal = '';
    end

% Size of the animal
    switch(sizeAnimal)
        case(1)
            nameSize = 'small_';
            nSize = 'small';
        case(2)
            nameSize = 'medium_';
            nSize = 'medium';
        case(3)
            nameSize = 'big_';
            nSize = 'big';
        otherwise
            nameSize = '';
            nSize = '';
    end

% Type of undesired effect on the image
    if (isequal(effect,[1 0 0]))
        nameEffect = 'Noise';
    elseif (isequal(effect,[0 1 0]))
        nameEffect = 'Definition';
    elseif (isequal(effect,[0 0 1]))
        nameEffect = 'Contrast';
    elseif (isequal(effect,[1 1 0]))
        nameEffect = 'NoiseDefinition';
    elseif (isequal(effect,[1 0 1]))
        nameEffect = 'NoiseContrast';
    elseif (isequal(effect,[0 1 1]))
        nameEffect = 'DefinitionContrast';
    elseif (isequal(effect,[1 1 1]))
        nameEffect = 'NoiseDefinitionContrast';
    else
        nameEffect = '';
    end
    nEffect = nameEffect;
   
 % Part of the body animal
    switch(body)
        case(1)
            nameBody = 'chest_';
            nBody = 'chest';
        case(2)
            nameBody = 'abdomen_';
            nBody = 'abdomen';
        case(3)
            nameBody = 'head_';
            nBody = 'head';
        case(4)
            nameBody = 'limbs_';
            nBody = 'limbs';
        case(5)
            nameBody = 'spine_';
            nBody = 'spine';
        case(6)
            if isempty(otherPart)
                nameBody = 'otherPart_';
                nBody = 'otherPart';
            else
                nameBody = [otherPart '_'];
                nBody = otherPart;
            end
        otherwise
            nameBody = '';
            nBody = '';
    end
    
      if (animal == 0)&&(sizeAnimal==0)&&(isequal(effect,[0 0 0]))&&(body == 0)&&(savedProtocol == 0)
            protocolName = ''; % When the user wants to load a protocol but he has not selected any field,
                               % all the options will be available
      elseif (animal == 0)&&(sizeAnimal==0)&&(isequal(effect,[0 0 0]))&&(body == 0)&&(savedProtocol == 1)
            protocolName = 'noFieldsSelected'; % When the user wants to save the protocol but he has not selected any field
                                               % a message will appear asking for the selection of the protocol features
      else
            protocolName = [nameAnimal nameSize nameBody nameEffect]; % Regular case
      end
    
end
    
    
  