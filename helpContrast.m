function varargout = helpContrast(varargin)
% HELPCONTRAST MATLAB code for helpContrast.fig
%      HELPCONTRAST, by itself, creates a new HELPCONTRAST or raises the existing
%      singleton*.
%
%      H = HELPCONTRAST returns the handle to a new HELPCONTRAST or the handle to
%      the existing singleton*.
%
%      HELPCONTRAST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HELPCONTRAST.M with the given input arguments.
%
%      HELPCONTRAST('Property','Value',...) creates a new HELPCONTRAST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before helpContrast_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to helpContrast_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help helpContrast

% Last Modified by GUIDE v2.5 27-Sep-2016 12:15:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @helpContrast_OpeningFcn, ...
                   'gui_OutputFcn',  @helpContrast_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before helpContrast is made visible.
function helpContrast_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to helpContrast (see VARARGIN)

% Choose default command line output for helpContrast
handles.output = hObject;

% Default percentage 25%
radiobutton25_Callback(hObject, eventdata, handles)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes helpContrast wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = helpContrast_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure

varargout{1} = handles.output;


% --- Executes on button press in radiobutton25.
function radiobutton25_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton25

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)
end

showImages(handles)

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton50.
function radiobutton50_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton50 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton50
if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)
end

showImages(handles)

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton100.
function radiobutton100_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton100
if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

showImages(handles)

% Update handles structure
guidata(handles.figure1,handles);

function showImages(handles)

% Disable the option of closing the window until showing all the images
set(gcf, 'closerequestfcn', '');

% Linking the axes in order to be able to apply the zoom tool at once
h(1)=handles.axes1;
h(2)=handles.axes2;
h(4)=handles.axes4;
h(5)=handles.axes5;
h(6)=handles.axes6;
linkaxes (h, 'xy')

if (get(handles.radiobutton25, 'Value') == 1)

    %Load the images
    GEq = dicomread ([pwd '\Example_images\contrastGE25']);
    text2 = 'Contrast improved image with General Equalization (25%)';
    GaB = dicomread ([pwd '\Example_images\contrastGammaB25']); 
    text4 = 'Contrast improved image with Brighten Gamma Correction (25%)';
    GaD = dicomread ([pwd '\Example_images\contrastGammaD25']);
    text5 = 'Contrast improved image with Darken Gamma Correction (25%)';
    SHE = dicomread ([pwd '\Example_images\contrastSH25']);
    text6 = 'Contrast improved image with Subhistogram Correction (50% dense image/ 25% soft image)';
    
elseif (get(handles.radiobutton50, 'Value') == 1)

    GEq = dicomread ([pwd '\Example_images\contrastGE50']); 
    text2 = 'Contrast improved image with General Equalization (50%)';
    GaB = dicomread ([pwd '\Example_images\contrastGammaB50']);
    text4 = 'Contrast improved image with Brighten Gamma Correction (50%)';
    GaD = dicomread ([pwd '\Example_images\contrastGammaD50']); 
    text5 = 'Contrast improved image with Darken Gamma Correction (50%)';
    SHE = dicomread ([pwd '\Example_images\contrastSH50']); 
    text6 = 'Contrast improved image with Subhistogram Correction (50% dense image/ 50% soft image)';
    
elseif (get(handles.radiobutton100, 'Value') == 1)

    GEq = dicomread ([pwd '\Example_images\contrastGE100']); 
    text2 = 'Contrast improved image with General Equalization (100%)';
    GaB = dicomread ([pwd '\Example_images\contrastGammaB100']); 
    text4 = 'Contrast improved image with Brighten Gamma Correction(100%)';
    GaD = dicomread ([pwd '\Example_images\contrastGammaD100']); 
    text5 = 'Contrast improved image with Darken Gamma Correction(100%)';
    SHE = dicomread ([pwd '\Example_images\contrastSH100']);
    text6 = 'Contrast improved image with Subhistogram Correction (50% dense image/ 100% soft image)';
end
    % We show the images scaled in a factor of 0.2 to speed up the code
    axes(handles.axes1)
    imshow(imresize(dicomread([pwd '\Example_images\contrastOriginal']),0.2),[]);
    set(handles.text1, 'String', 'Original Image')

    axes(handles.axes4)
    imshow(imresize(GaB,0.2),[])
    set(handles.text4, 'String', text4)

    axes(handles.axes5)
    imshow(imresize(GaD,0.2),[])
    set(handles.text5, 'String', text5)

    axes(handles.axes2)
    imshow(imresize(GEq,0.2),[])
    set(handles.text2, 'String', text2)

    axes(handles.axes6)
    imshow(imresize(SHE,0.2),[])
    set(handles.text6, 'String', text6)

% Enable the option of closing the window 
set(gcf, 'closerequestfcn', 'closereq');
