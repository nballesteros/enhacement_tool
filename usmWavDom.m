% Algorithm to enhance the edges and details of images using the unsharp masking (USM) algorithm
% in the wavelet domain
%...................................................................................................................
% INPUT PARAMETERS
% c_denoised, s_denoised: wavelet coefficients without noise
% image: image to be enhanced (necessary in case we have not decomposed the image yet).
% radius: of the image's neighborhood where the filter is applied
% lf_enhancement_factor: to enhance the approximation wavelet coefficients(valid values 0 to 2) with an USM filter
% level: of wavelet decomposition (default 2)
% wname: name of the wavelet we are going to use (default 'ndmey')
%...................................................................................................................
% OUTPUT PARAMETERS
% enhancedImage: enhanced image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function enhancedImage = usmWavDom(c_denoised, s_denoised, image, radius, lf_enhancement_factor, level, wname)

    if lf_enhancement_factor == 0
        enhancedImage = image; % If the user has selected a factor of 0, he doesn't want to enhance the image so we don't have to apply any processing
    else

        if isempty(c_denoised)
            [c_denoised, s_denoised] = wavedec2(image,level,wname); %Wavelet decomposition (only if we have not performed it in the previous stage).
        end

            % High frequency (details) coefficients
            coes_hf = [];
            for k = 1:level 
                det = detcoef2('compact',c_denoised,s_denoised,k);
                coes_hf = [det coes_hf];  
            end

            % Approximation coefficients 
            coes_lf = appcoef2(c_denoised, s_denoised, wname, level); 

            % We enhance the edge and detail information of the low frequency
            % coefficients with an USM filter
            c_lf_improved = usmFilter (coes_lf, radius, 2*lf_enhancement_factor);

            % We convert the matrix into a row 
            tam_vector2 = s_denoised(1,1)*s_denoised(1,2);
            c_lf_improved = reshape(c_lf_improved, 1, tam_vector2); 

            % We join all the coefficients into the same vector
            c_improved = [c_lf_improved coes_hf];

            % Inverse wavelet transform and data conversion
            enhancedImage = uint16(waverec2(c_improved, s_denoised, wname));
    end
end

