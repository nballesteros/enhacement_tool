% VisuShrink threshold level-dependent. The noise estimation is done in the
% first decomposition level (in the diagonal detail sub-band).
%...................................................................................................................
% INPUT PARAMETERS
% c,s: wavelet coefficients as well as their sizes
% level: of wavelet decomposition
% factorTh: parameter that controls the automatic threshold
%...................................................................................................................
% OUTPUT PARAMETERS
% threshold: to denoise the image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function threshold = thVSLvd (c,s,level,factorTh)
threshold = ones(3,level);
detCoef = ['h','d','v']; % Detail coefficients (high frequency)

det = detcoef2('d',c,s,1);
stdNoise = median(abs(det(det~=0)))/0.6745; %Noise estimation (done in the diagonal coefficients)

for k = 1:level 
    for d = 1:3 % For the three types of high frequency coefficients
        detK = detcoef2(detCoef(d),c,s,k); %Coefficients of each subband
        threshold(d,k) = stdNoise*sqrt(2*log(numel(detK))); %Threshold for each sub-band
        
        % If we are below the optimal threshold (from 0 to the optimal value)
        if factorTh <= 0.25
            newFact = factorTh*4;
            threshold(d,k) = newFact*threshold(d,k);
        else % If we are above the optimal threshold (from 1 to 10)
            newFact = 12*(factorTh-0.25)+1;
            threshold(d,k) = newFact*threshold(d,k);
        end
    
    end
end

    