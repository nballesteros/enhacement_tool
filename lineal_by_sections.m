% close all
% clear all
% clc
% 
% % Select a DICOM image
% [filename,pathname]=uigetfile('\*.*','Select a DICOM image to be processed', [pwd '\No_Processed_Images\'], 'MultiSelect', 'off');
% if filename == 0
%     return
% end
% path = fullfile(pathname, filename);
% image = dicomread(path);

function equalizedImage = lineal_by_sections (image, low_in, window, level)

% imageDicom = dicomread(path);
% lowDicom = min(imageDicom(:));
% image = imageDicom - lowDicom;

[x,y]=size(image);
% Look Up Table --> LUT lineal por trozos
high_in = double(max(image(:)));


% Dense image
windowDense = round(0.3*(high_in-low_in));
levelDense = round(0.85*(high_in-low_in) + low_in);
indDense1 = round(levelDense - 0.5*windowDense);
indDense2 = round(levelDense + 0.5*windowDense);
pteDense = 2;

if low_in > indDense1
    indDense1 = low_in;
end

if high_in < indDense2
    indDense2 = high_in;
end

LUT1 = (0:indDense1);
LUT2 = pteDense*(indDense1+1:indDense2);
diff = min(LUT2) - max(LUT1);
LUT2 = LUT2 - diff;
LUT3 = indDense2+1:high_in;
diff2 = min(LUT3) - max(LUT2);
LUT3 = LUT3 - diff2;
LUT_final = [LUT1 LUT2 LUT3];
LUT_norm = LUT_final/max(LUT_final);
LUT = LUT_norm * high_in;

% We apply the LUT
equalizedDense = uint16(reshape(LUT(image(:)+1), [x,y]));

figure
plot(LUT)
title ('LUT dense')

% Soft image
windowSoft = round(0.1*(high_in-low_in));
levelSoft = round(0.05*(high_in-low_in) + low_in);
indSoft1 = round(levelSoft - 0.5*windowSoft);
indSoft2 = round(levelSoft + 0.5*windowSoft);
pteSoft = 2;

if low_in > indSoft1
    indSoft1 = low_in;
end

if high_in < indSoft2
    indSoft2 = high_in;
end
LUT1 = (0:indSoft1);
LUT2 = pteSoft*(indSoft1+1:indSoft2);
diff = min(LUT2) - max(LUT1);
LUT2 = LUT2 - diff;
LUT3 = indSoft2+1:high_in;
diff2 = min(LUT3) - max(LUT2);
LUT3 = LUT3 - diff2;
LUT_final = [LUT1 LUT2 LUT3];
LUT_norm = LUT_final/max(LUT_final);
LUT = LUT_norm * high_in;

% We apply the LUT
equalizedSoft = uint16(reshape(LUT(image(:)+1), [x,y]));
lastEntropy = 0;

for i=0.05:0.05:1
    eqImage = i*equalizedDense+(1-i)*equalizedSoft;
%     maxIm = max(eqImage(:));
%     eqImageN = double(eqImage)/double(maxIm);
    if entropy(eqImage) > lastEntropy
        lastEntropy = entropy(eqImage)
        equalizedImage = eqImage;
        factor = i;
    end
end
% equalizedImage = equalizedImage + lowDicom;
figure
plot(LUT)
title ('LUT soft')

figure
subplot(2,2,1)
imshow(image,[])
subplot(2,2,2)
imshow(equalizedDense,[])
subplot(2,2,3)
imshow(equalizedSoft,[])
subplot(2,2,4)
imshow(equalizedImage,[])
