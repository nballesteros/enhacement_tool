% Auxiliar function to equalize histograms with bins clipping to control the contrast. 
% Values lower than low_in and greater than high_in are not equalized (linear LUT applied)
% Equalization is performed in the range [low_in high_in]
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be equalized
% clip: parameter that controls the contrast (between 0 and 1). Values
%       above the threshold are clipped and distributed between the rest of the bins
% low_in: minimum grey value of the range where we are going to apply the equalization
% high_in: minimum grey value of the range where we are going to apply the equalization
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: contrast improved image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = imEqualization(image, clip, low_in, high_in)

% Image information
[x,y] = size(image);
imageCorr = image;
low_range = min(image(:));
high_range = max(image(:));

% LUT lineal because levels lower than low_in should be maintained
if low_in > low_range
    LUT1 = low_range:low_in-1;
else
    LUT1 = [];
end

% Calculate the clipped histogram
hgram = histc(image(:),low_in:high_in);
clippedHist = clipBins(hgram,clip, low_in);

% LUT to equalize the histogram
cdf = cumsum(clippedHist);
cdf = cdf-cdf(1);
cdf = cdf./cdf(end);
max_pixel = double(high_in-low_in);
LUT2 = round(max_pixel.*cdf)+low_in;

%LUT lineal because levels lowers than low_in should be maintained
if high_in < high_range
    LUT3 = high_in+1:high_range;
else
    LUT3 = [];
end

% Resulting LUT
LUT = [LUT1 LUT2' LUT3];
LUT = LUT';

% Equalized image
equalizedImage = uint16(reshape(LUT(imageCorr(:)-min(LUT)+1), [x,y]));