function varargout = zoomImage(varargin)
% ZOOMIMAGE MATLAB code for zoomImage.fig
%      ZOOMIMAGE, by itself, creates a new ZOOMIMAGE or raises the existing
%      singleton*.
%
%      H = ZOOMIMAGE returns the handle to a new ZOOMIMAGE or the handle to
%      the existing singleton*.
%
%      ZOOMIMAGE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZOOMIMAGE.M with the given input arguments.
%
%      ZOOMIMAGE('Property','Value',...) creates a new ZOOMIMAGE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before zoomImage_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to zoomImage_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help zoomImage

% Last Modified by GUIDE v2.5 25-Jul-2017 15:28:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @zoomImage_OpeningFcn, ...
                   'gui_OutputFcn',  @zoomImage_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before zoomImage is made visible.
function zoomImage_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to zoomImage (see VARARGIN)

% Choose default command line output for zoomImage
handles.output = hObject;
handles.image = uint16(getappdata(0,'image'));

% Zoom in
zoomin = imread('zoomin.jpg');
zoominSmall= zoomin(1:10.2:end,1:9:end,:);
set(handles.pushbutton_zoomIn,'CData',zoominSmall)

% Zoom out
zoomout = imread('zoomout.jpg');
zoomoutSmall= zoomout(1:10.2:end,1:9:end,:);
set(handles.pushbutton_zoomOut,'CData',zoomoutSmall)

% No zoom
zoomno = imread('zoomno.jpg');
zoomnoSmall= zoomno(1:10.2:end,1:9:end,:);
set(handles.pushbutton_noZoom,'CData',zoomnoSmall)

% Show the new processed image            
axes(handles.axes1);
%[bins,~] = imhist(handles.image,2^16);
[minIm, maxIm] = imageRange(handles.image);  
handles.minIm = minIm;
handles.maxIm = maxIm;
imshow(handles.image,[minIm maxIm])

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes zoomImage wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = zoomImage_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function pushbutton_zoomIn_Callback(~, ~, handles)
% hObject    handle to pushbutton_zoomIn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(1.1)
pan on

% Update handles structure
guidata(handles.figure1, handles);

% --- Executes on button press in pushbutton_zoomOut.
function pushbutton_zoomOut_Callback(~, ~, handles)
% hObject    handle to pushbutton_zoomOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(1/1.1)
zoom off

% Update handles structure
guidata(handles.figure1, handles);

% --- Executes on button press in pushbutton_noZoom.
function pushbutton_noZoom_Callback(~, ~, handles)
% hObject    handle to pushbutton_noZoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pan off
imshow(handles.image,[handles.minIm handles.maxIm])

% Update handles structure
guidata(handles.figure1, handles);
