% Algorithm to enhance the edges and details of images using morphological
% processing in the wavelet domain
%...................................................................................................................
% INPUT PARAMETERS
% c_denoised, s_denoised: wavelet coeficients without noise
% image: image without noise (necessary if we have not decomposed it in wavelet coefficients in the deblurring step)
% lf_enhancement_factor: to enhance the morphological bands (valid value 0 to 1)
% level: of wavelet decomposition
% wname: type of wavelet 
% minSeSize: min size of the s.e.
% maxSeSize: max size of the structural element
% se: type of structural element
%...................................................................................................................
% OUTPUT PARAMETERS
% enhancedImage: resulting image from the algorithm
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016


function enhancedImage = morphoWavDom(c_denoised, s_denoised, image, lf_enhancement_factor, level, wname, minSeSize, maxSeSize, se)

    minImage = min(image(:));
    image = image-minImage;
    if lf_enhancement_factor == 0
        enhancedImage = image; % If the user has selected a factor of 0, he doesn't want to enhance the image so we don't have to apply any processing
    else
        if isempty(c_denoised)
            [c_denoised, s_denoised] = wavedec2(image,level,wname); % Wavelet decomposition (only if we have not performed it in the previous stage).
        end
        % Detail and approximation wavelet coeficients.
        appSize = s_denoised(1,1)*s_denoised(1,2);
        coes_lf = appcoef2(c_denoised, s_denoised, wname, level); % Approximation coefficients
        coes_hf = [];
        for k = 1:level
            det = detcoef2('compact',c_denoised,s_denoised,k);
            coes_hf = [det coes_hf];  % Detail coefficients
        end

        % We enhance the edges and details of the low frequency coefficients with a morphological processing
        c_lf_improved =  globalMorphological (uint16(coes_lf), lf_enhancement_factor, minSeSize, maxSeSize, se);

        % We convert the matrix into a row        
        c_lf = double(reshape(c_lf_improved,1, appSize));

        % We join all the coefficients into the same vector
        c_improved = [c_lf coes_hf];

        % Inverse wavelet transform
        enhancedImage = uint16(waverec2(c_improved,s_denoised, wname));
    end
    enhancedImage = enhancedImage + uint16(minImage);
end

