% Histogram matching: function to improve the contrast of an image.
% Transforms the input image (image) so that the histogram of the output image (equalizedImage) approximately
% matches the histogram of the modified image(ideal images saved in our database that correspond to a certain body part)
%...................................................................................................................
% INPUT PARAMETERS
% image: input image
% imRef: reference image 
% low_ref: background threshold of the reference image (saved)
% low_im: background threshold of the image to be processed (selected
%         manually by the user)
%...................................................................................................................
% OUTPUT PARAMETERS
% modifiedImage: contrast improved image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function modifiedImage = histogramMatch (orImage, imRef, low_ref, low_im_or)

% Maximum values of both images
high_ref = double(max(imRef(:)));
high_im = double(max(orImage(:)));

% Image intensity correction
normIm = double(orImage)/high_im;
normLow = double(low_im_or)/high_im;
image = uint16(normIm*high_ref);
low_im = round(normLow*high_ref);

% Estimate the histograms of the original and the reference images
hist1 = histc(image(:),low_im:high_ref);
hist2 = histc(imRef(:),low_ref:high_ref);

% Calculate the cdfs
cdf1 = cumsum(hist1); 
cdf1 = cdf1 - cdf1(1);
cdf1 = cdf1 / cdf1(end);

cdf2 = cumsum(hist2); 
cdf2 = cdf2 - cdf2(1);
cdf2 = cdf2 / cdf2(end);

% Create the LUT as the minimum differences in the cdfs for each bin of the
% histograms
indLUT = max(low_ref,low_im);
LUT1 = uint16(1:indLUT);

% Compare both cdfs to find which grey level of the image has a probability
% of ocurrence more similar to the grey levels of the reference image
for ind = 1:size(cdf1)
    [~,greyLevelMoreSimilar] = min(abs(cdf1(ind) - cdf2));
    myLUT(ind) = greyLevelMoreSimilar;
end
LUT2 = myLUT + indLUT;

if high_im > high_ref
    LUT3 = high_ref+1:high_im;
else
    LUT3 = [];
end
LUT = [LUT1 LUT2 LUT3];
LUT = LUT';

% Apply the LUT to obtain the image
changedArray = LUT(uint16(image(:)+1));
modifiedImage = uint16(reshape(changedArray, size(image)));



