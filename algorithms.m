function varargout = algorithms(varargin)
% ALGORITHMS MATLAB code for algorithms.fig
%      ALGORITHMS, by itself, creates a new ALGORITHMS or raises the existing
%      singleton*.
%
%      H = ALGORITHMS returns the handle to a new ALGORITHMS or the handle to
%      the existing singleton*.
%
%      ALGORITHMS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ALGORITHMS.M with the given input arguments.
%
%      ALGORITHMS('Property','Value',...) creates a new ALGORITHMS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before algorithms_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to algorithms_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLEShandles.activateRealTime

% Edit the above text to modify the response to help algorithms

% Last Modified by GUIDE v2.5 01-Aug-2017 12:37:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @algorithms_OpeningFcn, ...
                   'gui_OutputFcn',  @algorithms_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% 

% --- Executes just before algorithms is made visible.
function algorithms_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to algorithms (see VARARGIN)

if ~isdeployed
    addpath(genpath('./'))
end

% Choose default command line output for algorithms
handles.output = hObject;

%Initializations
handles.pathRoot = pwd;
javax.swing.UIManager.setLookAndFeel( com.sun.java.swing.plaf.windows.WindowsLookAndFeel)

% Disable warnings (for the treatment of the dicom info)
warning('OFF')

% Intermediate images
handles.image = [];
handles.imageNoCrop = []; % Whole image
handles.imageCropped = []; % Irradiated field image
handles.modifiedImage = []; % Image with intensity correction
handles.equalizedImage = []; % Sub-processed
handles.denoisedImage = []; % Sub-processed
handles.sDenoised = []; % wavelet parameters
handles.cDenoised = []; % wavelet parameters
handles.processedImage = []; % Processed
handles.processedImages = []; % Buffer of processed images to be compared

% Image information
handles.dicomInformation = [];
handles.originalFilePath  = '';
handles.factorZoom = [];
handles.info = '';
handles.irradiatedField = [];

% Denoising parameters
handles.level = 2;
handles.wavelet = 'ndmey';
handles.thresholdN = [];
handles.oldSliderNoise = 0;

% Contrast parameters
handles.minHist = 0;
handles.sliderContrast = 1.0;
handles.oldSliderContrast = 0;
handles.curveSoft = 0.5;
handles.curveDense = 0.5;
handles.levelSoft = 0.15;
handles.levelDense = 0.85;
handles.factorSoft = -1;
handles.factorDense = -1;
handles.curveGammaSoft = 0.5;
handles.curveGammaDense = 0.5;
handles.levelGammaSoft = 0.5;
handles.levelGammaDense = 0.5;
handles.levelLinealSoft = 0.5;
handles.levelLinealDense = 0.5;
handles.windowLinealSoft = 0.5;
handles.windowLinealDense = 0.5;
handles.algSoft = 1;
handles.algDense = 1;
handles.minOriginal = 0;
handles.maxOriginal = 4095;

% Processed image's name
handles.nameTypeNoise = '';
handles.nameTypeEnhancement = '';
handles.nameTypeContrast = '';
handles.nameTypeUSM = '';
handles.nameImage = '';
handles.nameProcessedImage = '';
handles.names = [];
handles.nameProtocol = '';
handles.nameROIcropped = '';
handles.nameBodyPart = '#NO_';

% Control parameters
handles.newLoaded = 0;
handles.activeRealTime = 0;
handles.panelChange = [0 0 0];
handles.originalIncluded = 0;

% Comparison mode
handles.nProcImages = 0;
handles.indexProcImages = 0;

% Buttons with picture
arrow = imread('arrowRight.jpg');
arrowSmall=arrow(1:8:end,1:8:end,:);
set(handles.pushbutton_next,'CData',arrowSmall)

arrow2 = imread('arrowLeft.jpg');
arrowSmall2=arrow2(1:8:end,1:8:end,:);
set(handles.pushbutton_previous,'CData',arrowSmall2)

% Buttons info
s1 = sprintf('Load a new image');
set(handles.pushbutton_load,'TooltipString',s1)

s2 = sprintf('Select the irradiated field and the threshold \nbetween background and foreground.');
set(handles.pushbutton_selectROI,'TooltipString',s2)

s3 = sprintf('Save the processed image');
set(handles.pushbutton_save,'TooltipString',s3)

s4 = sprintf('DICOM info of the image');
set(handles.pushbutton_info,'TooltipString',s4)

s5 = sprintf('Save the current processed image to be compared with other \ntypes of saved processed images');
set(handles.pushbutton_fix,'TooltipString',s5)

s6 = sprintf('Delete the image that is currently shown on the right \nfrom the list of images to be compared');
set(handles.pushbutton_delete,'TooltipString',s6)

s7 = sprintf('Goes to the next image of the list of images to be compared');
set(handles.pushbutton_next,'TooltipString',s7)

s8 = sprintf('Goes to the previous image of the list of images to be compared');
set(handles.pushbutton_previous,'TooltipString',s8)

s9 = sprintf('Apply the algorithms we have selected in the panels of the interface');
set(handles.pushbutton_process,'TooltipString',s9)

s10 = sprintf('Activate or deactivate real time processing. If it is deactivated, \nclick on the PROCESS button to obtain the processed image');
set(handles.activateRealTime,'TooltipString',s10)

s11 = sprintf('Load a protocol that is well suited for a type of undesired effect \n(noise, blurring, contrast) and part of the animal body');
set(handles.pushbutton_loadProtocol,'TooltipString',s11)

s12 = sprintf('Save a protocol to be used with a type of undesired effect \n(noise, blurring, contrast)and part of the animal body');
set(handles.pushbutton_saveProtocol,'TooltipString',s12)

s13 = sprintf('Show examples of the contrast improvement algorithms');
set(handles.pushbutton_helpContrast,'TooltipString',s13)

s14 = sprintf('Show examples of the edges and details enhancement algorithms');
set(handles.pushbutton_helpEnhancement,'TooltipString',s14)

s15 = sprintf('Show examples of the denoising algorithms');
set(handles.pushbutton_helpNoise,'TooltipString',s15)

s16 = sprintf('Accept the intensity correction and/or the ROI selection');
set(handles.pushbutton_okChange,'TooltipString',s16)

s17 = sprintf('Discard the intensity correction and the ROI selection');
set(handles.pushbutton_skip,'TooltipString',s17)

% Disable the buttons that cannot be used at the begining
set (handles.pushbutton_save,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_info,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_zoom_in,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_zoom_out,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_noZoom,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_change, 'Visible', 'on','Enable','off')
set (handles.pushbutton_process, 'Enable', 'off')
set (handles.pushbutton_selectROI, 'Enable', 'off')
set (handles.pushbutton_okChange, 'Enable', 'off')
set (handles.pushbutton_skip, 'Enable', 'off')
set (handles.pushbutton_fix, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_delete, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.original, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_next, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.pushbutton_previous, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
set (handles.text_percentageNoise, 'Enable', 'off', 'Position', [0.506 0.398 0.061 0.186], 'Enable','off', 'String', '0%  ')
set (handles.text_denoising, 'Position', [0.587 0.625 0.364 0.186], 'Enable', 'off')
set (handles.text_thresh, 'Enable','off','Position', [0.586 0.193 0.358 0.186], 'String', ' -                      Blurring                            +')
set (handles.text_th, 'Visible', 'off', 'Enable','off','String', '* th.')
set (handles.text_radius, 'Visible', 'on', 'Enable', 'off')
set (handles.text_enhancementPercentage, 'Enable','off','String', '0%')
set (handles.text_enhance, 'Enable','off')
set (handles.text_contrastPercentage, 'Enable','off','String', '0%')
set (handles.text_contrast, 'Enable','off', 'String', '-              Image`s contrast              +')
set (handles.text_selectBody, 'Enable', 'off')
set (handles.popupmenu_typeNoise, 'Visible', 'on', 'Value', 4,'Enable', 'off')
set (handles.popupmenu_typeEnhancement, 'Value', 4, 'Enable', 'off')
set (handles.popupmenu_typeEqualization, 'Value', 4, 'Enable', 'off')
set (handles.popupmenu_refIm, 'Enable', 'off')
set (handles.slider_noise, 'Enable', 'off', 'Value', 0.5,'SliderStep', [0.125 0.125], 'Position', [0.568 0.429 0.385 0.186])
set (handles.slider_th, 'Enable', 'off', 'Visible', 'off', 'Value', 0.25)
set (handles.slider_contrast, 'Enable', 'off', 'Value', 0.5,'Enable','off')
set (handles.slider_enhancement, 'Enable', 'off', 'Value', 0.5, 'Enable','off')
set (handles.edit_radius, 'Visible', 'on','Enable', 'off', 'String', 1)
set (handles.edit_seSize, 'Visible', 'off', 'String', '10')
set (handles.edit_minSe, 'Visible', 'off', 'String', '5')
set (handles.activateRealTime, 'Enable', 'off')
       
% We link the axis in order to apply the zoom and pan tools
h(1)=handles.axes1;
h(2)=handles.axes2;
linkaxes (h, 'xy')

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = algorithms_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% **********************************************************************************************************************************************************************************************************

% CALLBACKS OF THE ELEMENTS OF THE INTERFACE

% **********************************************************************************************************************************************************************************************************

% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% INTENSITY CORRECTION
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

% --- Executes on button press in pushbutton_selectROI.
function pushbutton_selectROI_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_selectROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%.........................................................................
% When the user push the button, interface asks for the selection of the
% irradiated field of the image and its background
%.........................................................................
% Send the image and its modified version (cropped) as well as the current threshold
% and the irradiated field to the secondary window
setappdata (0,'image',handles.imageNoCrop)
setappdata (0,'modifiedImage',handles.imageCropped)
setappdata (0,'backTh',handles.minHist)
setappdata (0,'rect',handles.irradiatedField)

% Call the window to select the irradiated field and the background threshold
irradiatedFieldBackground
uiwait

% Get the information from the secondary window
handles.minHist = getappdata (0,'backTh');
handles.irradiatedField = getappdata (0,'rect');
xmin = handles.irradiatedField(1);
ymin = handles.irradiatedField(2);
xmax = handles.irradiatedField(3);
ymax = handles.irradiatedField(4);

% Clear processed image
axes(handles.axes1)
cla 

% When the image was not cropped
if isempty(xmin)
    %Update the name of the image
    handles.nameROIcropped = ['&b' num2str(handles.minHist)];
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    % The image and its modified version are the original one (no cropped)
    handles.image = handles.imageNoCrop;
    handles.imageCropped = handles.image;
    
    % Show the image
    axes(handles.axes2)
    %imshow(handles.image,[handles.minOriginal handles.maxOriginal])
    imshow(handles.image,[handles.minHist handles.maxOriginal])
   
    % Reset control parameter
    handles.newLoaded = 0;

% When the image was cropped
else
    %When the image has been cropped, the list of processed images should
    %be reseted as the sizes changes and all cannot be in the same buffer
    %of images.
    clear handles.processedImages handles.names
    handles.processedImages  = [];
    handles.names = [];
    handles.indexProcImages = 0;
    handles.nProcImages = 0;
    set(handles.text_index, 'String', '')
    %Update the name of the image
    handles.nameROIcropped = ['&x' num2str(round(xmin)) '&y' num2str(round(ymin)) '&w' num2str(round(xmax-xmin)) '&h' num2str(round(ymax-ymin)) '&b' num2str(handles.minHist) '_'];
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    % Crop the image 
    handles.image = handles.imageNoCrop(ymin:ymax,xmin:xmax);
    handles.imageCropped = handles.image;
    % Show the image
    axes(handles.axes2)
    %imshow(handles.image,[handles.minOriginal handles.maxOriginal])
    imshow(handles.image,[handles.minHist handles.maxOriginal])
    % Reset control parameter
    handles.newLoaded = 0;
end

% Update the modified image with the new cropping and background threshold
popupmenu_refIm_Callback(hObject, eventdata, handles)

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on selection change in popupmenu_refIm.
function popupmenu_refIm_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_refIm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_refIm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_refIm

% Depending on the body part selected by the user, the reference image will
% be one of the images saved in the database 
option = get(handles.popupmenu_refIm, 'Value');

% Apply the histogram matching algorithm
switch (option)
    case(2)
        imRef = dicomread ([pwd '\Database\ChestPA']);
        handles.nameBodyPart = '#XP_';
        low_th = 1;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(3)
        imRef = dicomread ([pwd '\Database\ChestLAT']);
        handles.nameBodyPart = '#XL_';
        low_th = 78;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(4)
        imRef = dicomread ([pwd '\Database\AbdPA']);
        handles.nameBodyPart = '#AP_';
        low_th = 1;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(5)
        imRef = dicomread ([pwd '\Database\AbdLAT']);
        handles.nameBodyPart = '#AL_';
        low_th = 1;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(6) 
        imRef = dicomread ([pwd '\Database\ChestAbdPA']);
        handles.nameBodyPart = '#KP_';
        low_th = 50;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(7)
        imRef = dicomread ([pwd '\Database\ChestAbdLAT']);
        handles.nameBodyPart = '#KL_';
        low_th = 110;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(8)
        imRef = dicomread ([pwd '\Database\SkullPA']);
        handles.nameBodyPart = '#SP_';
        low_th = 1;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(9)
        imRef = dicomread ([pwd '\Database\SkullLAT']);
        handles.nameBodyPart = '#SL_';
        low_th = 15;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(10)
        imRef = dicomread ([pwd '\Database\Spine']);
        handles.nameBodyPart = '#SN_';
        low_th = 250;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(11)
        imRef = dicomread ([pwd '\Database\Cervical']);
        handles.nameBodyPart = '#RV_';
        low_th = 1;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(12)
        imRef = dicomread ([pwd '\Database\Hips']);
        handles.nameBodyPart = '#HI_';
        low_th = 5;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(13)
        imRef = dicomread ([pwd '\Database\Limbs']);
        handles.nameBodyPart = '#LB_';
        low_th = 210;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(14)
        imRef = dicomread ([pwd '\Database\LimbsImplants']);
        handles.nameBodyPart = '#LI_';
        low_th = 186;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(15)
        imRef = dicomread ([pwd '\Database\Paws']);
        handles.nameBodyPart = '#PW_';
        low_th = 300;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    case(16)
        imRef = dicomread ([pwd '\Database\Shoulder']);
        handles.nameBodyPart = '#SH_';
        low_th = 200;
        modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, handles.minHist);
    otherwise
        modifiedImage = handles.imageCropped;
        handles.nameBodyPart = '#NO_';
end

% Image whose histogram has been matched with the reference one
handles.modifiedImage = modifiedImage;

% Enable the okChange button
set (handles.pushbutton_okChange, 'Enable', 'on', 'BackgroundColor', [0.894, 0.941, 0.902])

% Enable the fix and delete buttons
set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

% Show the modified image
axes(handles.axes1);
%[bins,~] = imhist(handles.modifiedImage,2^16);
[minIm, maxIm] = imageRange(handles.modifiedImage);
imshow(handles.modifiedImage,[minIm maxIm])
handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart 'CNo_%0' '_NNo%0' '_ENo_%0' ')'];
set (handles.text_nameRight, 'String', handles.nameProcessedImage);

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in pushbutton_skip.
function pushbutton_skip_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_skip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% If we do not want the images from histogram matching, the list of
% processed images has to be reseted.
clear handles.processedImages handles.names
handles.processedImages = [];
handles.names = [];
handles.indexProcImages = 0;
handles.nProcImages = 0;

% If user push this button, he does not want to modify the intensity of the
% image. Disable the intensity correction panel and enable the rest of the panels.
set (handles.popupmenu_refIm, 'Enable', 'off')
set (handles.pushbutton_selectROI, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_okChange, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_skip, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_process, 'Enable', 'on','BackgroundColor', [0.882, 0.882, 0.969])
set (handles.pushbutton_save,  'BackgroundColor', [0.729, 0.831, 0.957], 'Enable', 'on')
set (handles.pushbutton_loadProtocol,  'BackgroundColor', [0.729, 0.831, 0.957], 'Enable', 'on')
set (handles.pushbutton_helpContrast, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_helpNoise, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_helpEnhancement, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_fix, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_delete, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.original, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_next, 'Enable', 'off')
set (handles.pushbutton_previous, 'Enable', 'off')
set (handles.text_index, 'String', ' ')
set (handles.text_selectBody, 'Enable', 'off')
set (handles.popupmenu_typeEnhancement, 'Enable', 'on')
set (handles.popupmenu_typeNoise, 'Enable', 'on')
set (handles.popupmenu_typeEqualization, 'Enable', 'on')
set (handles.activateRealTime, 'Enable', 'on', 'String', ' No Real Time', 'BackgroundColor', [0.95 0.80 0.80], 'ForegroundColor', [0.899 0.0 0.0])
handles.nameROIcropped = '';

% No body part selected. Update the names of the images
handles.nameBodyPart = '#NO_';
handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
set (handles.text_nameRight, 'String', handles.nameProcessedImage);

% The modified image is the image itself
handles.modifiedImage = handles.imageNoCrop;
handles.image = handles.imageNoCrop;

% Show original image on the right box (no histogram change has been done)
axes(handles.axes2)
imshow(handles.image,[handles.minOriginal handles.maxOriginal])
axes(handles.axes1)
imshow(handles.image,[handles.minOriginal handles.maxOriginal])

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_okChange.
function pushbutton_okChange_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_okChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%....................................................................................................
% When the user push this button, he wants to keep a image cropping and/or the histogram modification
%....................................................................................................

% Original image is the modified one and it has to be shown in both boxes
handles.modifiedImage = getimage(handles.axes1);
axes(handles.axes2)
%[bins,~] = imhist(uint16(handles.modifiedImage),2^16);
[minIm, maxIm] = imageRange(uint16(handles.modifiedImage));
handles.minOriginal = minIm;
handles.maxOriginal = maxIm;
imshow(handles.modifiedImage,[minIm maxIm])
handles.image = handles.modifiedImage;
handles.equalizedImage = handles.modifiedImage;
handles.denoisedImage = handles.modifiedImage;
handles.processedImage = handles.modifiedImage;
clear handles.processedImages handles.names
% handles.processedImages(:,:,1) = handles.modifiedImage;
% handles.names{1,1} = get(handles.text_nameRight, 'String');
handles.indexProcImages = 0;
handles.nProcImages = 0;

% Disable the intensity correction panel and enable the rest of the panels.
set (handles.popupmenu_refIm, 'Enable', 'off')
set (handles.pushbutton_selectROI, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_okChange, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_skip, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_process, 'Enable', 'on', 'BackgroundColor', [0.882, 0.882, 0.969])
set (handles.pushbutton_save,  'BackgroundColor', [0.729, 0.831, 0.957], 'Enable', 'on')
set (handles.pushbutton_loadProtocol,  'BackgroundColor', [0.729, 0.831, 0.957], 'Enable', 'on')
set (handles.pushbutton_helpContrast, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_helpNoise, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_helpEnhancement, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
set (handles.pushbutton_fix, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_delete, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.original, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_next, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.pushbutton_previous, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
set (handles.text_selectBody, 'Enable', 'off')
set (handles.text_index, 'String', ' ')
set (handles.popupmenu_typeEnhancement, 'Enable', 'on')
set (handles.popupmenu_typeNoise, 'Enable', 'on')
set (handles.popupmenu_typeEqualization, 'Enable', 'on')
set (handles.activateRealTime, 'Enable', 'on', 'String', ' No Real Time', 'BackgroundColor', [0.95 0.80 0.80], 'ForegroundColor', [0.899 0.0 0.0])


% Update the control parameter
handles.newLoaded = 0;
    
% Update handles structure
guidata(handles.figure1,handles);

% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% CONTRAST IMPROVEMENT PANEL
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

% --- Executes on slider movement.
function slider_contrast_Callback(hObject, eventdata, handles)
% hObject    handle to slider_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Depending on the type of equalization, the percentage of contrast is calculated differently.
typeEq = get(handles.popupmenu_typeEqualization, 'Value');
levelContrast = get (handles.slider_contrast, 'Value');

    if typeEq == 3 % Gamma Correction
         if levelContrast >= 0.5
                percGamma = (levelContrast-0.5)*2*100;
            if levelContrast > 0.5
                set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Dense']);
            else
                set(handles.text_contrastPercentage, 'String', 'No contrast modification');
            end
         else 
            percGamma = (1-levelContrast*2)*100;
            set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Soft']);
         end
    else % Histogram Equalization
         percContrast = round(levelContrast*100);
         set(handles.text_contrastPercentage, 'String', [num2str(percContrast) '%']);
    end

    % Process the image directly if real time is activated
    if handles.activeRealTime == 1
        
        % Show the message PROCESSING...
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)
        
        % Equalizing the image
        [equalizedImage, handles] = equalizeImage (handles);
        handles.equalizedImage = uint16(equalizedImage);

        % Denoising image
        [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
        handles.denoisedImage = uint16(denoisedImage);
        handles.cDenoised = cDenoised;
        handles.sDenoised = sDenoised;

        % Enhancing the denoised image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);

        % Processed image's name
        handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
        
        % Enable the fix and delete buttons
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

        %We select the current limits in case the image has been zoomed and panned
        xLim = get(gca,'XLim');
        yLim = get(gca,'YLim');
        handles.factorZoom = [xLim yLim];

        % Show the new processed image
        axes(handles.axes1);
        %[bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        set (handles.text_nameRight, 'String', handles.nameProcessedImage);
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 
       
        % Processing finished
        set (handles.text_processing, 'Visible', 'off')
        set (handles.pushbutton_wlOr, 'Visible', 'on')
        handles.nameProtocol = ''; % Reset
        handles.panelChange =  [0 0 0]; % No panel has been changed
    else
        handles.panelChange(1) = 1; % Contrast panel has been changed
    end
    
% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on selection change in popupmenu_typeEqualization.
function popupmenu_typeEqualization_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_typeEqualization contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_typeEqualization

% Enable and disable fields depending on the type of equalization
typeEqualization = get(handles.popupmenu_typeEqualization, 'Value');

% Calculate the percentage of contrast improvement
factor = get (handles.slider_contrast, 'Value');
percentage = num2str(round((factor)*100));

 if typeEqualization == 1 % Subhistogram intensity correction
    set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'off')
    set (handles.text_contrast, 'Visible', 'on', 'Enable', 'off', 'String', ' -              Image`s contrast              + ')
    set (handles.text_contrastPercentage, 'Visible','on', 'Enable','off','String', [num2str(percentage) '%']);
    set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Select the functions parameters', 'BackgroundColor', [0.729, 0.831, 0.957])
    
elseif typeEqualization == 2 % General equalization
    set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on')
    set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on', 'String', ' -              Image`s contrast              + ')
    set (handles.text_contrastPercentage, 'Visible','on', 'Enable','on','String', [num2str(percentage) '%']);
    set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background', 'BackgroundColor', [0.729, 0.831, 0.957])
    
elseif typeEqualization == 3 % Gamma correction
    set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on', 'Value', 0.5) %put the slider where we don't apply any gamma (lineal slope)
    set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on', 'String', ' Soft     <---     TISSUE     --->      Dense ')
    set (handles.text_contrastPercentage, 'Visible','on', 'Enable','on','String', 'No contrast modification');
    set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background', 'BackgroundColor', [0.729, 0.831, 0.957])

 else %None
    set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'off')
    set (handles.text_contrast, 'Visible', 'on', 'Enable', 'off', 'String', ' -              Image`s contrast              + ', 'Enable', 'off')
    set(handles.text_contrastPercentage, 'Visible','on','String', '0%', 'Enable', 'off');
    set (handles.pushbutton_change, 'Visible', 'on','Enable','off','String','Eliminate the background', 'BackgroundColor', [0.941, 0.941, 0.941])    
end

% Process the image directly if real time is activated
if handles.activeRealTime == 1
    
    % We show the message PROCESSING...
    if (typeEqualization ~= 4)
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)
    end

    % Process the image
    [equalizedImage, handles] = equalizeImage (handles);
    handles.equalizedImage = uint16(equalizedImage);
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    % We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the new processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(1) = 1; % Contrast panel has been changed
end
% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_change.
function pushbutton_change_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_change (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ........................................................................
% Button to change the sub-histogram intensity correction parameters or
% the select the background threshold (general eq and gamma correction)
% ........................................................................
typeContrastAlg = get(handles.popupmenu_typeEqualization, 'Value');

switch (typeContrastAlg)
    case(1)
        handles = subHistogramCorrection(handles);
    otherwise 
        handles = background (handles);
end

% Update handles structure
guidata(handles.figure1,handles);


% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% DENOISING PANEL
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

% --- Executes on selection change in popupmenu_typeNoise.
function popupmenu_typeNoise_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_typeNoise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_typeNoise

% Depending on the denoising algorithm selected by the user, the different elements of the interface will be enabled, disabled or 
% they will be placed at different positions
typeNoise = get (handles.popupmenu_typeNoise, 'Value');

if (typeNoise==1) 
    set (handles.slider_noise, 'Enable','on','SliderStep', [0.125 0.125], 'Position', [0.568 0.639 0.385 0.186])
    set (handles.slider_th, 'Visible', 'on', 'Enable', 'on')
    set (handles.text_denoising, 'Enable','on','Position', [0.587 0.835 0.364 0.186])
    set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.639 0.061 0.186])
    set (handles.text_th, 'Visible', 'on','Enable','on')
    set (handles.text_thresh, 'Visible','on','Enable','on','Position', [0.586 0.103 0.358 0.186], 'String', ' -             | Optimal edges threshold          +')

elseif typeNoise == 4
    set (handles.slider_noise, 'Position', [0.568 0.429 0.385 0.186], 'Enable', 'off')
    set (handles.slider_th, 'Visible', 'off')
    set (handles.text_thresh, 'Position', [0.586 0.193 0.358 0.186], 'Enable', 'off',  'String', ' -                      Blurring                            +')
    set (handles.text_th, 'Visible', 'off')
    set (handles.text_denoising, 'Position', [0.587 0.625 0.364 0.186], 'Enable', 'off')
    set (handles.text_percentageNoise, 'Position', [0.506 0.398 0.061 0.186], 'Enable','off')

else
    set (handles.slider_noise, 'Enable','on','SliderStep', [0.01 0.01], 'Position', [0.568 0.429 0.385 0.186])
    set (handles.slider_th, 'Visible', 'off')
    set (handles.text_thresh, 'Enable','on','Position', [0.586 0.193 0.358 0.186], 'String', ' -                      Blurring                            +')
    set (handles.text_th, 'Visible', 'off')
    set (handles.text_denoising, 'Enable','on','Position', [0.587 0.625 0.364 0.186])
    set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.398 0.061 0.186])
end

% Process the image directly if real time is activated
if handles.activeRealTime == 1
    
    % We show the message PROCESSING...
    if (typeNoise ~= 4)
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)
    end

    % Denoising is done over the equalized image. If it is empty, it
    % should be equal to the original image
    if isempty (handles.equalizedImage)
        handles.equalizedImage = handles.image;
    end

    % Process the image
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    % We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));    

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.newLoaded = 0;
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(2) = 1; % Denoising panel has been changed
end

% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on slider movement.
function slider_noise_Callback(hObject, eventdata, handles)
% hObject    handle to slider_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Depending on the type of denoising algorithm, different options are enabled/disabled
typeNoise = get (handles.popupmenu_typeNoise, 'Value');

% We force the wavelet level to be greater or lower than the current one
currentNoise = get(handles.slider_noise, 'Value');
if (typeNoise == 1)
    if currentNoise > handles.oldSliderNoise
        if (currentNoise - handles.oldSliderNoise)<1/8
            newNoise = currentNoise + 1/8;
            if newNoise > 1
                newNoise = 1;
            end
        else
            newNoise = currentNoise;
        end
    else
        if (handles.oldSliderNoise - currentNoise)<1/8
            newNoise = currentNoise - 1/8;
            if newNoise < 0
                newNoise = 0;
            end
        else
            newNoise = currentNoise;
        end
    end
    set (handles.slider_noise, 'Value', newNoise)
    handles.oldSliderNoise = newNoise;
else
    handles.oldSliderNoise = currentNoise;
end

% Calculate and set the percentage of denoising
percentage = [num2str(round(get(handles.slider_noise, 'Value')*100)) '%  '];
set (handles.text_percentageNoise, 'String', percentage)

% Process the image directly if real time is activated
if handles.activeRealTime == 1
    
    % Show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Visible', 'off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)
    
    % Denoising is done over the equalized image. If it is empty, it
    % should be equal to the original image
    if isempty (handles.equalizedImage)
        handles.equalizedImage = handles.image;
    end
    
    % Process the image
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));
    
    % Processing finished    
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(2) = 1; % Denoising panel has been changed
end

% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on slider movement.
function slider_th_Callback(hObject, eventdata, handles)
% hObject    handle to slider_th (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Depending on the slider, the threshold changes
factorTh = get(handles.slider_th, 'Value');
if factorTh < 0.25
    newFact = factorTh*4;
elseif factorTh == 0.25
    newFact = [];
else
    newFact = 12*(factorTh-0.25)+1;
end
if newFact == 1
    percentageTh = ' th.';
else
    percentageTh = [num2str(newFact,2) '* th.'];
end

% Show the calculated percentage and the threshold text
percentage = [num2str(round(get(handles.slider_noise, 'Value')*100)) '%  '];
set(handles.text_percentageNoise, 'String', percentage)
set(handles.text_th, 'String', percentageTh)

% Process the image directly if real time is activated
if handles.activeRealTime == 1
    
    % Show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Visible', 'off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)
    
    % Denoising is done over the equalized image. If it is empty, it
    % should be equal to the original image
    if isempty (handles.equalizedImage)
        handles.equalizedImage = handles.image;
    end
    
    % Process the image
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set(handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.newLoaded = 0;
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(2) = 1; % Denoising panel has been changed
end

% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% EDGES AND DETAILS ENHANCEMENT PANEL
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

% --- Executes on slider movement.
function slider_enhancement_Callback(hObject, eventdata, handles)
% hObject    handle to slider_enhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Calculate the percentage of enhancement and set the value on the interface
levelEnhancement = get (handles.slider_enhancement, 'Value');
percentage = [num2str(round(levelEnhancement*100)) '%'];
set (handles.text_enhancementPercentage, 'String', percentage)

% Process the image directly if real time is activated
if handles.activeRealTime == 1
    
    % Show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Visible', 'off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)

    % Enhancing is done over the denoised image. If it is empty, it
    % should be equal to the original image
    if isempty (handles.denoisedImage)
        handles.denoisedImage = handles.image;
    end

    % Process the image
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the new processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set(handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(3) = 1; % Enhancement panel has been changed
end

% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;
    
% Update handles structure
guidata(handles.figure1,handles);

function edit_radius_Callback(hObject, eventdata, handles)
% hObject    handle to edit_radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_radius as text
%        str2double(get(hObject,'String')) returns contents of edit_radius as a double

radius = get (handles.edit_radius, 'String');
typeProc = get(handles.popupmenu_typeEnhancement,'Value');
[rs,cs] = size(handles.image); %Default value

% Firstly, we limit the values the user can introduce in the interface
if (typeProc == 2)
    % We are going to apply a wavelet decomposition so we need to know
    % the level from the noise slider 
    level = ceil(8*(get(handles.slider_noise,'Value')));
    [rs,cs] = size(handles.image);
    % The approximation coefficients image has a reduced size in
    % relation with the level of decomposition
    rs = round(rs/(2^level));
    cs = round(cs/(2^level));
end

% Error messages appear if you don't write a valid radius
if ~isempty (strfind(radius, ','));
    uiwait(msgbox('The edge`s thickness has to be a number >= 0.001', 'Info'))
    set(hObject,'String','1')
    validEnhancement = 0;
else
    radius = str2double(radius);
    if isnan (radius)
        uiwait(msgbox('The edge`s thickness has to be a number >= 0.001', 'Info'));
        set(hObject,'String','1')
        validEnhancement = 0;
    elseif isempty (radius)
        uiwait(msgbox('The edge`s thickness has to be a number >= 0.001', 'Info'));
        set(hObject,'String','1')
        validEnhancement = 0;
    elseif (radius > min(rs,cs))
        uiwait(msgbox(['The edge�s thickness cannot be grater than the matrix�s dimensions (' num2str(min(rs,cs)) ')'], 'Info'));
        validEnhancement = 0;
        set(hObject,'String',num2str(min(rs,cs)))
    elseif radius < 0.001
        uiwait(msgbox('The edge`s thickness has to be a number >= 0.001', 'Info'));
        validEnhancement = 0;
    else
        validEnhancement = 1;
        guidata(handles.figure1,handles);
    end
end

% We can continue only if the radius is valid for the processing
if validEnhancement == 1
    
    % Process the image directly if real time is activated
    if handles.activeRealTime == 1

        % Show the message PROCESSING...
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)
        
        % Enhancing is done over the denoised image. If it is empty, it
        % should be equal to the original image
        if isempty (handles.denoisedImage)
            handles.denoisedImage = handles.image;
        end

        % Process the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);

        % Enable the fix and delete buttons
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        
        % Processed image's name
        handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

        % We select the current limits in case the image has been zoomed and panned
        xLim = get(gca,'XLim');
        yLim = get(gca,'YLim');
        handles.factorZoom = [xLim yLim];

        % Show the new processed image
        axes(handles.axes1);
        %[bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        set (handles.text_nameRight, 'String', handles.nameProcessedImage);
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

        % Processing finished
        set (handles.text_processing, 'Visible', 'off')
        set (handles.pushbutton_wlOr, 'Visible', 'on')
        handles.nameProtocol = ''; %Reset
        handles.panelChange = [0 0 0]; % No panel has been changed
    
    else
        handles.panelChange(3) = 1; % Enhancement has been changed
    end
    
    % The image has been processed so we modify the control parameter newLoaded
    handles.newLoaded = 0;
end  

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on selection change in popupmenu_typeEnhancement.
function popupmenu_typeEnhancement_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeEnhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_typeEnhancement contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_typeEnhancement

% Enable and disable some fields depending on the type of processing
type = get (handles.popupmenu_typeEnhancement, 'Value');

if (type == 1) || (type == 2) %USM
    set (handles.slider_enhancement, 'Enable', 'on')
    set (handles.edit_seSize, 'Visible', 'off')
    set (handles.edit_minSe, 'Visible', 'off')
    set (handles.edit_radius, 'Visible', 'on', 'Enable', 'on')   
    set (handles.text_se, 'Visible', 'off')
    set (handles.text_radius, 'Visible', 'on', 'Enable', 'on')
    set (handles.text_enhance, 'Enable', 'on')
    set (handles.text_enhancementPercentage, 'Enable', 'on')
    
elseif type == 3 %Morpho
    set (handles.slider_enhancement, 'Enable', 'on')
    set (handles.edit_seSize, 'Visible', 'on', 'Enable', 'on')
    set (handles.edit_minSe, 'Visible', 'on', 'Enable', 'on')
    set (handles.edit_radius, 'Visible', 'off')
    set (handles.text_se, 'Visible', 'on')
    set (handles.text_se, 'Enable', 'on')
    set (handles.text_radius, 'Visible', 'off')
    set (handles.text_enhance, 'Enable', 'on')
    set (handles.text_enhancementPercentage, 'Enable', 'on')  

else %None
    set (handles.slider_enhancement, 'Enable', 'off')
    set (handles.edit_seSize, 'Visible', 'off')
    set (handles.edit_minSe, 'Visible', 'off')
    set (handles.edit_radius, 'Visible', 'on', 'Enable', 'off')
    set (handles.text_se, 'Visible', 'off')
    set (handles.text_radius, 'Visible', 'on', 'Enable', 'off')
    set (handles.text_enhance, 'Enable', 'off')
    set (handles.text_enhancementPercentage, 'Enable', 'off')
    
end

% Process the image directly if real time is activated
if handles.activeRealTime == 1

    % Show the message PROCESSING...
    if type ~= 4 
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)
    end

    % Enhancing is done over the denoised image. If it is empty, it
    % should be equal to the original image
    if isempty (handles.denoisedImage)
        handles.denoisedImage = handles.image;
    end

    % Process the iamge
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);

    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        
    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the new processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.panelChange = [0 0 0]; % No panel has been changed
else
    handles.panelChange(3) = 1; % Enhancement panel has been changed
end
    
% The image has been processed so we modify the control parameter newLoaded
handles.newLoaded = 0;

% Update handles structure
guidata(handles.figure1,handles);

function edit_seSize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_seSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_seSize as text
%        str2double(get(hObject,'String')) returns contents of edit_seSize as a double

% Get the sizes of the s.e. introduced by the user
seSize = get (handles.edit_seSize, 'String');
minSeSize = str2double(get(handles.edit_minSe, 'String'));

% Firstly, we have to limit the values that the user can introduce in the
% interface
typeProc = get(handles.popupmenu_typeEnhancement,'Value');
[rs,cs] = size(handles.image); %Default value

if (typeProc == 3)
    % We are going to apply a wavelet decomposition so we need to know
    % the level from the noise slider 
    level = ceil(8*(get(handles.slider_noise,'Value')));
    rs = round(rs/(2^level));
    cs = round(cs/(2^level));    
end

if ~isempty (strfind(seSize, ','));
    uiwait(msgbox('The maximum detail`s size has to be an integer number >= 1', 'Info'))
    set(hObject,'String','10')
    validEnhancement = 0;
else
    seSize = str2double(seSize);
    integerTest=~mod(seSize,1);
    
    if isnan (seSize)
        uiwait(msgbox('The maximum detail`s size has to be an integer number >= 1', 'Info'));
        set(hObject,'String','10')
        validEnhancement = 0;
    elseif isempty (seSize)
        uiwait(msgbox('The maximum detail`s size has to be an integer number >= 1', 'Info'));
        set(hObject,'String','10')
        validEnhancement = 0;
    elseif (seSize < 1) || (integerTest == 0)
        uiwait(msgbox('The maximum detail`s size has to be an integer number >= 1', 'Info'));
        set(hObject,'String','10')
        validEnhancement = 0;
    elseif (seSize > min(rs,cs))
        uiwait(msgbox(['The details cannot be grater than the matrix�s dimensions (' num2str(min(rs,cs)) ')'], 'Info'));
        set(hObject,'String',num2str(min(rs,cs)))
        validEnhancement = 0;
    else
        validEnhancement = 1;
    end
end

if minSeSize > seSize
    uiwait(msgbox('The minimum detail`s size cannot be greater than the maximum one. Please, change it in the `EDGES AND DETAILS ENHANCEMENT`panel', 'Info'));
    validEnhancement = 0;
end

% We can continue only if the radius is valid for the processing
if validEnhancement == 1

    % Process the image directly if real time is activated
    if handles.activeRealTime == 1   
        
        % Show the message PROCESSING...
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)

        % Enhancing is done over the denoised image. If it is empty, it
        % should be equal to the original image
        if isempty (handles.denoisedImage)
            handles.denoisedImage = handles.image;
        end

        % Process the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);

        % Enable the fix and delete buttons
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        
        % Processed image's name
        handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

        %We select the current limits in case the image has been zoomed and panned
        xLim = get(gca,'XLim');
        yLim = get(gca,'YLim');
        handles.factorZoom = [xLim yLim];

        % Show the new processed image
        axes(handles.axes1);
        %[bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        set(handles.text_nameRight, 'String', handles.nameProcessedImage);
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

        % Processing finished
        set (handles.text_processing, 'Visible', 'off')
        set (handles.pushbutton_wlOr, 'Visible', 'on')
        handles.nameProtocol = ''; % Reset
        handles.panelChange = [0 0 0]; % No panel has been changed
    else
        handles.panelChange(3) = 1; % Enhancing panel has been changed
    end

end

% Update handles structure
guidata(handles.figure1,handles);

function edit_minSe_Callback(hObject, eventdata, handles)
% hObject    handle to edit_minSe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_minSe as text
%        str2double(get(hObject,'String')) returns contents of edit_minSe as a double
   
% Get the sizes of the s.e. introduced by the user
minSeSize = get (handles.edit_minSe, 'String');
maxSeSize = str2double(get (handles.edit_seSize, 'String'));

% Firstly, we have to limit the values that the user can introduce in the
% interface
typeProc = get(handles.popupmenu_typeEnhancement,'Value');
[rs,cs] = size(handles.image); %Default value

if (typeProc == 3)
    % We are going to apply a wavelet decomposition so we need to know
    % the level from the noise slider 
    level = ceil(8*(get(handles.slider_noise,'Value')));
    rs = round(rs/(2^level));
    cs = round(cs/(2^level));
end

if ~isempty (findstr (minSeSize, ','));
    uiwait(msgbox('The minimum detail you can enhance has to be an integer number >= 1', 'Info'))
    set(hObject,'String','1')
    validEnhancement = 0;
else
    minSeSize = str2double(minSeSize);
    integerTest=~mod(minSeSize,1);

    if isnan (minSeSize)
        uiwait(msgbox('The minimum detail you can enhance has to be an integer number >= 1', 'Info'));
        set(hObject,'String','5')
        validEnhancement = 0;
    elseif isempty (minSeSize)
        uiwait(msgbox('The minimum detail you can enhance has to be an integer number >= 1', 'Info'));
        set(hObject,'String','5')
        validEnhancement = 0;
    elseif (minSeSize < 1) || (integerTest == 0)
        uiwait(msgbox('The minimum detail you can enhance has to be an integer number >= 1', 'Info'));
        set(hObject,'String','5')
        validEnhancement = 0;
    elseif (minSeSize > min(rs,cs)) 
        uiwait(msgbox(['The details cannot be grater than the matrix�s dimensions (' num2str(min(rs,cs)) ')'], 'Info'));
        set(hObject,'String',num2str(min(rs,cs)))
        validEnhancement = 0;
    else
        validEnhancement = 1;
    end
end

if minSeSize > maxSeSize
    uiwait(msgbox('The minimum detail`s size cannot be greater than the maximum one. Please, change it in the `EDGES AND DETAILS ENHANCEMENT`panel', 'Info'));
    validEnhancement = 0;
end

% We can continue only if the radius is valid for the processing
if validEnhancement == 1

    % Process the image directly if real time is activated
   if handles.activeRealTime == 1 
       
        % Show the message PROCESSING...
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)

        % Enhancing is done over the denoised image. If it is empty, it
        % should be equal to the original image
        if isempty (handles.denoisedImage)
            handles.denoisedImage = handles.image;
        end
        
        % Processing the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);

        % Enable the fix and delete buttons
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        
        % Processed image's name
        handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
        
        % We select the current limits in case the image has been zoomed and panned
        xLim = get(gca,'XLim');
        yLim = get(gca,'YLim');
        handles.factorZoom = [xLim yLim];

        % Show the new processed image
        axes(handles.axes1);
        %[bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        set(handles.text_nameRight, 'String', handles.nameProcessedImage);
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 

        % Processing finished
        set (handles.text_processing, 'Visible', 'off')
        set (handles.pushbutton_wlOr, 'Visible', 'on')
        handles.nameProtocol = ''; % Reset
        handles.panelChange = [0 0 0]; % No panel has been changed
   else
       handles.panelChange(3) = 1; % Enhancing panel has been changed
   end

end

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% BUTTON TO PROCESS THE IMAGES

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_process.
function pushbutton_process_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_process (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Before the processing, we assure if the image has been loaded before
if isempty (handles.image)
    uiwait(msgbox('Load a DICOM image to be processed', 'Info'))
else
    % Show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Enable', 'on', 'Visible','off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)
    
    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    
    % Reset the protocol
    handles.nameProtocol = ''; %Reset
    
    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];
            
    % If the contrast has been modified, the processing should be done from the beginning
    if handles.panelChange(1) == 1
        % Improving the contrast of the image
        [equalizedImage, handles] = equalizeImage (handles);
        handles.equalizedImage = uint16(equalizedImage);
        % Denoising the image
        [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
        handles.denoisedImage = denoisedImage;
        handles.cDenoised = cDenoised;
        handles.sDenoised = sDenoised;
        % Enhancing the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);
        
    % If the denoising has been modified, the processing should be done from this step   
    elseif handles.panelChange(2) == 1
        
        % Denoising is done over the equalized image so, in case no equalization has been done before, 
        % the equalized image = original image
        if isempty(handles.equalizedImage)
            handles.equalizedImage = handles.image;
        end
        % Denoising the image
        [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
        handles.denoisedImage = denoisedImage;
        handles.cDenoised = cDenoised;
        handles.sDenoised = sDenoised;
        % Enhancing the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);
        
    % If the enhancing has been modified, the processing should be done from this step
    else
        % Enhancing is done over the denoised image so, in case no denoising has been done before, 
        % the denoised image = original image
        if isempty(handles.denoisedImage)
            handles.denoisedImage = handles.image;
        end
        % Enhancing the image
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);
    end

     % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    % Show the new processed image            
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage); 
    imshow(handles.processedImage,[minIm maxIm])
    set(handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Reseting the control parameters
    handles.newLoaded = 0;
    handles.panelChange = [0 0 0];

    % Enable/disable the interface's elements
    set (handles.pushbutton_saveProtocol, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_save,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.activateRealTime, 'Enable', 'on', 'Value', 0)
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Enable', 'on', 'Visible','on')
end

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% AUXILIAR FUNCTIONS TO PROCESS THE IMAGES

% **********************************************************************************************************************************************************************************************************

function [equalizedImage, handles] = equalizeImage (handles)

handles.image = uint16(handles.image);

% Searching the percentage of equalization
typeEqualization = get (handles.popupmenu_typeEqualization, 'Value');
factor = get (handles.slider_contrast, 'Value');
percentage = num2str(round(factor*100));
set(handles.text_contrastPercentage, 'String', [num2str(percentage) '%']);
   
if typeEqualization == 1 % Sub-histogram intensity correction
    [equalizedImage, factorSoft, factorDense] = subHistCorr (handles.image, handles.curveSoft, handles.curveDense, handles.levelSoft, handles.levelDense, handles.curveGammaSoft, handles.curveGammaDense, handles.levelGammaSoft, handles.levelGammaDense, handles.windowLinealSoft, handles.levelLinealSoft, handles.windowLinealDense, handles.levelLinealDense, handles.algSoft, handles.algDense, handles.factorSoft, handles.factorDense, handles.minHist);    
    handles.factorSoft = factorSoft;
    handles.factorDense = factorDense;
    handles.nameTypeContrast = ['CSH_^as' num2str(handles.algSoft) 'ad' num2str(handles.algDense) 'cs' num2str(handles.curveSoft) 'cd' num2str(handles.curveDense)...
        'ls' num2str(handles.levelSoft) 'ld' num2str(handles.levelDense) 'cgs' num2str(handles.curveGammaSoft) 'cgd' num2str(handles.curveGammaDense) 'lgs' num2str(handles.levelGammaSoft)...
        'lgd' num2str(handles.levelGammaDense) 'wls' num2str(handles.windowLinealSoft) 'wld' num2str(handles.windowLinealDense) 'lls' num2str(handles.levelLinealSoft) 'lld' num2str(handles.levelLinealDense) ...
        'fs' num2str(handles.factorSoft) 'fd' num2str(handles.factorDense) '%0^'];

elseif typeEqualization == 2 % General equalization
    equalizedImage = clipHistEq (handles.image,factor,handles.minHist);
    handles.nameTypeContrast = ['CGe_min' num2str(handles.minHist) '_%' num2str(percentage)];
    
elseif typeEqualization == 3 % Gamma correction
    if factor >= 0.5 % Dense Tissues
        gamma = (factor-0.5) * 8 + 1;
        percGamma = (factor-0.5)*2*100;
        if factor > 0.5
            set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Dense']);
        else
            set(handles.text_contrastPercentage, 'String', 'No contrast modification');
        end
    else % Soft tissues
        gamma = (factor*8)/5 + 1/5;
        percGamma = (1-factor*2)*100;
        set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Soft']);

    end
    maxIm = max(handles.image(:));
    minIm = min(handles.image(:));
    low_in = handles.minHist;
    equalizedImage = gamma_Correction (handles.image, gamma, low_in, maxIm,  minIm);
    
    handles.nameTypeContrast = ['CGm_min' num2str(handles.minHist) '_%' num2str(percentage)];
    
else %None
    handles.nameTypeContrast = 'CNo_%0';
    equalizedImage = handles.image;
end

% Values modification
maxEqualized = max(max(equalizedImage));
maxImage = max(max(handles.image));
handles.equalizedImage = uint16((single(equalizedImage)/single(maxEqualized))*single(maxImage));

% Update handles structure
guidata(handles.figure1,handles);


function [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles)

typeNoise = get (handles.popupmenu_typeNoise, 'Value');

% Initialization of the returned variables
cDenoised = [];
sDenoised = [];
handles.thresholdN = [];
factor = get(handles.slider_noise, 'Value');
percentage = [num2str(round(factor*100)) '%  '];
set(handles.text_percentageNoise, 'String', percentage)

switch (typeNoise)
    case 1 % Wavelet transform 
        
        % First, we take the threshold value and modify the string to show
        % a proper name of it
        handles.thresholdN = get(handles.slider_th, 'Value');
        
        if handles.thresholdN == 0
            stringTh = '0.00';
        elseif handles.thresholdN == 1
            stringTh = '1.00';
        else
            stringTh = num2str(handles.thresholdN,2);
            if length (stringTh)<4
                stringTh = [stringTh '0'];
                if length (stringTh)<4
                    stringTh = [stringTh '0'];
                end
            end
        end

        % We take the level of decomposition (from 1 to 8)
        if factor <= 0.125
            handles.level = 1;
        elseif (factor>0.125)&&(factor<=0.25)
            handles.level = 2;
        elseif (factor>0.25)&&(factor<=0.375)
            handles.level = 3;
        elseif (factor>0.375)&&(factor<=0.5)
            handles.level = 4;
        elseif (factor>0.5)&&(factor<=0.625)
            handles.level = 5;
        elseif (factor>0.625)&&(factor<=0.75)
            handles.level = 6;
        elseif (factor > 0.75) && (factor <= 0.875)
            handles.level = 7;
        else
            handles.level = 8;
        end      
      
        % Function to denoise the image
        [cDenoised, sDenoised, denoisedImage] = denoiseWavelet (handles.equalizedImage, handles.level, handles.wavelet, factor, handles.thresholdN);
        % Name
        handles.nameTypeNoise = ['_NWa' '_th' stringTh '%' percentage(1,1:end-3)];
      
    case 2 % Laplacian pyramid decomposition
        denoisedImage = laplacianPyramidDenoise (handles.equalizedImage, factor);
        handles.nameTypeNoise = ['_NPy%' percentage(1,1:end-3)];
   
    case 3 % NLM
        newImage = single(handles.equalizedImage)/single(max(handles.equalizedImage(:))); % Image normalization
        [~, denoisedImage] = BM3D(1,newImage, factor*50);
        denoisedImage = denoisedImage*single(max(handles.equalizedImage(:)));
        handles.nameTypeNoise = ['_NNl%' percentage(1,1:end-3)];
        
    case 4 % None
        denoisedImage = handles.equalizedImage;
        handles.nameTypeNoise = '_NNo%0';
end

% Values modification
maxDenoised = max(max(denoisedImage));
maxImage = max(max(handles.image));
handles.denoisedImage = uint16((single(denoisedImage)/single(maxDenoised))*single(maxImage));

% Update handles structure
guidata(handles.figure1,handles);
                 

function [processedImage, handles] = enhanceImage (handles)

% We are going to independize the wavelet decomposition and the enhancing algorithms.
cDenoised = [];
sDenoised = [];

% Type of processing
typeProcessing = get(handles.popupmenu_typeEnhancement, 'Value');

% We get the enhancement factor and we convert it into a percentage that is going to be shown
factor = get(handles.slider_enhancement,'Value');
percentage = num2str(round(factor*100));
set (handles.text_enhancementPercentage, 'String', [percentage '%'])

if typeProcessing == 1
    %USM in spatial Domain
    radius = get (handles.edit_radius, 'String');
    processedImage = usmSpaDom(handles.denoisedImage, str2double(radius), factor);
    handles.nameTypeEnhancement = ['_EUs' '_R' radius '_%' percentage];
    
elseif typeProcessing == 2
    %USM in wavelet Domain
    level = 2; %We force the level to avoid artifacts
    radius = get (handles.edit_radius, 'String');
    processedImage = usmWavDom(cDenoised, sDenoised, handles.denoisedImage, str2double(radius), factor, level, handles.wavelet);
    handles.nameTypeEnhancement = ['_EUw' '_R' radius '_%' percentage];
    
elseif typeProcessing == 3
    % Morphological processing in wavelet domain
    level = 2; %We force the level to avoid artifacts
    seSize = get(handles.edit_seSize, 'String');
    minSeSize = get(handles.edit_minSe, 'String');
    se = 'disk';
    processedImage = morphoWavDom(cDenoised, sDenoised, handles.denoisedImage, factor, level, handles.wavelet, str2double(minSeSize), str2double(seSize), se);
    handles.nameTypeEnhancement = ['_EMw_sMi' minSeSize '_sMa' seSize '_%' percentage];
    
else
    handles.nameTypeEnhancement = '_ENo_%0';
    processedImage = handles.denoisedImage;
    
end

% Values modification
maxProcessed= max(processedImage(:));
maxImage = max(handles.image(:));
handles.processedImage = uint16((single(processedImage)/single(maxProcessed))*single(maxImage));

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% AUXILIAR FUNCTIONS TO IMPROVE THE IMAGE CONTRAST

% **********************************************************************************************************************************************************************************************************
function handles = subHistogramCorrection(handles)
% ................................................................................
% Change the curvature and the position of the sigma and/or gamma functions
% ................................................................................

% Send the image and the function parameters to the secondary window
setappdata (0, 'image', handles.image)
setappdata (0, 'valueCurveSoft', handles.curveSoft) %Sigma curve soft
setappdata (0, 'valueLevelSoft', handles.levelSoft) %Sigma level soft
setappdata (0, 'valueCurveDense', handles.curveDense) %Sigma curve dense
setappdata (0, 'valueLevelDense', handles.levelDense) %Sigma level dense
setappdata (0, 'valueCurveGammaSoft', handles.curveGammaSoft) %Gamma curve soft
setappdata (0, 'valueCurveGammaDense', handles.curveGammaDense) %Gamma curve dense
setappdata (0, 'valueLevelGammaSoft', handles.levelGammaSoft) %Gamma level soft
setappdata (0, 'valueLevelGammaDense', handles.levelGammaDense) %Gamma level dense
setappdata (0, 'valueLevelDense', handles.levelDense) %Sigma level dense
setappdata (0, 'valueWindowLinealSoft', handles.windowLinealSoft) %Lineal window soft
setappdata (0, 'valueWindowLinealDense', handles.windowLinealDense) %Lineal window dense
setappdata (0, 'valueLevelLinealSoft', handles.levelLinealSoft) %Lineal level soft
setappdata (0, 'valueLevelLinealDense', handles.levelLinealDense) %Lineal level dense
setappdata (0, 'valueFactorSoft', handles.factorSoft) %Weight for the soft image
setappdata (0, 'valueFactorDense', handles.factorDense) %Weight for the dense image
setappdata (0, 'algSoft', handles.algSoft) %Sigma (1) or gamma (2) selected for soft tissues
setappdata (0, 'algDense', handles.algDense) %Sigma (1) or gamma (2) selected for dense tissues
setappdata (0, 'background', handles.minHist) %Background selected by the user
setappdata (0, 'eqIm', [])

% Call the secondary window
subHistCorrection
uiwait
    % Update the functions parameters from the secondary window
    handles.curveSoft = getappdata (0,'valueCurveSoft');
    handles.curveDense = getappdata (0,'valueCurveDense');
    handles.levelSoft = getappdata (0,'valueLevelSoft');
    handles.levelDense = getappdata (0,'valueLevelDense');
    handles.curveGammaSoft = getappdata (0,'valueCurveGammaSoft');
    handles.curveGammaDense = getappdata (0,'valueCurveGammaDense');
    handles.levelGammaSoft = getappdata (0,'valueLevelGammaSoft');
    handles.levelGammaDense = getappdata (0,'valueLevelGammaDense');    
    handles.levelLinealSoft = getappdata (0,'valueLevelLinealSoft');
    handles.levelLinealDense = getappdata (0,'valueLevelLinealDense');
    handles.windowLinealSoft = getappdata (0,'valueWindowLinealSoft');
    handles.windowLinealDense = getappdata (0,'valueWindowLinealDense');    
    handles.algSoft = getappdata (0,'algSoft');
    handles.algDense = getappdata (0,'algDense');
    handles.factorSoft = getappdata (0,'valueFactorSoft');
    handles.factorDense = getappdata (0,'valueFactorDense');
    eqIm = getappdata (0,'eqIm');

   
if ~isempty(eqIm)
    try
        % Show the message PROCESSING...
        set (handles.pushbutton_wlOr, 'Visible', 'off')
        set (handles.text_processing, 'Visible', 'on')
        uiwait (handles.figure1,1)

        % We equalize the image and show it in the right box
        [equalizedImage, handles] = equalizeImage (handles);
        handles.equalizedImage = uint16(equalizedImage);
        [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
        handles.denoisedImage = denoisedImage;
        handles.cDenoised = cDenoised;
        handles.sDenoised = sDenoised;
        [processedImage, handles] = enhanceImage (handles);
        handles.processedImage = uint16(processedImage);

        % Enable the fix and delete buttons
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])

        % Processed image's name
        handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

        %We select the current limits in case the image has been zoomed and panned
        xLim = get(gca,'XLim');
        yLim = get(gca,'YLim');
        handles.factorZoom = [xLim yLim];

        % Show the new processed image
        axes(handles.axes1);
        %[bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        set(handles.text_nameRight, 'String', handles.nameProcessedImage);
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 

        % Processing finished
        set (handles.text_processing, 'Visible', 'off')
        set (handles.pushbutton_wlOr, 'Visible', 'on')
        handles.nameProtocol = ''; % Reset
        handles.panelChange =  [0 0 0]; % No panel has been changed
    catch me
        return
    end
end

% Update handles
guidata(handles.figure1,handles);

function handles = background(handles)

% .........................................................................................
% Select a threshold to separate the background manually (background will not be processed)
% .........................................................................................

% Flush the interface
drawnow

% Send the type of contrast improvement algorithm (gamma correction or equalization)
% to the secondary window
typeContrast = get(handles.popupmenu_typeEqualization, 'Value');
setappdata(0,'type',typeContrast)

% Send the percentage of improvement to the secondary window
handles.sliderContrast = get(handles.slider_contrast,'Value');
setappdata (0, 'slider', handles.sliderContrast)

% Send the image and the current threshold to the secondary window
setappdata (0,'image',handles.image)
setappdata (0, 'minHis', handles.minHist)

% Call the window to select the background threshold
backgroundThreshold
uiwait
drawnow

% Get the new information from the secondary window 
newMin = getappdata (0,'min');
if ~isempty (newMin)
    handles.minHist = newMin;
end
handles.sliderContrast = getappdata (0,'slider');

% Depending on the algorithm, we need different texts and slider meanings
set(handles.slider_contrast, 'Value', handles.sliderContrast)
typeEq = get(handles.popupmenu_typeEqualization, 'Value');
levelContrast = get (handles.slider_contrast, 'Value');

if typeEq == 3 % Gamma Correction
     if levelContrast >= 0.5
            percGamma = (levelContrast-0.5)*2*100;
        if levelContrast > 0.5
            set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Dense']);
        else
            set(handles.text_contrastPercentage, 'String', 'No contrast modification');
        end
     else 
        percGamma = (1-levelContrast*2)*100;
        set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Soft']);
     end
else % Histogram Equalization
     percContrast = round(levelContrast*100);
     set(handles.text_contrastPercentage, 'String', [num2str(percContrast) '%']);
end

% Process the image directly if real time is activated
if handles.activeRealTime == 1

    % Show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Visible', 'off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)
    
    % We equalize the image and show it in the right box        
    [equalizedImage, handles] = equalizeImage (handles);
    handles.equalizedImage = uint16(equalizedImage);
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = denoisedImage;
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);

    % Enable the fix and delete buttons
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        
    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show new processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));

    % Processing finished
    set (handles.text_processing, 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    handles.nameProtocol = ''; %Reset
    handles.panelChange =  [0 0 0]; % No panel has been changed
else
    handles.panelChange(1) = 1; % The contrast panel has been changed
end

% Update the handles structure
guidata(handles.figure1,handles);


% **********************************************************************************************************************************************************************************************************

% LOAD AND SAVE THE IMAGES

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_load.
function pushbutton_load_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Reset ALL
set(handles.text_processing,'Visible', 'off')
clear handles.image handles.equalizedImage handles.denoisedImage handles.processedImage handles.modifiedImage handles.ROI handles.rectangle handles.processedImages handles.names
handles.newLoaded = 1;
handles.nProcImages = 0;
handles.indexProcImages = 0;
handles.ROI = [];
handles.info = '';
handles.nameProtocol = '';
handles.nameROIcropped = '';
handles.nameBodyPart = '#NO_';
handles.detailActive = 0;
handles.rectangle = [];
handles.minHist = 0;
handles.factorSoft = 0;
handles.factorDense = 0;
iptsetpref('ImshowAxesVisible','off');
set(handles.text_index, 'String', ' ')

% Dialog box to select the image to be processed
[filename,pathname]=uigetfile('\*.*','Select a DICOM image to be processed', [pwd '\No_Processed_Images\']);
if filename==0
    return
end

% We try to read the image. If dicomread doesn't work, a message appears
try
    handles.image = dicomread(fullfile(pathname, filename));
    handles.image = imrotate(handles.image,-90);
    handles.originalFilePath = fullfile(pathname, filename);
    handles.imageNoCrop = handles.image;
    handles.modifiedImage = handles.image;
    handles.imageCropped = handles.image;
    handles.equalizedImage = handles.image;
    handles.denoisedImage = handles.image;
    handles.processedImage = handles.image;
    handles.processedImages = [];
    handles.irradiatedField = [1 1 size(handles.image,2) size(handles.image,1)];
    handles.nameImage = filename;
    handles.nameProcessedImage = ' ';
    handles.names = [];
        
catch me
    uiwait(msgbox('The image has not been loaded', 'Info'))
    return
end

% If the file that has been read is not a DICOM format, a message appears
if isempty(handles.image)
    uiwait(msgbox('The specified file is not a DICOM format', 'Info'))
    return
end

% Try to read the DICOM info of the image. If it does not work, a message appears
try
    info = dicominfo (fullfile(pathname, filename));
    handles.dicomInformation = info;
catch me 
    uiwait(msgbox('The specified file is not in DICOM format', 'Info'))
    return
end

% Check if the field ImageType is included or not in the info. In case we
% don't have it, we have to create it because it is necessary for the following steps.
namesInfo = fieldnames(info);
imtype = strcmp(namesInfo, 'ImageType');
if isempty (find(imtype, 1))
    imagetype = struct ('ImageType', 'ORIGINAL');
    names = [fieldnames(info); fieldnames(imagetype)];
    handles.dicomInformation = cell2struct([struct2cell(info); struct2cell(imagetype)], names, 1);
end

% Load the parameters if the loaded image has been processed before
[ROIcropped, bp, contrastFilter, percentageC, minGrey, cSoft, cDense, lSoft, lDense, gcSoft, gcDense, glSoft, glDense, wlSoft, wlDense, llSoft, llDense, fSoft, fDense, aSoft, aDense, noiseFilter, percentageN, thresholdN, enhanceFilter, percentageE, minSizeSE, maxSizeSE, radius] = loadParameters (handles.dicomInformation.ImageType);

if (~strcmp(handles.dicomInformation.ImageType, 'ORIGINAL'))&&(~isempty(noiseFilter))&&(~isempty(enhanceFilter))&&(~isempty(contrastFilter))
        
    % ................................................................................
    % LOAD THE PROCESSED IMAGE
    % ................................................................................
    % Show the message LOADING to inform the user the image is being prepared
    set(handles.text_processing, 'String', 'LOADING...', 'Visible', 'on')
    set(handles.pushbutton_wlOr, 'Visible', 'off')
    
    axes(handles.axes2)
    cla
    axes(handles.axes1)
    cla

    % The loaded image is an image processed before
    handles.processedImage = handles.image;
    handles.nameProcessedImage = handles.dicomInformation.ImageType;
    
    % Read the handles.nameImage.txt to obtain the path and the name of the original image
    [pathOriginalImage, nameOriginalImage] = searchOriginalPath ([pathname handles.nameImage '.txt']);
    handles.originalFilePath = pathOriginalImage;

    try
        auxPath = changePath (pathOriginalImage); % First of all, we have to change original image path (in case other user processed the image)
        clear pathOriginalImage
        pathOriginalImage = auxPath;
        % We try to read the image. If dicomread doesn't work, a message appears
        handles.image = dicomread(pathOriginalImage);
        handles.imageNoCrop = handles.image;
        handles.modifiedImage = handles.image;
        handles.imageCropped = handles.image;
        handles.equalizedImage = handles.image;
        handles.denoisedImage = handles.image;
        handles.nameImage = nameOriginalImage;
        % Dicom info and name of the original image
        handles.dicomInformation = dicominfo(pathOriginalImage);
        names = fieldnames(handles.dicomInformation);
        infocell = struct2cell (handles.dicomInformation);
        infoDICOM = [names infocell];    
        setappdata(0,'info',infoDICOM) % Send the DICOM info to the secondary window
        

        % ................................................................................
        % CROP THE IMAGE
        % ................................................................................
        if ~isempty(ROIcropped)
            auxImage = handles.image;
            handles.imageNoCrop = auxImage;
            [rw,cl]=size(auxImage);
            xmin = ROIcropped(1);
            ymin = ROIcropped(2);
            width = ROIcropped(3);
            height = ROIcropped(4);
            back = ROIcropped(5);
            if ymin < 1
                ymin = 1;
            end
            if xmin < 1
                xmin = 1;
            end
            ymax = ymin+height;
            if ymax > rw
                ymax = rw;
            end
            xmax=xmin+width;
            if xmax > cl
                xmax = cl;
            end
            handles.image = auxImage(ymin:ymax, xmin:xmax);
            handles.imageCropped = handles.image;
        end

        % ................................................................................
        % INTENSITY CORRECTION
        % ................................................................................
        switch (bp)
            case('XP')
                imRef = dicomread ([pwd '\Database\ChestPA']);
                low_th = 1;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 2;
            case('XL')
                imRef = dicomread ([pwd '\Database\ChestLAT']);
                low_th = 78;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 3;
            case('AP')
                imRef = dicomread ([pwd '\Database\AbdPA']);
                low_th = 1;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 4;
            case('AL')
                imRef = dicomread ([pwd '\Database\AbdLAT']);
                low_th = 1;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 5;
            case('KP') 
                imRef = dicomread ([pwd '\Database\ChestAbdPA']);
                low_th = 50;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 6;
            case('KL')
                imRef = dicomread ([pwd '\Database\ChestAbdLAT']);
                low_th = 110;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 7;
            case('SP')
                imRef = dicomread ([pwd '\Database\SkullPA']);
                low_th = 1;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 8;
            case('SL')
                imRef = dicomread ([pwd '\Database\SkullLAT']);
                low_th = 15;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 9;
            case('SN')
                imRef = dicomread ([pwd '\Database\Spine']);
                low_th = 250;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 10;
            case('RV')
                imRef = dicomread ([pwd '\Database\Cervical']);
                low_th = 1;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 11;
            case('HI')
                imRef = dicomread ([pwd '\Database\Hips']);
                low_th = 5;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 12;
            case('LB')
                imRef = dicomread ([pwd '\Database\Limbs']);
                low_th = 210;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 13;
            case('LI')
                imRef = dicomread ([pwd '\Database\LimbsImplants']);
                low_th = 186;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 14;
            case('PW')
                imRef = dicomread ([pwd '\Database\Paws']);
                low_th = 300;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 15;
            case('SH')
                imRef = dicomread ([pwd '\Database\Shoulder']);
                low_th = 200;
                modifiedImage = histogramMatch (handles.imageCropped, imRef, low_th, back);
                option = 14;
            otherwise
                modifiedImage = handles.imageCropped;
                option = 1;
        end
        handles.modifiedImage = modifiedImage;
        handles.image = modifiedImage;
        handles.nameBodyPart = ['#' bp '_'];
        set (handles.popupmenu_refIm, 'Value', option);
        
    catch me
        uiwait(msgbox('The image has not been loaded', 'Info'))
        return
    end

    % ................................................................................
    % PERCENTAGES
    % ................................................................................
    set (handles.text_contrastPercentage, 'String', [num2str(round(percentageC)) '%'])
    set (handles.slider_contrast, 'Value', percentageC/100)
    set (handles.text_percentageNoise, 'Enable', 'on', 'String', [num2str(round(percentageN)) '%  '])
    set (handles.slider_noise, 'Enable', 'on', 'Value', percentageN/100)
    set (handles.text_enhancementPercentage, 'String', [num2str(round(percentageE)) '%'])
    set (handles.slider_enhancement, 'Value', percentageE/100)
    
    % ................................................................................
    % CONTRAST PARAMETERS
    % ................................................................................
    % Load the type of contrast filter and its parameters
    set (handles.popupmenu_typeEqualization, 'Enable', 'on', 'Value', contrastFilter)
    
    % Load contrast parameters
    if ~isempty(minGrey)
        handles.minHist = minGrey;
    end

    switch (contrastFilter)
        case (1) %Sub-histogram intensity correction
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'off')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'off', 'String', ' -              Image`s contrast              + ')
            set (handles.text_contrastPercentage, 'Visible','on', 'Enable', 'off', 'String', '0%');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Select the functions parameters')
            handles.curveSoft = cSoft;
            handles.curveDense = cDense;
            handles.levelSoft = lSoft;
            handles.levelDense = lDense;
            handles.factorSoft = fSoft;
            handles.factorDense = fDense;
            handles.curveGammaSoft = gcSoft;
            handles.curveGammaDense = gcDense;
            handles.levelGammaSoft = glSoft;
            handles.levelGammaDense = glDense;
            handles.levelLinealSoft = llSoft;
            handles.levelLinealDense = llDense;
            handles.windowLinealSoft = wlSoft;
            handles.windowLinealDense = wlDense;
            handles.algSoft = aSoft;
            handles.algDense = aDense;
            
        case (2) % General equalization
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on', 'String', ' -              Image`s contrast              + ')
            set (handles.text_contrastPercentage, 'Visible','on', 'Enable', 'on');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background')

        case (3) % Gamma Correction
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on','String', ' Soft     <---     TISSUE     --->      Dense ')
            set (handles.text_contrastPercentage, 'Visible','on', 'Enable', 'on');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background')
            levelContrast = percentageC/100;
            if levelContrast >= 0.5
                percGamma = (levelContrast-0.5)*2*100;
                if levelContrast > 0.5
                    set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Dense']);
                else
                    set(handles.text_contrastPercentage, 'String', 'No contrast modification');
                end
            else
                percGamma = (1-levelContrast*2)*100;
                set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Soft']);
            end
            
        otherwise
            set (handles.slider_contrast, 'Visible','on','Enable','off')
            set (handles.text_contrast, 'String', ' -              Image`s contrast              + ', 'Enable', 'off')
            set (handles.text_contrastPercentage, 'String', '0%', 'Enable', 'off');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','off','String','Eliminate the background')
    end

    % ................................................................................
    % DENOISING PARAMETERS
    % ................................................................................
    % Load the type of DENOISING filter and its parameters
    set (handles.popupmenu_typeNoise, 'Enable', 'on', 'Value', noiseFilter)

    if (noiseFilter==1)
        set (handles.slider_noise, 'Enable','on','SliderStep', [0.125 0.125], 'Position', [0.568 0.639 0.385 0.186])
        set (handles.slider_th, 'Visible', 'on', 'Enable', 'on', 'Value', thresholdN)
        if thresholdN < 0.25
            newFact = thresholdN*4;
        elseif thresholdN == 0.25
            newFact = [];
        else
            newFact = 12*(thresholdN-0.25)+1;
        end
        if newFact == 1
            percentageTh = ' th.';
        else
            percentageTh = [num2str(newFact,2) '* th.'];
        end
        set (handles.text_denoising, 'Enable','on','Position', [0.587 0.835 0.364 0.186])
        set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.639 0.061 0.186])
        set (handles.text_th, 'Visible', 'on','Enable','on', 'String', percentageTh)
        set (handles.text_thresh, 'Visible', 'on', 'Enable','on','Position', [0.586 0.103 0.358 0.186], 'String', ' -             | Optimal edges threshold          +')

    elseif noiseFilter == 4
        set (handles.slider_noise, 'Position', [0.568 0.429 0.385 0.186], 'Enable', 'off')
        set (handles.slider_th, 'Visible', 'off')
        set (handles.text_thresh, 'Position', [0.586 0.193 0.358 0.186], 'Enable', 'off',  'String', ' -                      Blurring                            +')
        set (handles.text_th, 'Visible', 'off')
        set (handles.text_denoising, 'Position', [0.587 0.625 0.364 0.186], 'Enable', 'off')
        set (handles.text_percentageNoise, 'Position', [0.506 0.398 0.061 0.186], 'Enable','off')

    else
        set (handles.slider_noise, 'Enable','on','SliderStep', [0.01 0.01], 'Position', [0.568 0.429 0.385 0.186])
        set (handles.slider_th, 'Visible', 'off')
        set (handles.text_thresh, 'Enable','on','Position', [0.586 0.193 0.358 0.186], 'String', ' -                      Blurring                            +')
        set (handles.text_th, 'Visible', 'off')
        set (handles.text_denoising, 'Enable','on','Position', [0.587 0.625 0.364 0.186])
        set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.398 0.061 0.186])
    end

    % ................................................................................
    % EDGES AND DETAILS ENHANCEMENT PARAMETERS
    % ................................................................................
    % Load the type of contrast filter and its parameters
    set (handles.popupmenu_typeEnhancement, 'Visible', 'on', 'Enable', 'on', 'Value', enhanceFilter)
    set (handles.slider_enhancement, 'Enable', 'on')

    if (enhanceFilter == 1) || (enhanceFilter == 2) % USM
        set (handles.edit_seSize, 'Visible', 'off')
        set (handles.edit_minSe, 'Visible', 'off', 'Value', minSizeSE)
        set (handles.edit_radius, 'Visible', 'on', 'Enable', 'on', 'String', radius)
        set (handles.text_se, 'Visible', 'off')
        set (handles.text_radius, 'Visible', 'on', 'Enable', 'on')
        set (handles.text_enhance, 'Enable', 'on')
        set (handles.text_enhancementPercentage, 'Enable', 'on')
        
    elseif enhanceFilter == 3
        set (handles.edit_seSize, 'Visible', 'on', 'Enable', 'on', 'String', maxSizeSE)
        set (handles.edit_minSe, 'Visible', 'on', 'Enable', 'on','String', minSizeSE)
        set (handles.edit_radius, 'Visible', 'off')
        set (handles.text_se, 'Visible', 'on', 'Enable', 'on')
        set (handles.text_enhance, 'Enable', 'on')
        set (handles.text_enhancementPercentage, 'Enable', 'on')
        set (handles.text_radius, 'Visible', 'off')

    else
        set (handles.edit_seSize, 'Visible', 'off')
        set (handles.edit_minSe, 'Visible', 'off')
        set (handles.edit_radius, 'Visible', 'on', 'Enable', 'off')
        set (handles.text_se, 'Visible', 'off')
        set (handles.text_radius, 'Visible', 'on', 'Enable', 'off')
        set (handles.text_enhance, 'Enable', 'off')
        set (handles.text_enhancementPercentage, 'Enable', 'off')
    end

    % ................................................................................
    % IMAGE PROCESSING
    % ................................................................................
    % Equalizing the image
    [equalizedImage, handles] = equalizeImage (handles);
    handles.equalizedImage = uint16(equalizedImage);

    % Denoising image
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;

    % Enhancing the denoised image
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);
    
    % Enable/disable the compare options
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.original, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
    set (handles.pushbutton_next, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
    set (handles.pushbutton_previous, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
    set(handles.text_nameRight, 'String', handles.nameProcessedImage)

    % Show the original and processed images
    axes(handles.axes2)
    % Outliers
    %[bins,~] = imhist(handles.image,2^16);
    [minIm, maxIm] = imageRange(handles.image);
    handles.minOriginal = minIm;
    handles.maxOriginal = maxIm;   
    imshow(handles.image,[handles.minOriginal handles.maxOriginal])
    set(handles.text_nameLeft, 'String', handles.nameImage)
    axes(handles.axes1)
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])

    % ................................................................................
    % ENABLE/DISABLE THE ELEMENTS OF THE INTERFACE
    % ................................................................................
    set (handles.pushbutton_selectROI, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
    set (handles.pushbutton_okChange, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
    set (handles.pushbutton_skip, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
    set (handles.pushbutton_info,  'Enable', 'on', 'BackgroundColor', [1.0, 1.0, 1.0])
    set (handles.pushbutton_zoom_in,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_zoom_out,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_noZoom,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_saveProtocol,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_loadProtocol,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_process, 'Enable', 'on', 'BackgroundColor', [0.882, 0.882, 0.969])
    set (handles.pushbutton_save,  'BackgroundColor', [0.729, 0.831, 0.957], 'Enable', 'on')
    set (handles.pushbutton_helpContrast, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
    set (handles.pushbutton_helpNoise, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
    set (handles.pushbutton_helpEnhancement, 'Enable', 'on', 'BackgroundColor', [0.953, 0.871, 0.733])
    set (handles.pushbutton_change, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_info, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_wlOr, 'Visible', 'on', 'Enable', 'on')
    set (handles.pushbutton_wl, 'Visible', 'on', 'Enable', 'on')
    set (handles.pushbutton_amplify, 'Visible', 'on', 'Enable', 'on')
    set (handles.pushbutton_amplifyOriginal, 'Visible', 'on', 'Enable', 'on')
    set (handles.text_selectBody, 'Enable', 'off')
    set (handles.text_processing, 'String', 'PROCESSING', 'Visible', 'off')
    set (handles.popupmenu_refIm, 'Enable', 'off')
    set (handles.activateRealTime, 'Enable', 'on', 'String', ' No Real Time', 'BackgroundColor', [0.95 0.80 0.80], 'ForegroundColor', [0.899 0.0 0.0])

        
else% When the loaded image has not been processed before
      
    % Reset image
    axes(handles.axes1)
    cla
    axes(handles.axes2)
    cla
    
    % Get the DICOM info of the loaded image
    names = fieldnames(info);
    infocell = struct2cell (info);
    infoDICOM = [names infocell];
    setappdata(0,'info',infoDICOM) % We send the information to the secondary window (cell array, struct cannot be passed)
    
    % Reset the intermediate images
    handles.equalizedImage = handles.image;
    handles.denoisedImage = handles.image;
    handles.processedImage = handles.image;
    
    % Show the loaded image in both axes
    axes(handles.axes2);
    if isempty(handles.image)
        uiwait(msgbox('The file you have selected is not an appropriate image', 'Info'))
        set (handles.pushbutton_save,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set (handles.pushbutton_info,  'BackgroundColor', [0.941, 0.941, 0.941],'Enable', 'off')
    else
        % Outliers
        %[bins,~] = imhist(handles.image,2^16);
        [minIm, maxIm] = imageRange(handles.image);
        handles.minOriginal = minIm;
        handles.maxOriginal = maxIm;     
        imshow(handles.image,[handles.minOriginal handles.maxOriginal])
        set (handles.text_nameLeft, 'String', filename);
        axes(handles.axes1);
        [bins,~] = imhist(handles.processedImage,2^16);
        [minIm, maxIm] = imageRange(handles.processedImage);
        imshow(handles.processedImage,[minIm maxIm])
        
        % Reset contrast
        handles.minHist = min(min(handles.image));
        handles.factorSoft = 0;
        handles.factorDense = 0;
        handles.oldSliderContrast = 0;
        set (handles.pushbutton_change, 'Visible', 'on','Enable','off','String','Eliminate the background','BackgroundColor', [0.941, 0.941, 0.941])
        set (handles.pushbutton_helpContrast, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        set (handles.popupmenu_typeEqualization, 'Visible', 'on','Value',4, 'Enable', 'off')
        set (handles.slider_contrast,'Visible','on','Enable','off')
        set (handles.text_contrastPercentage, 'Enable','off','String', '50%')
        set (handles.text_contrast, 'Enable','off', 'String', '-              Image`s contrast              +')

        % Reset noise
        handles.oldSliderNoise = 0;
        set (handles.popupmenu_typeNoise, 'Enable', 'off','Value',4)
        set (handles.slider_noise, 'Value', 0.5,'Enable', 'off')
        set (handles.slider_th, 'Visible', 'off', 'Value', 0.25)
        set (handles.slider_noise, 'SliderStep', [0.125 0.125], 'Position', [0.568 0.429 0.385 0.186])
        set (handles.text_denoising, 'Position', [0.587 0.625 0.364 0.186], 'Enable', 'off')
        set (handles.text_thresh, 'Enable','off','Position', [0.586 0.193 0.358 0.186], 'String', ' -                      Blurring                            +')
        set (handles.text_percentageNoise, 'Position', [0.506 0.398 0.061 0.186], 'Enable','off', 'String', '50%  ')
        set (handles.text_th, 'Visible', 'off', 'Enable','off','String', '* th.')
        set (handles.pushbutton_helpNoise, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        
        % Reset enhancement
        set (handles.popupmenu_typeEnhancement, 'Enable', 'off','Value',4)
        set (handles.slider_enhancement, 'Value', 0.5, 'Enable','off')
        set (handles.edit_radius, 'Visible', 'on', 'Enable', 'off', 'String', '1')
        set (handles.edit_seSize, 'Visible', 'off', 'String', '10')
        set (handles.edit_minSe, 'Visible', 'off', 'String', '5')
        set (handles.text_enhancementPercentage, 'Enable','off','String', '50%')
        set (handles.text_radius, 'Visible', 'on', 'Enable', 'off')
        set (handles.text_se, 'Visible', 'off')
        set (handles.pushbutton_helpEnhancement, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        
        % Reset other interface elements
        set (handles.pushbutton_process, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        set (handles.pushbutton_save,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set (handles.pushbutton_info,  'BackgroundColor', [0.729, 0.831, 0.957],'Enable', 'on')
        set (handles.pushbutton_loadProtocol, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        set (handles.pushbutton_saveProtocol,  'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
        set (handles.pushbutton_zoom_in, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_zoom_out, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_noZoom, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_fix, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_delete, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.text_nameRight, 'String', handles.nameProcessedImage);
        set (handles.activateRealTime, 'Enable', 'off', 'Value', 0, 'String', ' No Real Time', 'BackgroundColor', [0.941, 0.941, 0.941], 'ForegroundColor', [0.9 0.0 0.0])
        set (handles.pushbutton_wlOr, 'Enable', 'on', 'Visible','on')
        set (handles.pushbutton_wl, 'Enable', 'on', 'Visible','on')
        set (handles.pushbutton_amplify, 'Enable', 'on', 'Visible','on')
        set (handles.pushbutton_amplifyOriginal, 'Visible', 'on', 'Enable', 'on')
        handles.activeRealTime = 0;
        
        % Default processed name
        handles.nameTypeContrast = 'CNo_%0';
        handles.nameTypeNoise = '_NNo%0';
        handles.nameTypeEnhancement = '_ENo_%0'; 
        
        % Enable the intensity panel
        set (handles.popupmenu_refIm, 'Enable', 'on', 'Value', 1)
        set (handles.pushbutton_selectROI, 'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
        set (handles.pushbutton_skip, 'Enable', 'on', 'BackgroundColor', [0.937, 0.867, 0.867])
        set (handles.text_selectBody, 'Enable', 'on')
        
        % Enable/disable the compare options
        set (handles.pushbutton_fix, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
        set (handles.pushbutton_delete, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
        set (handles.pushbutton_next, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
        set (handles.pushbutton_previous, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
    
    end
end

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%comprobar si mi imagen procesada est� en la lista. Si no, incluirla

% Copy the list so that we do not modify anything in the interface in case
% the user does not want to save the image finally
listIm = handles.processedImages;
indexIm = handles.indexProcImages;
nIm = handles.nProcImages;
namesIm = handles.names;

% Check if the original image is in the list. It should be deleted.
if (handles.originalIncluded == 1)
    aux = listIm(:,:,2:end);
    for k = 1:nIm-1
        auxNames{1,k} = namesIm{1,k+1};
    end
    clear listIm
    clear namesIm
    listIm = aux;
    namesIm = auxNames;
    clear aux
    % Update the number of images of the list and the index
    nIm = nIm - 1;
    if indexIm ~= 1
        indexIm = indexIm - 1;
    end
end
    
% Check if the current processed image is in the array of processed images
processedIncluded = 0;

for k = 1:nIm
    if handles.processedImage==listIm(:,:,k)
        processedIncluded = 1;
        break
    else
        processedIncluded = 0;
    end
end

% If not, we include it in the list because maybe the user wants to save it
if processedIncluded == 0
    nIm = nIm + 1;
    indexIm = nIm;
    listIm(:,:,nIm) = handles.processedImage;
    namesIm{1,nIm} = handles.nameProcessedImage;
end

if nIm > 1
    % We communicate with the window to save images and protocols. We inform
    % about we are saving an image (not a protocol)
    setappdata(0, 'protocol', 0)
    setappdata(0,'processedImages', listIm);
    setappdata(0,'indexProcImages', indexIm);
    setappdata(0,'nProcImages', nIm);
    
    % Secondary window
    saveImages
    uiwait (saveImages)
    drawnow
    
    % Get the index of the image to be saved
    index = getappdata(0,'index');
    if isempty(index)
        imSave = [];
    else
        imSave = uint16(listIm(:,:,index));
        nameIm = namesIm{1,index};
    end
else
    % If there is only a image to be saved, it is done directly
    imSave = handles.processedImage;
    nameIm = handles.nameProcessedImage;
end

if ~isempty(imSave)
    if handles.newLoaded == 1
        uiwait(msgbox('Please, process the image before saving','Info'))
    else
        % Before saving the image, we call the figure of comments
        comments
        uiwait(comments)

        % We save the comments 
        initImage = getappdata(0,'initImage');
        improvement = getappdata(0,'improvement');
        comment = getappdata(0,'comments');
        author = getappdata(0,'author');
        checkProtocol = getappdata(0,'checkProtocol');
        testValue = [0 0 0];

        % When the user close the comment window without saving anything
        if isequal(initImage,testValue)
            return
        else
            % We update the DICOM field "ImageType" by the code of the used algorithms 
            handles.dicomInformation.ImageType = nameIm;
            drawnow % Function to update the interface so that the uiputfile is runned out even when we have launched the uiwait function

            % Search the folder where we will save the image and the comments
            formats = '*.*';
            [name, path] = uiputfile(formats,'Save the processed image and the feedback', [pwd '\Processed_Images\']); %Without name. The user decides
            if name == 0; return; end
            fName = fullfile(path,name);

            % If dicomwrite fails, a message appears
            try
                dicomwrite(imSave, fName, handles.dicomInformation, 'CreateMode', 'copy', 'WritePrivate', true);
                % If the comments have not been saved, a message appears
                try
                    saveComments (initImage, improvement, comment, author, name, path, handles.originalFilePath, checkProtocol, handles.nameProtocol)
                catch me
                    uiwait(msgbox('The comments have not been saved', 'Info'))
                end
            catch me
                uiwait(msgbox('The image has not been saved', 'Info'))
            end
        end
    end
end

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% LOAD AND SAVE PROTOCOLS

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_loadProtocol.
function pushbutton_loadProtocol_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadProtocol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% We communicate with the window to load/save protocols to indicate we are loading a protocol.
setappdata(0, 'type', 'load')
save_load_protocol % Filter all the protocols to find the one that include all the features selected by the user (type of animal, size and effect in the image)
uiwait(save_load_protocol)

% Get the features of the protocol
animal = getappdata(0,'animal');
sizeAnimal = getappdata(0,'size');
effect = getappdata(0,'effect');
body = getappdata(0,'body');
exotic = getappdata (0,'exotic');
other = getappdata (0, 'other');
part = getappdata (0, 'otherPart');
savedProtocol = 0; % We are loading, not saving

drawnow % Give the control to the interface again

% Create the name of the protocol according to the features selected by the
% user (nameProtocol) and return the categories selected by the user
[nameProtocol, nAnimal, nSize, nBody, nEffect] = buildName(animal, sizeAnimal, effect, body, exotic, other, part, savedProtocol);
protocolFound = findProtocol(nameProtocol);

% Folder where the protocols are saved
if isempty(nameProtocol)
	return
elseif strcmp(nameProtocol, 'all') % Show all protocols
    [filename,pathname]=uigetfile('*.*','Select a protocol to be applied', [pwd '\Protocols\']);
else % The exact protocol was not found so the filtering is done depending on the categories
    if isempty(protocolFound) % Protocols filter
        if isempty(nAnimal)&&isempty(nSize)&&isempty(nBody)&&isempty(nEffect) % 0 0 0 0
            [filename,pathname]=uigetfile('*.*','Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&isempty(nSize)&&isempty(nBody)&&~isempty(nEffect) % 0 0 0 1
            [filename,pathname]=uigetfile({['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&isempty(nSize)&&~isempty(nBody)&&isempty(nEffect) % 0 0 1 0
            [filename,pathname]=uigetfile({['*' nBody '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&isempty(nSize)&&~isempty(nBody)&&~isempty(nEffect) % 0 0 1 1
            [filename,pathname]=uigetfile({['*' nBody '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&~isempty(nSize)&&isempty(nBody)&&isempty(nEffect) % 0 1 0 0
            [filename,pathname]=uigetfile({['*' nSize '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&~isempty(nSize)&&isempty(nBody)&&~isempty(nEffect) % 0 1 0 1
            [filename,pathname]=uigetfile({['*' nSize '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&~isempty(nSize)&&~isempty(nBody)&&isempty(nEffect) % 0 1 1 0
            [filename,pathname]=uigetfile({['*' nSize '*.txt'];['*' nBody '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif isempty(nAnimal)&&~isempty(nSize)&&~isempty(nBody)&&~isempty(nEffect) % 0 1 1 1
            [filename,pathname]=uigetfile({['*' nSize '*.txt'];['*' nBody '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&isempty(nSize)&&isempty(nBody)&&isempty(nEffect) % 1 0 0 0
            [filename,pathname]=uigetfile({[nAnimal '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&isempty(nSize)&&isempty(nBody)&&~isempty(nEffect) % 1 0 0 1
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&isempty(nSize)&&~isempty(nBody)&&isempty(nEffect) % 1 0 1 0
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nBody '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&isempty(nSize)&&~isempty(nBody)&&~isempty(nEffect) % 1 0 1 1
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nBody '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&~isempty(nSize)&&isempty(nBody)&&isempty(nEffect) % 1 1 0 0
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nSize '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&~isempty(nSize)&&isempty(nBody)&&~isempty(nEffect) % 1 1 0 1
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nSize '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        elseif ~isempty(nAnimal)&&~isempty(nSize)&&~isempty(nBody)&&isempty(nEffect) % 1 1 1 0
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nSize '*.txt'];['*' nBody '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
            
        else % 1 1 1 1
            [filename,pathname]=uigetfile({[nAnimal '*.txt'];['*' nSize '*.txt'];['*' nBody '*.txt'];['*' nEffect '*.txt']},'Select a protocol to be applied', [pwd '\Protocols\']);
        end
    else % The exact protocol was found
        [filename,pathname]=uigetfile([protocolFound '.*'],'Select a protocol to be applied', [pwd '\Protocols\']);
    end
end

% Save the protocol to filter its name (and place automatically the correct parameters on the interface)
if filename==0, return, end
handles.nameProtocol = filename;
protocol = readProtocol ([pathname filename]);

try
    % We show the message PROCESSING...
    set (handles.pushbutton_wlOr, 'Visible', 'off')
    set (handles.text_processing, 'Visible', 'on')
    uiwait (handles.figure1,1)
    
    [~, ~, contrastFilter, percentageC, minGrey, cSoft, cDense, lSoft, lDense, gcSoft, gcDense, glSoft, glDense, wlSoft, wlDense, llSoft, llDense, fSoft, fDense, aSoft, aDense, noiseFilter, percentageN, thresholdN, enhanceFilter, percentageE, minSizeSE, maxSizeSE, radius] = loadParameters (protocol);
              
    % ................................................................................
    % PERCENTAGES
    % ................................................................................
    set (handles.text_contrastPercentage, 'String', [num2str(round(percentageC)) '%'])
    set (handles.slider_contrast, 'Value', percentageC/100)
    set (handles.text_percentageNoise, 'Enable', 'on', 'String', [num2str(round(percentageN)) '%  '])
    set (handles.slider_noise, 'Enable', 'on', 'Value', percentageN/100)
    set (handles.text_enhancementPercentage, 'String', [num2str(round(percentageE)) '%'])
    set (handles.slider_enhancement, 'Value', percentageE/100)
    % ................................................................................
    % CONTRAST PARAMETERS
    % ................................................................................
    % Load the contrast parameters
    if ~isempty(minGrey)
        handles.minHist = minGrey;
    end
    
    % Load the type of contrast algorithm
    set (handles.popupmenu_typeEqualization, 'Enable', 'on', 'Value', contrastFilter)

    switch (contrastFilter)
        case (1) %Sub-histogram intensity correction
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'off')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'off', 'String', ' -              Image`s contrast              + ')
            set (handles.text_contrastPercentage, 'Visible','on', 'Enable', 'off','String', '0%');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Select the functions parameters', 'BackgroundColor', [0.729, 0.831, 0.957])
            handles.curveSoft = cSoft;
            handles.curveDense = cDense;
            handles.levelSoft = lSoft;
            handles.levelDense = lDense;
            handles.factorSoft = fSoft;
            handles.factorDense = fDense;
            handles.curveGammaSoft = gcSoft;
            handles.curveGammaDense = gcDense;
            handles.levelGammaSoft = glSoft;
            handles.levelGammaDense = glDense;
            handles.levelLinealSoft = llSoft;
            handles.levelLinealDense = llDense;
            handles.windowLinealSoft = wlSoft;
            handles.windowLinealDense = wlDense;
            handles.algSoft = aSoft;
            handles.algDense = aDense;

        case (2) % General equalization
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on', 'String', ' -              Image`s contrast              + ')
            set (handles.text_contrastPercentage, 'Visible','on');
            set (handles.text_contrastPercentage,'Enable', 'on');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background', 'BackgroundColor', [0.729, 0.831, 0.957])

        case (3) % Gamma Correction
            set (handles.slider_contrast, 'Visible', 'on', 'Enable', 'on')
            set (handles.text_contrast, 'Visible', 'on', 'Enable', 'on','String', ' Soft     <---     TISSUE     --->      Dense ')
            set (handles.text_contrastPercentage, 'Visible','on');
            set (handles.text_contrastPercentage,'Enable', 'on');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','on','String','Eliminate the background', 'BackgroundColor', [0.729, 0.831, 0.957])
            levelContrast = percentageC/100;
            if levelContrast >= 0.5
                percGamma = (levelContrast-0.5)*2*100;
                if levelContrast > 0.5
                    set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Dense']);
                else
                    set(handles.text_contrastPercentage, 'String', 'No contrast modification');
                end
            else
                percGamma = (1-levelContrast*2)*100;
                set(handles.text_contrastPercentage, 'String', [num2str(round(percGamma)) '% Soft']);
            end      
            
        otherwise
            set (handles.slider_contrast, 'Visible','on','Enable','off')
            set (handles.text_contrast, 'String', ' -              Image`s contrast              + ', 'Enable', 'off')
            set (handles.text_contrastPercentage, 'String', '0%', 'Enable', 'off');
            set (handles.pushbutton_change, 'Visible', 'on','Enable','off','String','Eliminate the background', 'BackgroundColor', [0.729, 0.831, 0.957])
    end

    % ................................................................................
    % DENOISING PARAMETERS
    % ................................................................................
    
    % Load the denoising filter and its parameters
    set (handles.popupmenu_typeNoise, 'Enable', 'on', 'Value', noiseFilter)

    if (noiseFilter==1) 
        set (handles.slider_noise, 'Enable','on','SliderStep', [0.125 0.125], 'Position', [0.568 0.639 0.385 0.186])
        set (handles.slider_th, 'Visible', 'on', 'Enable', 'on', 'Value', thresholdN)
        set (handles.text_denoising, 'Enable','on','Position', [0.587 0.835 0.364 0.186])
        set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.639 0.061 0.186])
        set (handles.text_th, 'Visible', 'on','Enable','on')
        set (handles.text_thresh, 'Visible', 'on', 'Enable','on','Position', [0.586 0.103 0.358 0.186], 'String', ' -             | Optimal edges threshold          +')

    elseif noiseFilter == 4
        set (handles.slider_noise, 'Position', [0.568 0.429 0.385 0.186], 'Enable', 'off')
        set (handles.slider_th, 'Visible', 'off')
        set (handles.text_thresh, 'Position', [0.586 0.193 0.358 0.186], 'Enable', 'off',  'String', ' -                      Blurring                            +')
        set (handles.text_th, 'Visible', 'off')
        set (handles.text_denoising, 'Position', [0.587 0.625 0.364 0.186], 'Enable', 'off')
        set (handles.text_percentageNoise, 'Position', [0.506 0.398 0.061 0.186], 'Enable','off')

    else
        set (handles.slider_noise, 'Enable','on','SliderStep', [0.01 0.01], 'Position', [0.568 0.429 0.385 0.186])
        set (handles.slider_th, 'Visible', 'off')
        set (handles.text_thresh, 'Enable','on','Position', [0.586 0.193 0.358 0.186], 'String', ' -                      Blurring                            +')
        set (handles.text_th, 'Visible', 'off')
        set (handles.text_denoising, 'Enable','on','Position', [0.587 0.625 0.364 0.186])
        set (handles.text_percentageNoise, 'Enable','on','Position', [0.506 0.398 0.061 0.186])
    end

    % ................................................................................
    % EDGES AND DETAILS ENHANCEMENT PARAMETERS
    % ................................................................................
    
    % Load the type of enhancement algorithm and its parameters
    set (handles.popupmenu_typeEnhancement, 'Visible', 'on', 'Enable', 'on', 'Value', enhanceFilter)
    set (handles.slider_enhancement, 'Enable', 'on')

    if (enhanceFilter == 1) || (enhanceFilter == 2)  % USM
        set (handles.edit_seSize, 'Visible', 'off')
        set (handles.edit_minSe, 'Visible', 'off', 'Value', minSizeSE)
        set (handles.edit_radius, 'Visible', 'on', 'Enable', 'on', 'String', radius)
        set (handles.text_se, 'Visible', 'off')       
        set (handles.text_radius, 'Visible', 'on', 'Enable', 'on')      
        set (handles.text_enhance, 'Enable', 'on')
        set (handles.text_enhancementPercentage, 'Enable', 'on')
        set (handles.slider_enhancement, 'Enable', 'on')
        
    elseif enhanceFilter == 3
        set (handles.edit_radius, 'Visible', 'off')
        set (handles.edit_seSize, 'Visible', 'on', 'Enable', 'on', 'String', maxSizeSE)
        set (handles.edit_minSe, 'Visible', 'on', 'Enable', 'on', 'String', minSizeSE)
        set (handles.text_enhance, 'Enable', 'on')
        set (handles.text_enhancementPercentage, 'Enable', 'on')
        set (handles.text_radius, 'Visible', 'off')
        set (handles.text_se, 'Visible', 'on', 'Enable', 'on')
        set (handles.slider_enhancement, 'Enable', 'on')
        
    else
        set (handles.edit_seSize, 'Visible', 'off')
        set (handles.edit_minSe, 'Visible', 'off')
        set (handles.edit_radius, 'Visible', 'on', 'Enable', 'off')
        set (handles.text_se, 'Visible', 'off')
        set (handles.text_radius, 'Visible', 'on', 'Enable', 'off')
        set (handles.text_enhance, 'Enable', 'off')
        set (handles.text_enhancementPercentage, 'Enable', 'off')
        set (handles.slider_enhancement, 'Enable', 'off')

    end
    
    % ................................................................................
    % IMAGE PROCESSING
    % ................................................................................

    % Equalizing the image
    [equalizedImage, handles] = equalizeImage (handles);
    handles.equalizedImage = uint16(equalizedImage);

    % Denoising image
    [denoisedImage, cDenoised, sDenoised, handles] = denoiseImage (handles);
    handles.denoisedImage = uint16(denoisedImage);
    handles.cDenoised = cDenoised;
    handles.sDenoised = sDenoised;

    % Enhancing the denoised image
    [processedImage, handles] = enhanceImage (handles);
    handles.processedImage = uint16(processedImage);

    % Processed image's name
    handles.nameProcessedImage = [handles.nameImage '(' handles.nameROIcropped handles.nameBodyPart handles.nameTypeContrast handles.nameTypeNoise handles.nameTypeEnhancement ')'];
    set(handles.text_nameRight, 'String', handles.nameProcessedImage)

    %We select the current limits in case the image has been zoomed and panned
    xLim = get(gca,'XLim');
    yLim = get(gca,'YLim');
    handles.factorZoom = [xLim yLim];

    % Show the new processed image
    axes(handles.axes1);
    %[bins,~] = imhist(handles.processedImage,2^16);
    [minIm, maxIm] = imageRange(handles.processedImage);
    imshow(handles.processedImage,[minIm maxIm])
    set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));
    set (handles.text_nameRight, 'String', handles.nameProcessedImage);

    % ................................................................................
    % ENABLE/DISABLE THE ELEMENTS OF THE INTERFACE
    % ................................................................................
    set (handles.pushbutton_info,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_save,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_zoom_in,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_zoom_out,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_noZoom,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_fix,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_delete,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_saveProtocol,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.pushbutton_loadProtocol,  'Enable', 'on', 'BackgroundColor', [0.729, 0.831, 0.957])
    set (handles.text_processing, 'String', 'PROCESSING', 'Visible', 'off')
    set (handles.pushbutton_wlOr, 'Visible', 'on')
    
    % Reseting the control parameters
    handles.newLoaded = 0;

catch me
    set (handles.text_processing, 'Visible', 'on')
    uiwait(msgbox('The protocol has not been loaded', 'Info'))
    return
end

% Update the handles structure
guidata(handles.figure1,handles);

        
% --- Executes on button press in pushbutton_saveProtocol.
function pushbutton_saveProtocol_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_saveProtocol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Copy the list so that we do not modify anything in the interface in case
% the user does not want to save the image finally
listIm = handles.processedImages;
indexIm = handles.indexProcImages;
nIm = handles.nProcImages;
namesIm = handles.names;

% Check if the original image is in the list. It should be deleted.
if (handles.originalIncluded == 1)
    aux = listIm(:,:,2:end);
    for k = 1:nIm-1
        auxNames{1,k} = namesIm{1,k+1};
    end
    clear listIm
    clear namesIm
    listIm = aux;
    namesIm = auxNames;
    clear aux
    % Update the number of images of the list and the index
    nIm = nIm - 1;
    if indexIm ~= 1
        indexIm = indexIm - 1;
    end
end
    
% Check if the current processed image is in the array of processed images
processedIncluded = 0;

for k = 1:nIm
    if handles.processedImage==listIm(:,:,k)
        processedIncluded = 1;
        break
    else
        processedIncluded = 0;
    end
end

% If not, we include it in the list because maybe the user wants to save it
if processedIncluded == 0
    nIm = nIm + 1;
    indexIm = nIm;
    listIm(:,:,nIm) = handles.processedImage;
    namesIm{1,nIm} = handles.nameProcessedImage;
end

if nIm > 1
    % We communicate with the window to save images and protocols. We inform
    % about we are saving a protocol (not an image)
    setappdata(0, 'protocol', 1)
    setappdata(0,'processedImages', listIm);
    setappdata(0,'indexProcImages', indexIm);
    setappdata(0,'nProcImages', nIm);
    % Secondary window
    saveImages
    uiwait (saveImages)
    drawnow
    % Index of the protocol to be saved
    index = getappdata(0,'index');
    if isempty(index)
        protocol = [];
    else
        protocol = namesIm{1,index};
    end
else
    protocol = handles.nameProcessedImage;
end

drawnow %Function to update the interface (we can run uiputfile after uiwait)

% Now, a window appear so we have to indicate the animal, its size and the
% undesireable effect in the image.
if ~isempty(protocol)
    setappdata(0, 'type', 'save')
    save_load_protocol
    uiwait(save_load_protocol)

    drawnow %Function to update the interface (we can run uiputfile after uiwait)

    % Get the features selected by the user
    animal = getappdata(0,'animal');
    sizeAnimal = getappdata(0,'size');
    effect = getappdata(0,'effect');
    body = getappdata(0,'body');
    exotic = getappdata (0,'exotic');
    other = getappdata (0, 'other');
    part = getappdata (0, 'otherPart');
    savedProtocol = getappdata (0, 'savedProtocol');

    drawnow % Give the control to the interface again

    % Create a name for the protocol automatically 
    [nameProtocol, ~, ~, ~, ~] = buildName(animal, sizeAnimal, effect, body, exotic, other, part, savedProtocol);

    if isempty(nameProtocol)
        return
    elseif (isequal(nameProtocol, 'noFieldsSelected'))
        uiwait(msgbox('You have not saved any protocol. Please, select the features that define the protocol before saving it','Info'))
        return
    else
        [name, path] = uiputfile('*.txt', 'Save the protocol', [pwd '\Protocols\' nameProtocol]); 
        if name == 0, return, end
        saveProtocol (protocol, name, path)
    end
end

% Update the handles structure
guidata(handles.figure1,handles);



% --- Executes on mouse press over figure background.
function figure1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

drawnow % Flush the interface when clicking on it with the mouse

% **********************************************************************************************************************************************************************************************************

% REAL TIME MODE

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in activateRealTime.
function activateRealTime_Callback(hObject, eventdata, handles)
% hObject    handle to activateRealTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of activateRealTime

% Button to activate/deactivate real time
activeRealTime = get(handles.activateRealTime, 'Value');
handles.activeRealTime = activeRealTime;

if handles.activeRealTime == 0
    set(handles.activateRealTime, 'ForegroundColor', [0.9 0.0 0.0], 'String', ' No Real Time', 'BackgroundColor', [0.95 0.80 0.80])
    set(handles.pushbutton_process, 'Enable', 'on', 'BackgroundColor', [0.882, 0.882, 0.969])
else
    set(handles.activateRealTime, 'ForegroundColor', [0.0 0.6 0.0], 'String', ' Real Time', 'BackgroundColor', [0.80 0.95 0.80])
    set(handles.pushbutton_process, 'Enable', 'off', 'BackgroundColor', [0.941, 0.941, 0.941])
end

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% DICOM INFO

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_info.
function pushbutton_info_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_info (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% We force the detail mode to be deactivated
if handles.detailActive == 1
    pushbutton_noDetails_Callback(hObject, eventdata, handles)
end
info

% **********************************************************************************************************************************************************************************************************

% HELP FUNCTIONS

% **********************************************************************************************************************************************************************************************************
% --- Executes on button press in pushbutton_helpContrast.
function pushbutton_helpContrast_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_helpContrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
helpContrast

% --- Executes on button press in pushbutton_helpNoise.
function pushbutton_helpNoise_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_helpNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
helpNoise

% --- Executes on button press in pushbutton_helpEnhancement.
function pushbutton_helpEnhancement_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_helpEnhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
helpEnhancement

% **********************************************************************************************************************************************************************************************************

% ZOOM IN-OUT

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_zoom_in.
function pushbutton_zoom_in_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zoom_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(1.1)
pan on

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_zoom_out.
function pushbutton_zoom_out_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zoom_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom(1/1.1)
zoom off

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_noZoom.
function pushbutton_noZoom_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_noZoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pan off

% Update the images without zoom
axes(handles.axes1);
%[bins,~] = imhist(handles.processedImage,2^16);
[minIm, maxIm] = imageRange(handles.processedImage);
imshow(handles.processedImage,[minIm maxIm])

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

% WINDOW/LEVEL BUTTONS

% **********************************************************************************************************************************************************************************************************

% --- Executes on button press in pushbutton_wl.
function pushbutton_wl_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_wl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% imcontrast(handles.axes1)
imcontrast(handles.axes1);

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in pushbutton_wlOr.
function pushbutton_wlOr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_wlOr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imcontrast(handles.axes2);

% Update handles structure
guidata(handles.figure1,handles);

% **********************************************************************************************************************************************************************************************************

%COMPARE MODE

% **********************************************************************************************************************************************************************************************************
% --- Executes on button press in pushbutton_fix.
function pushbutton_fix_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_fix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Colour the button in green to indicate the fix action is done
set(hObject, 'BackgroundColor', 'g')
pause(0.2)

% The number of images and the index increases
handles.nProcImages = handles.nProcImages + 1;
handles.indexProcImages = handles.nProcImages;

%Save the image and the name
handles.processedImages (:,:,handles.indexProcImages) = getimage(handles.axes1);
handles.names{1,handles.indexProcImages}=handles.nameProcessedImage;

% If there are more than one image, the next and previous buttons are enabled
if (handles.nProcImages >=1) && (handles.originalIncluded == 0)
    set(handles.original,'Enable','on', 'BackgroundColor', [0.729, 0.831, 0.957],'ForegroundColor', [0.2, 0.302, 0.494])
else
    set(handles.original,'Enable','off', 'BackgroundColor', [0.941 0.941 0.941])
end
if handles.nProcImages > 1
    set(handles.pushbutton_next, 'Enable', 'on', 'BackgroundColor', [1 1 1])
    set(handles.pushbutton_previous, 'Enable', 'on', 'BackgroundColor', [1 1 1])
    
else
    set(handles.pushbutton_next, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
    set(handles.pushbutton_previous, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
end
set(handles.text_index,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])
set(hObject, 'BackgroundColor', [0.729, 0.831, 0.957])

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_delete.
function pushbutton_delete_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_delete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%We select the current limits in case the image has been zoomed and panned
xLim = get(gca,'XLim');
yLim = get(gca,'YLim');
handles.factorZoom = [xLim yLim];
    
if handles.nProcImages ~= 0
    % Colour the button in green to indicate the delete action has been done
    set(hObject, 'BackgroundColor', 'r')
    pause(0.2)
    if (handles.nProcImages == 1) || ((handles.nProcImages == 2) && (handles.originalIncluded == 1) && (handles.indexProcImages == 2)) % In the list there is only an image or one image and the original
        handles.originalIncluded = 0;
        set(handles.pushbutton_delete, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set(handles.pushbutton_fix, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set(handles.original, 'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set (handles.pushbutton_save,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        set (handles.pushbutton_saveProtocol,  'BackgroundColor', [0.941, 0.941, 0.941], 'Enable', 'off')
        clear handles.names handles.processedImages
        handles.names = [];
        handles.processedImages = [];
        handles.nProcImages = 0;
        handles.indexProcImages = 0;
        set(handles.text_index,'String', ' ')  
        axes(handles.axes1)
        imshow(handles.image,[handles.minOriginal handles.maxOriginal])
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));  
        set(handles.text_nameRight, 'String', handles.nameImage)
        
    else % If not, we update the list of processed images
       
       % If the image to be deleted is the original...
       if (handles.originalIncluded == 1) && (handles.indexProcImages == 1)
            handles.originalIncluded = 0;
            set(handles.original, 'Enable', 'on','BackgroundColor', [0.729, 0.831, 0.957])
       end
        
       % Update the list
        for i=1:handles.nProcImages-1
            if i < handles.indexProcImages
                aux(:,:,i) = handles.processedImages(:,:,i);
                auxNames{1,i} = handles.names{1,i};
            else
                aux(:,:,i) = handles.processedImages(:,:,i+1);
                auxNames{1,i} = handles.names{1,i+1};
            end
        end
        
        % Clear the auxiliar variables
        clear handles.processedImages handles.names
        handles.processedImages = aux;
        handles.names = auxNames;
        clear aux auxNames
        handles.nProcImages = handles.nProcImages - 1;

        % In case the deleted image is the last one of the list, the index should be updated to
        % the last element of the array.
        if handles.indexProcImages > handles.nProcImages
            handles.indexProcImages = handles.nProcImages;
        end
        
        % Show the proper image of the list
        axes(handles.axes1)    
        %[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
        [minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
        imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
        set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 

        set(handles.text_nameRight, 'String', handles.names{1,handles.indexProcImages})
        set(handles.text_index,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)]) 
        set(hObject, 'BackgroundColor', [0.729, 0.831, 0.957])
    end

    % Next and previous buttons are enabled only if there are more than one images
    if handles.nProcImages <= 1
        set(handles.pushbutton_next, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
        set(handles.pushbutton_previous, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941])
    else
        set(handles.pushbutton_next, 'Enable', 'on', 'BackgroundColor', [1 1 1])
        set(handles.pushbutton_previous, 'Enable', 'on', 'BackgroundColor', [1 1 1])
    end
end

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_next.
function pushbutton_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%We select the current limits in case the image has been zoomed and panned
xLim = get(gca,'XLim');
yLim = get(gca,'YLim');
handles.factorZoom = [xLim yLim];

% The index increases
handles.indexProcImages = handles.indexProcImages + 1;
% If the index is the last one, the next is the first
if handles.indexProcImages > handles.nProcImages
    handles.indexProcImages = 1;
end

% Show the proper image
axes(handles.axes1)      
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 

% Update the texts
set(handles.text_nameRight, 'String', handles.names{1,handles.indexProcImages})
set(handles.text_index,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])
        
% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_previous.
function pushbutton_previous_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Update handles structure

%We select the current limits in case the image has been zoomed and panned
xLim = get(gca,'XLim');
yLim = get(gca,'YLim');
handles.factorZoom = [xLim yLim];

% The index is updated
handles.indexProcImages = handles.indexProcImages - 1;
% If the index is the first of the list, it is updated to the last one
if handles.indexProcImages < 1 
    handles.indexProcImages = handles.nProcImages;
end

% Show the new processed image            
axes(handles.axes1);       
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4)); 

% Update the texts
set(handles.text_nameRight, 'String', handles.names{1,handles.indexProcImages})
set(handles.text_index,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])

% % Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in original.
function original_Callback(hObject, eventdata, handles)
% hObject    handle to original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Disable the button
set(handles.original,'Enable','off', 'BackgroundColor', [0.941 0.941 0.941])

%We select the current limits in case the image has been zoomed and panned
xLim = get(gca,'XLim');
yLim = get(gca,'YLim');
handles.factorZoom = [xLim yLim];

% Update the number of images and the index
handles.nProcImages = handles.nProcImages + 1;
handles.indexProcImages = 1;

% Update the list of processed images
aux = handles.processedImages;
auxNames = handles.names;
clear handles.processedImages handles.names
for i = 2:handles.nProcImages
    handles.processedImages(:,:,i) = aux(:,:,i-1);
    handles.names{1,i} = auxNames{1,i-1};
end
handles.processedImages(:,:,1) = handles.image;
handles.names{1,1} = handles.nameImage; 
set(handles.text_index,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])

% Show the original image           
axes(handles.axes1);    
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(gca,'XLim',handles.factorZoom(1,1:2),'YLim',handles.factorZoom(1,3:4));
set(handles.text_nameRight, 'String', handles.names{1,handles.indexProcImages})
set(handles.pushbutton_next, 'Enable', 'on')
set(handles.pushbutton_previous, 'Enable', 'on')
    
% Control parameter that indicates we have included the original image in
% the list
handles.originalIncluded = 1;

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_refIm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_refIm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slider_enhancement_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_enhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function edit_seSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_seSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_radius_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_minSe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_minSe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function popupmenu_typeEnhancement_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeEnhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function popupmenu_typeNoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slider_noise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function slider_th_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_th (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function popupmenu_typeEqualization_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_typeEqualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slider_contrast_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton_amplify.
function pushbutton_amplify_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_amplify (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'image',getimage(handles.axes1))
zoomImage

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in pushbutton_amplifyOriginal.
function pushbutton_amplifyOriginal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_amplifyOriginal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'image',getimage(handles.axes2))
zoomImage

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
javax.swing.UIManager.setLookAndFeel( com.sun.java.swing.plaf.windows.WindowsLookAndFeel)
