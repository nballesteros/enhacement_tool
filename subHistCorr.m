% Subhistogram intensity correction: apply different types of processing
% for the soft and the dense tissues. They can be sigma or gamma functions.
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be equalized
% curveSoft, levelSoft, curveDense, levelDense: sigma parameters for the soft and dense tissues.
% curveGammaSoft, curveGammaDense, levelGammaSoft, levelGammaDense: gamma parameters for the soft and dense tissues.
% factorSoft, factorDense: weighting
% algSoft, algDense: sigma or gamma for the soft and dense tissues
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with improved contrast
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [equalizedImage, factorSoft, factorDense] = subHistCorr (image, curveSoft, curveDense, levelSoft, levelDense, curveGammaSoft, curveGammaDense, levelGammaSoft, levelGammaDense, windowLinealSoft, levelLinealSoft, windowLinealDense, levelLinealDense, algSoft, algDense, factorSoft, factorDense, background)
   
% Higher intensity to be considered during equalization
maxIm = double(round(max(image(:))));
minIm = double(round(min(image(:))));
lowOriginal = minIm;
if background > minIm
    minIm = background;
end

if algSoft == 1  
    % Sigma transform with a low level to see better the soft tissues
    eq1 = sigmaTransform(image, curveSoft, levelSoft, minIm, maxIm, lowOriginal, 1);
elseif algSoft == 2
    gamSoft = 1 -(curveGammaSoft*4/5);
    low_in = round(minIm + levelGammaSoft*(maxIm - minIm));
    eq1 = gamma_Correction (image, gamSoft, low_in, maxIm, lowOriginal);
else
    eq1 = linealBySections (image, maxIm, windowLinealSoft, levelLinealSoft, lowOriginal, 0);
end

if algDense == 1
    % Sigma transform with a low level to see better the dense tissues
    eq2 = sigmaTransform(image, curveDense, levelDense, minIm, maxIm, lowOriginal, 0);
elseif algDense == 2
    gamDense = (curveGammaDense * 4) + 1;
    low_in = round(minIm + levelGammaDense*(maxIm - minIm));
    eq2 = gamma_Correction (image, gamDense, low_in, maxIm, lowOriginal);
else
    eq2 = linealBySections (image, maxIm, windowLinealDense, levelLinealDense, lowOriginal, 1);
end

% It is mandatory to substract the minimum of each image when we are
% working with images with values near 2^16 because, if we do not do it, the resulting image is
% satured
sImage = eq1 - min(eq1(:));
dImage = eq2 - min(eq2(:));

% Equalized image
if (factorSoft == 0)&&(factorDense == 0)
    [eqImage, factorSoft, factorDense] = maximizeEntropy(sImage, dImage, maxIm_input);
else
    eqImage = (factorSoft*sImage+factorDense*dImage);
    diffIm = maxIm-max(eqImage(:));
    eqImage = eqImage + diffIm;
end
equalizedImage = uint16(eqImage);


