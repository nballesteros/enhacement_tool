% maximizeEntropy: function to find the optimal weights for the images
% obtained in subHistCorr. This is done according to the entropy.
%...................................................................................................................
% INPUT PARAMETERS
% eq1: soft tissue image
% eq2: dense tissue image
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with improved contrast
% factorSoft, factorDense: optimized weighting factors
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2017

function [equalizedImage, factorSoft, factorDense] = maximizeEntropy(eq1,eq2, maxIm_input)

rangeAlpha = [0 1];
toleranceAlpha = (rangeAlpha(end)-rangeAlpha(1))/10;
ro = (3-sqrt(5))/2;

% First limit. Find the optimum image for this alpha
alpha1 = rangeAlpha(1) + ro*(rangeAlpha(end)-rangeAlpha(1));
eqImage1 = alpha1*eq1+(1-alpha1)*eq2;
maxIm1 = max(eqImage1(:));
eqImageN1 = double(eqImage1)/double(maxIm1);
entropy1 = entropy(eqImageN1);
    
% Second limit. Find the optimum image for this alpha
alpha2 = rangeAlpha(1) + (1-ro)*(rangeAlpha(end)-rangeAlpha(1));
eqImage2 = alpha2*eq1+(1-alpha2)*eq2;
maxIm2 = max(eqImage2(:));
eqImageN2 = double(eqImage2)/double(maxIm2);
entropy2 = entropy(eqImageN2);  
    
% ITERATIVE STAGE
while (rangeAlpha(2)-rangeAlpha(1)) > toleranceAlpha
     if entropy2 > entropy1

        % Narrow the range
        rangeAlpha = [alpha1 rangeAlpha(end)];

        % The second limit becomes the first limit
        alpha1 = alpha2;
        eqImageN1 = eqImageN2;

        % Calculate again the second limit
        alpha2 = rangeAlpha(1) + (1-ro)*(rangeAlpha(end)-rangeAlpha(1));
        eqImage2 = alpha2*eq1+(1-alpha2)*eq2;
        maxIm2 = max(eqImage2(:));
        eqImageN2 = double(eqImage2)/double(maxIm2);
        entropy2 = entropy(eqImageN2);

        % Update the final variables according to the maximum entropy
        if entropy2 > entropy1
            equalizedImage = eqImageN2;
            factorSoft = alpha2;
            factorDense = 1-factorSoft;
        else
            equalizedImage = eqImageN1;
            factorSoft = alpha1;
            factorDense = 1-factorSoft;
        end
     else

        % Narrow the range
        rangeAlpha = [rangeAlpha(1) alpha2];

        % The first limit becomes the second limit
        alpha2 = alpha1;
        eqImageN2 = eqImageN1;

        % Calculate again the first limit
        alpha1 = rangeAlpha(1) + ro*(rangeAlpha(end)-rangeAlpha(1));
        eqImage1 = alpha1*eq1+(1-alpha1)*eq2;
        maxIm1 = max(eqImage1(:));
        eqImageN1 = double(eqImage1)/double(maxIm1);
        entropy1 = entropy(eqImageN1);

        % Update the final variables according to the maximum entropy
        if entropy1 > entropy2
            equalizedImage = eqImageN1;
            factorSoft = alpha1;
            factorDense = 1-factorSoft;
        else
            equalizedImage = eqImageN2;
            factorSoft = alpha2;
            factorDense = 1-factorSoft;
        end
     end
end

equalizedImage = equalizedImage + double(maxIm_input - max(equalizedImage(:)));