% SIGMA TRANSFORM
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% curve: degree of sigma curvature
% level: level (position of the center of the sigma)
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with better contrast
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = sigmaTransform(image, curve, level, minIm, maxIm, lowOriginal, typeImage)

% Image information
lowOriginal = double(lowOriginal);
image = image - lowOriginal;
high_in = double(maxIm-lowOriginal);
low_in = double(minIm-lowOriginal);
[x,y] = size(image);

% Create the SIGMA LUT
if typeImage == 1 %Soft
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(curve*9) 0+(level*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:0.75*length(LUTsigma));
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT3 = 0.75*length(LUTsigma)+1:length(LUTsigma);
    LUT = [LUT1 LUT2 LUT3-min(LUT3)+max(LUT2)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
else
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(curve*9) 0.5+(level*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:end);
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT = [LUT1 LUT2];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
end
    

% Sigma image where we have applied the sigma LUT
equalizedImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = equalizedImage + lowOriginal;
