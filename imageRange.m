function [minIm, maxIm] = imageRange(image)
 
    medianIm = medfilt2(image,'symmetric');
    
    [bins,~] = imhist(medianIm,2^16);
    
% Nerea    
%     k = find(bins,1);
%     X = bins(k:end,:); 
%     Y = k:length(bins);
%     
%     suma = cumsum(X); % Sum histogram counts to get cumulative distribution function.
%     suma = suma / suma(end); % Normalize.
%     data10 = find(suma>= 0.05, 1, 'first');
%     minIm = Y(data10);
%     maxIm = max(image(:));
   

%   In�s
    binsInv = flipdim(bins,1);
    outlier = 0.005*sum(bins);
    suma = 0;
    sumInv = 0;
    for i=2:length(bins)
        suma = suma + bins(i);
        if suma > outlier
            minIm = i-1;
            break
        end
    end
    for i=1:length(bins)
        sumInv = sumInv + binsInv(i);
        if sumInv > outlier
            maxIm = length(bins) - i;
            break
        end
    end


       
    
end