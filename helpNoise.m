function varargout = helpNoise(varargin)
% HELPNOISE MATLAB code for helpNoise.fig
%      HELPNOISE, by itself, creates a new HELPNOISE or raises the existing
%      singleton*.
%
%      H = HELPNOISE returns the handle to a new HELPNOISE or the handle to
%      the existing singleton*.
%
%      HELPNOISE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HELPNOISE.M with the given input arguments.
%
%      HELPNOISE('Property','Value',...) creates a new HELPNOISE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before helpNoise_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to helpNoise_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help helpNoise

% Last Modified by GUIDE v2.5 11-Jan-2017 11:37:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @helpNoise_OpeningFcn, ...
                   'gui_OutputFcn',  @helpNoise_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before helpNoise is made visible.
function helpNoise_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to helpNoise (see VARARGIN)

% Choose default command line output for helpNoise
handles.output = hObject;

% Default percentage 25%
set(handles.radiobutton25, 'Value', 1)

% Loading the example images
originalIm = dicomread([pwd '\Example_images\noiseOriginal']);

% Show original noisy image
axes(handles.axes1)
imshow(imresize(originalIm,0.5),[])
set(handles.text1, 'Visible', 'on')

% Linking the axes in order to be able to apply the zoom tool at once
h(1)=handles.axes1;
h(2)=handles.axes2;
h(3)=handles.axes3;
h(4)=handles.axes4;
linkaxes (h, 'xy')

% Show images with 25% denoising (default)
showImages(handles)

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = helpNoise_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in radiobutton25.
function radiobutton25_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton25

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)
end

% Show images
showImages(handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in radiobutton50.
function radiobutton50_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton50 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton50

if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)    
end

% Show images
showImages(handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in radiobutton100.
function radiobutton100_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton100

if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

% Show images
showImages(handles)

% Update handles structure
guidata(hObject, handles);

function showImages(handles)

% Disable the option of closing the window until showing all the images
set(gcf, 'closerequestfcn', '');

% Load the images depending on the denoising percentage selected
if (get(handles.radiobutton25, 'Value') == 1)
    Wav = dicomread([pwd '\Example_images\noiseWavelet25']);
    Pyr = dicomread([pwd '\Example_images\noisePyramid25']);
    NLM = dicomread([pwd '\Example_images\noiseNLM25']);
      
elseif (get(handles.radiobutton50, 'Value') == 1)
    Wav = dicomread([pwd '\Example_images\noiseWavelet50']);
    Pyr = dicomread([pwd '\Example_images\noisePyramid50']);
    NLM = dicomread([pwd '\Example_images\noiseNLM50']);
    
elseif (get(handles.radiobutton100, 'Value') == 1)    
    Wav = dicomread([pwd '\Example_images\noiseWavelet100']);
    Pyr = dicomread([pwd '\Example_images\noisePyramid100']);
    NLM = dicomread([pwd '\Example_images\noiseNLM100']);    
end

% Show all images scaled in a factor of 0.5 to speed up the code
axes(handles.axes2)
set(handles.text2, 'Visible', 'on')
imshow(imresize(Wav,0.5),[])
axes(handles.axes3)
set(handles.text3, 'Visible', 'on')
imshow(imresize(Pyr,0.5),[])
axes(handles.axes4)
set(handles.text4, 'Visible', 'on')
imshow(imresize(NLM,0.5),[])

% Enable the option of closing the window 
set(gcf, 'closerequestfcn', 'closereq');
