% Auxiliar function to clip the bins of a given histogram
%...................................................................................................................
% INPUT PARAMETERS
% histImage: histogram we want to modify
% clip: clipping factor. Values above the threshold are clipped and
%       distributed between the rest of the bins.
% low_in: minimum grey value from which we consider the histogram to be clipped 
%...................................................................................................................
% OUTPUT PARAMETERS
% clippedHist: clipped histogram
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function clippedHist = clipBins (histImage, clip, low_in)

% Intermediate variables for the clipped histogram
clippedHist = histImage;
histClipped = histImage;
excessPixels = 0;
histSize = size(histImage,1);

if low_in == 0 % When we are considering the whole histogram, from zero, we obviate the value 0 as the peak distorts the result
    % Max of the histogram needed to establish the threshold of clipping
    maxHist = max(histImage(2:end,1));
    clip = (clip/1.1); % We go from the 0% of the maximum value to the 90%
    factor = maxHist*clip;

    % We check if the bin exceeds the threshold. The background of the image (hist(1)) is avoided
    for i = 2:histSize
        if histClipped(i,1) > factor % We limit the bins that are above the threshold
            excessPixels = excessPixels + histClipped(i,1) - factor; % We calculate the excess of pixels in this bin
            histClipped(i,1) = factor; % The histogram has to be the maximum 
        end
    end

    % We count how many extra pixels has to be included in each bin (except the gray value 0)
    extraBins = round(excessPixels/(histSize-1));
    for i = 2:size(histImage,1)
        % Because the round of extraBins, it is possible that this value is greater than excessPixels (in the last bin) so only the remaining pixels should be added.
        if excessPixels < extraBins 
            clippedHist(i,1) = histClipped(i,1) + excessPixels;
            return
        end
        clippedHist(i,1) = histClipped(i,1) + extraBins;
        excessPixels = excessPixels - extraBins;
        
    end
else
    % Max of the histogram needed to establish the threshold of clipping
    maxHist = max(histImage);
    clip = (clip/1.1); % We go from the 0% of the maximum value to the 90%
    factor = maxHist*clip;

    % We check if the bin exceeds the threshold. The background of the image (hist(1)) is avoided
    for i = 1:histSize
        if histClipped(i,1) > factor %We limit the bins that are above the threshold
            excessPixels = excessPixels + histClipped(i,1) - factor; %We calculate the excess of pixels in this bin
            histClipped(i,1) = factor; % The histogram has to be the maximum 
        end
    end

    % We count how many extra pixels has to be included in each bin (except the gray value 0)
    extraBins = round(excessPixels/(histSize-1));
    for i = 1:histSize
        % Because the round of extraBins, it is possible that this value is greater than excessPixels (in the last bin) so only the remaining pixels should be added.
        if excessPixels < extraBins 
            clippedHist(i,1) = histClipped(i,1) + excessPixels;
            return
        end
        clippedHist(i,1) = histClipped(i,1) + extraBins;
        excessPixels = excessPixels - extraBins;
    end
end

