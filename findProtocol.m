% Function to find a protocol among all that are saved
%.............................................................................................................................
% INPUT PARAMETERS
% nameProtocol: automatic name created according to the options the user has selected. 
%.............................................................................................................................
% OUTPUT PARAMETERS
% newProtocol: variable that is empty if we have not found the protocol's name. Otherwise, it takes the name of the protocol 
%.............................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016


function protocolFound = findProtocol(nameProtocol)

% nameProtocol is empty if the user has close the protocol window (we
% don't have to filter anything)
if isempty(nameProtocol)
    protocolFound = '';
% if protocol = all, no filter should be done but all the protocols must be
% shown
elseif strcmp(nameProtocol, 'all') 
    protocolFound = 'all';
else
    % We find exactly the protocol according to the categories selected by
    % the user. In case we don't find it, the variable newProtocol takes an
    % empty value (and we will be able to filter the protocols through the categories).
    protocolFound = '';
    folder = dir([pwd '\Protocols\']);
    for i = 1:size(folder,1)
        if strcmp(folder(i,1).name, [nameProtocol '.txt'])
            protocolFound = nameProtocol;
        end
    end
end
