function varargout = irradiatedFieldBackground(varargin)
% IRRADIATEDFIELDBACKGROUND MATLAB code for irradiatedFieldBackground.fig
%      IRRADIATEDFIELDBACKGROUND, by itself, creates a new IRRADIATEDFIELDBACKGROUND or raises the existing
%      singleton*.
%
%      H = IRRADIATEDFIELDBACKGROUND returns the handle to a new IRRADIATEDFIELDBACKGROUND or the handle to
%      the existing singleton*.
%
%      IRRADIATEDFIELDBACKGROUND('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IRRADIATEDFIELDBACKGROUND.M with the given input arguments.
%
%      IRRADIATEDFIELDBACKGROUND('Property','Value',...) creates a new IRRADIATEDFIELDBACKGROUND or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before irradiatedFieldBackground_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to irradiatedFieldBackground_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help irradiatedFieldBackground

% Last Modified by GUIDE v2.5 18-Oct-2016 13:06:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @irradiatedFieldBackground_OpeningFcn, ...
                   'gui_OutputFcn',  @irradiatedFieldBackground_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before irradiatedFieldBackground is made visible.
function irradiatedFieldBackground_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to irradiatedFieldBackground (see VARARGIN)

% Choose default command line output for irradiatedFieldBackground
handles.output = hObject;

% Crop button with picture
rectang = imread('recorte.jpg');
rectang = rectang(1:5:end,1:4.5:end,:);
set(handles.pushbutton_irradiatedField,'CData',rectang)

% We take the image and the current background threshold
handles.image = getappdata(0,'image');
handles.modifiedImage = getappdata(0,'modifiedImage');
handles.backTh = getappdata(0,'backTh');
handles.irradiatedField = getappdata(0,'rect');

% Set the data on the interface
handles.maxImage = double(max(max(handles.modifiedImage)));
handles.minImage = double(min(min(handles.modifiedImage)));
set(handles.edit_backTh, 'String', num2str(handles.backTh))
set(handles.slider_backgroundLevel, 'Value', (handles.backTh-handles.minImage)/(handles.maxImage-handles.minImage));

%--------------------------------------------------------------------------
% LUT
%--------------------------------------------------------------------------
inRange = handles.minImage:handles.maxImage;
%Normalization
LUT = (inRange-handles.minImage)/(handles.maxImage-handles.minImage);
% Limit of the LUT
ind1 = find(LUT==(handles.backTh-handles.minImage)/(handles.maxImage-handles.minImage));
if ind1 ~= 1
    LUT(1,ind1:end) = 0.5;
    % Heights correcction
    ratio = 0.5/LUT(1,ind1-1);
    LUT(1,1:ind1-1) = LUT(1,1:ind1-1)  * ratio;
else
    LUT(1,ind1:end) = 0.5;
end
    
%--------------------------------------------------------------------------
% SHOW IMAGE, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------
[bins,histImage] = imhist(handles.modifiedImage,2^16);

% Max of the histogram (avoiding 0) to scale the LUT
maxHist = max(bins(2:end));

% We show the histogram and the background threshold mark
axes(handles.axes2);
if handles.minImage > 2
    bar(histImage(handles.minImage:handles.maxImage),bins(handles.minImage:handles.maxImage),'b')
else
    bar(histImage(2:handles.maxImage),bins(2:handles.maxImage),'b')
end
hold on
plot (handles.minImage:handles.maxImage, LUT*maxHist*0.9, 'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
set(gca, 'xcolor', 'b', 'ycolor', 'b', 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Show the correct image (preview)
colorBackground (handles)

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes irradiatedFieldBackground wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = irradiatedFieldBackground_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton_irradiatedField.
function pushbutton_irradiatedField_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_irradiatedField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Reset
axes(handles.axes1)
%[bins,histImage] = imhist(handles.image,2^16);
[minIm, maxIm] = imageRange(handles.image);
imshow(handles.image,[minIm maxIm])
set(handles.edit_backTh, 'String', num2str(handles.backTh))
set(handles.slider_backgroundLevel, 'Value', handles.backTh/handles.maxImage);

% Take the rectangle
try
    rect = getrect(handles.axes1);
    xmin = rect(1);
    ymin = rect(2);
    width = rect(3);
    height = rect(4); 

    % Check the validity of the cropped ROI limits
    [rw,cl] = size(handles.image);
    if ymin < 1
        ymin = 1;
    end
    if xmin < 1
        xmin = 1;
    end
    ymax = ymin+height;
    if ymax > rw
        ymax = rw;
    end
    xmax=xmin+width;
    if xmax > cl
        xmax = cl;
    end

    % Default
    handles.irradiatedField = [xmin ymin xmax ymax];
    handles.modifiedImage = handles.image(ymin:ymax, xmin:xmax);

    updatehandles (handles)
catch
    return
end

% Update handles structure
guidata(handles.figure1,handles)

% --- Executes on slider movement.
function slider_backgroundLevel_Callback(hObject, eventdata, handles)
% hObject    handle to slider_backgroundLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Update the values when we modify the slider to select the background
% threshold
backTh = get(handles.slider_backgroundLevel,'Value');
handles.backTh = round(handles.minImage + backTh * (handles.maxImage - handles.minImage));
if handles.backTh < handles.minImage
    handles.backTh = handles.minImage;
end
set(handles.edit_backTh, 'String', num2str(handles.backTh))

% Update the visualization with the new threshold
updatehandles (handles)

% Update handles structure
guidata(handles.figure1,handles)

function edit_backTh_Callback(hObject, eventdata, handles)
% hObject    handle to edit_backTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_backTh as text
%        str2double(get(hObject,'String')) returns contents of edit_backTh as a double

% Take the background threshold
backTh = get(handles.edit_backTh,'String');

% Check if the user has selected a valid grey value
if isempty(backTh)
    uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'))
    set(hObject,'String',num2str(handles.minImage))
    handles.backTh = handles.minImage;
elseif ~isempty (strfind(backTh, ','));
    uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'))
    set(hObject,'String',num2str(handles.minImage))
    handles.minRelative = handles.minImage;
else
     backTh = str2double(backTh);
     if isnan(backTh)
        uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
        set(hObject,'String',num2str(handles.minImage))
        handles.backTh = handles.minImage;
     else
        integerTest=~mod(backTh,1);
        if isempty (backTh)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
            set(hObject,'String',num2str(handles.minImage))
            handles.backTh = handles.minImage;
        elseif (backTh < handles.minImage) || (integerTest == 0)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
            set(hObject,'String',num2str(handles.minImage))
            handles.backTh = handles.minImage;
        elseif (backTh >= handles.maxImage)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value). It cannot be greater than the maximum histogram value', 'Info'));
            set(hObject,'String',num2str(handles.minImage))
            handles.backTh = handles.minImage;
        else
            handles.backTh = backTh;
        end
     end
end
set(handles.slider_backgroundLevel, 'Value', handles.backTh/handles.maxImage)

% Update the visualization with the new threshold
updatehandles (handles)

% Update handles structure
guidata(handles.figure1,handles)

% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% We send the new background threshold and the cropping limits
setappdata (0,'backTh',handles.backTh)
setappdata (0, 'rect', handles.irradiatedField)

close


function updatehandles (handles)

inRange = handles.minImage:handles.maxImage;
%Normalization
LUT = (inRange-handles.minImage)/(handles.maxImage-handles.minImage);
% Limit of the LUT
ind1 = find(LUT==(handles.backTh-handles.minImage)/(handles.maxImage-handles.minImage));
if ind1 ~= 1
    LUT(1,ind1:end) = 0.5;
    % Heights correcction
    ratio = 0.5/LUT(1,ind1-1);
    LUT(1,1:ind1-1) = LUT(1,1:ind1-1)  * ratio;
else
    LUT(1,ind1:end) = 0.5;
end
    
%--------------------------------------------------------------------------
% SHOW IMAGE, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------
cla (handles.axes1);

[bins,histImage] = imhist(handles.modifiedImage,2^16);

% Max of the histogram (avoiding 0) to scale the LUT
maxHist = max(bins(2:end));

% We show the histogram and the background threshold mark
axes(handles.axes2);
if handles.minImage > 2
    bar(histImage(handles.minImage:handles.maxImage),bins(handles.minImage:handles.maxImage),'b')
else
    bar(histImage(2:handles.maxImage),bins(2:handles.maxImage),'b')
end
hold on
plot (handles.minImage:handles.maxImage, LUT*maxHist*0.9, 'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
set(gca, 'xcolor', 'b', 'ycolor', 'b', 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Preview image
colorBackground (handles)

% Update handles
guidata(handles.figure1,handles)


function colorBackground (handles)

% Reduce the size of the image to speed up the code
imSmall = imresize(handles.modifiedImage,0.25);

% Create a image with all pixels in red
redChannel = ones(size(imSmall))*0.1;
greenChannel = ones(size(imSmall))*0.3;
blueChannel = ones(size(imSmall));
coloredImage = cat(3, redChannel, greenChannel, blueChannel);

% Create a mask with transparency in the pixels where the image is below
% the threshold
mask = true(size(imSmall));
axes(handles.axes1)
mask(imSmall<handles.backTh)=false;

% Show both images and the mask
imshow(coloredImage,[])
hold on;
%[bins,histImage] = imhist(imSmall,2^16);
[minIm, maxIm] = imageRange(imSmall);
h = imshow(imSmall,[minIm maxIm]);
set(h,'AlphaData',mask);
hold off

% Update handles
guidata(handles.figure1,handles)
