% SIGMA TRANSFORM
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% curve: degree of sigma curvature
% level: level (position of the center of the sigma)
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with better contrast
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = sigmaTransformSub(image, curve, level, minIm, maxIm, lowOriginal)

% Image information
lowOriginal = double(lowOriginal);
image = image - lowOriginal;
high_in = double(maxIm-lowOriginal);
low_in = round(minIm + level*(maxIm - minIm));
low_in = double(low_in-lowOriginal);
[x,y] = size(image);

% Create the SIGMA LUT
LUT1 = (0:high_in);
inRange = (0:high_in)/high_in;
LUTsigma = sigmf(inRange,[7+(curve*8) 0.5])*(high_in-low_in);
sigmaSize = length(LUTsigma);
LUT2 = LUTsigma(round(0.25*sigmaSize):round(0.75*sigmaSize));
diff1 = LUT1(low_in) - LUT2(1);
LUTini = [LUT1(1:low_in) LUT2+diff1];

if length(LUTini) > high_in
    LUT = LUTini(1:high_in+1);
else
    diffSize = length(LUT1)-length(LUTini);
    diff2 = diff1 + LUT2(end) - LUT1(length(LUT1)-diffSize);
    LUT = [LUTini LUT1(length(LUT1)-diffSize:length(LUT1))+diff2];
end

LUTnorm = LUT/max(LUT);
LUT = LUTnorm*high_in;

% % Image information
% lowOriginal = double(lowOriginal);
% image = image - lowOriginal;
% high_in = double(maxIm-lowOriginal);
% low_in = round(minIm + level*(maxIm - minIm));
% low_in = double(low_in-lowOriginal);
% [x,y] = size(image);
% 
% % Create the SIGMA LUT
% LUT1 = (0:high_in);
% inRange = (0:high_in)/high_in;
% LUTsigma = sigmf(inRange,[7+(curve*8) 0.5])*(high_in-low_in);
% sigmaSize = length(LUTsigma);
% LUT2 = LUTsigma(round(0.25*sigmaSize):round(0.75*sigmaSize));
% diff1 = LUT1(low_in) - LUT2(1);
% LUTini = [LUT1(1:low_in) LUT2+diff1];
% 
% if length(LUTini) > high_in
%     LUT = LUTini(1:high_in+1);
% else
%     diffSize = length(LUT1)-length(LUTini);
%     diff2 = diff1 + LUT2(end) - LUT1(length(LUT1)-diffSize);
%     LUT = [LUTini LUT1(length(LUT1)-diffSize:length(LUT1))+diff2];
% end
% 
% LUTnorm = LUT/max(LUT);
% LUT = LUTnorm*high_in;

% Sigma image where we have applied the sigma LUT
equalizedImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = equalizedImage + lowOriginal;
