% MORPHOLOGICAL PROCESSING
% Top-hat --> this function stands out bright details depending on the s.e.
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% size: of the structural element
% se: type of structural element
%...................................................................................................................
% OUTPUT PARAMETERS
% morpholocialImage: image where we have applied a tophat transformation
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016


function morphologicalImage = tophat (image, size, se)

se = strel (se, size); % Structural element
morphologicalImage = imtophat (image, se); % Top-hat