% Gamma correction: function to improve the contrast of an image. We can
% visualize better the soft tissues if we apply an outward curve function
% or the most dense tissues (bones) if we use an inward curve function.
%...................................................................................................................
% INPUT PARAMETERS
% image: to be equalized
% gamma_factor: factor that specify the curvature of the function that modifies the
% contrast:
    % Gamma = 1 --> lineal function
    % Gamma < 1 --> outward curvature
    % Gamma > 1 --> inward curvature
% low_in, high_in: min and max grey levels we want to consider to modify the histogram
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: contrast improved image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = gamma_Correction (image, gamma_factor, low_in, high_in)

% Image information
imageCorr = image;
high_in = double(high_in);
low_in = double(low_in);
[x,y] = size(image);

% We define and normalize the range to create the LUT
inRange = (low_in:high_in)-low_in;
imageCorr = imageCorr - low_in;
inRange = inRange/(high_in-low_in);

% Look Up Table
LUT = (high_in-low_in)*(inRange.^gamma_factor);

% We have to correct the values that are above the high in
imageCorr(imageCorr>high_in-low_in) = high_in-low_in;
% imageCorr(imageCorr<low_in) = low_in;%new

% We apply the LUT, correct the values that are below the low_in,
% de-normalize the values and, finaly, we reshape the result to form the
% image
equalizedImage = uint16(reshape(LUT(imageCorr(:)+1), [x,y]));
equalizedImage = equalizedImage + low_in;

% We mantain the pixels that are lower or higher than the limits
indexLow = find(image<=low_in);
if ~isempty(indexLow)
    equalizedImage(indexLow) = image(indexLow);
end

