% Gamma correction: function to improve the contrast of an image. We can
% visualize better the soft tissues if we apply an outward curve function
% or the most dense tissues (bones) if we use an inward curve function.
%...................................................................................................................
% INPUT PARAMETERS
% image: to be equalized
% gamma_factor: factor that specify the curvature of the function that modifies the
% contrast:
    % Gamma = 1 --> lineal function
    % Gamma < 1 --> outward curvature
    % Gamma > 1 --> inward curvature
% low_in, high_in: min and max grey levels we want to consider to modify the histogram
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: contrast improved image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = gamma_Correction (image, gamma_factor, low_in, high_in, lowOriginal)

% Image information
lowOriginal = double(lowOriginal);
image = image - lowOriginal;
high_in = double(high_in-lowOriginal);
low_in = double(low_in-lowOriginal);
%To avoid artifacts, when  
if gamma_factor > 1
    low_in = low_in/5;
end
[x,y] = size(image);

% Look Up Table
LUT1 = (0:low_in);
inRange = (0:high_in)/(high_in);
LUT2 = (inRange.^gamma_factor)*(high_in);
LUT_rec = LUT2(low_in+1:end);
LUT_rect = LUT_rec - min(LUT_rec);
LUT_rectNorm = LUT_rect/max(LUT_rect);
LUT3 = LUT_rectNorm*(high_in-max(LUT1));
LUT = [LUT1 LUT3+max(LUT1)];
% LUT1 = (0:low_in);
% inRange = (0:high_in-lowOriginal)/(high_in-lowOriginal);
% LUT2 = (inRange.^gamma_factor)*(high_in);
% LUT_rec = LUT2(low_in+1-lowOriginal:end);
% LUT3 = LUT_rec - min(LUT_rec) + max(LUT1);
% LUT = [LUT1 LUT3];

% Processed image
equalizedImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = equalizedImage + lowOriginal;

