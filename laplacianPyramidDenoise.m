% Algorithm to denoise an image based on its laplacian pyramid decomposition
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be denoised
% factor: of denoising
%...................................................................................................................
% OUTPUT PARAMETERS
% denoisedImage: denoised image
% pyramid: laplacian pyramid to be used in other stages of the processing
% sizesPyramid: matrix with the sizes of the laplacian pyramid sub-bands
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [denoisedImage, pyramid, sizesPyramid] = laplacianPyramidDenoise (image, factor)

% We have to work with float images
image = single(image);

% Initialization
pyramid = [];
sizesPyramid = [];

%If the user set the bar to 0, no denoising stage will be applied
if factor == 0 
    denoisedImage = image;
else
    % Blurred and reduced versions of the previous image
    I0 = impyramid(image, 'reduce');
    I1 = impyramid(I0, 'reduce');
    I2 = impyramid(I1, 'reduce');

    % Resized versions of the blurred and reduced images
    [nr, nc] = size(image);
    R0 = imresize(I0, [nr nc], 'bilinear');
    [nr, nc] = size(I0);
    R1 = imresize(I1, [nr nc], 'bilinear');
    [nr, nc] = size(I1);
    R2 = imresize(I2, [nr nc], 'bilinear');

    % Difference images
    D0 = R0 - image; %Image size
    D1 = R1 - I0; % I0 size
    D2 = R2 - I1; % I1 size

    % Level of denoising control. We impose a maximum value from which we put
    % to 0 all the pixels (because we consider them as noise) while we maintain 
    % the values below this limit (because they correspond to information).
    % Besides, we can set to 0 all the first sub-band because it is mostly composed
    % by noise. However, we establish a restriction for the rest of the bands (since they have more information
    % than noise and there has to be a limit to avoid the blurring).
    D0factor = factor;
    D0(abs(D0)  <= D0factor*max(D0(:))) = 0;  
    D1factor = 0.5*factor;
    D1(abs(D1)  <= D1factor*max(D1(:))) = 0;
    D2factor = 0.1*factor;
    D2(abs(D2)  <= D2factor*max(D2(:))) = 0;

    % We build the pyramid in case we need this decomposition in other stages
    % of the processing. The variable sizesPyramid contains the sizes of each
    % sub-band (this is important since we downsample the images as well as we blur
    % them)
    pyramid(:,:,1) = D0;
    sizesPyramid (1,:) = size(D0);
    pyramid(1:size(D1,1),1:size(D1,2),2) = D1;
    sizesPyramid (2,:) = size(D1);
    pyramid(1:size(D2,1),1:size(D2,2),3) = D2;
    sizesPyramid (3,:) = size(D2);
    pyramid(1:size(I2,1),1:size(I2,2),4) = I2;
    sizesPyramid (4,:) = size(I2);

    % We reconstruct the image from the pyramid
    [nr, nc] = size(D2);
    I1n = imresize(I2, [nr nc], 'bilinear') - D2;
    [nr, nc] = size(D1);
    I0n = imresize(I1n, [nr nc], 'bilinear') - D1;
    [nr, nc] = size(D0);
    denoisedImage = imresize(I0n, [nr nc], 'bilinear') - D0;

    % Data conversion 
    denoisedImage=uint16(denoisedImage);
end



