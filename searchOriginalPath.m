% Function to read a .txt that contains the path to the original image
% and its name. We do it only if the processed image has been
% processed before because, in that case, the .txt has been generated.
%...................................................................................................................
% INPUT PARAMETERS
% comments: is a column cellarray with the comments that were written by the user.
% At the end of the column there is a path that corresponds to the original
% image from which the user performed the processing. We need to load this
% original image again together with the processed one so that the user can
% compare both.
%...................................................................................................................
% OUTPUT PARAMETERS
% pathOriginalImage: folder that contains the original image.
% nameOriginalImage: name of this original image.
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [pathOriginalImage, nameOriginalImage] = searchOriginalPath (comments)

% Variable initialization
pathOriginalImage = [];
nameOriginalImage = [];
path = [];

% Read the comments
myCell = textread (comments, '%s'); 
sizeCell = size(myCell,1); %end of the array

% Find the path
for i = sizeCell: -1: 1
    finalCell = myCell{i,1};
        if ~strcmp (finalCell, 'image:')
            % Join the final cells (because they contain the path) till the
            % string 'image:' appears (it points the beginning of the path)
            path = [finalCell ' ' path];
        else
            break % When we find the word image: means that we have the complete path
        end
end
pathOriginalImage = path;

% Find the name of the image
index = strfind(pathOriginalImage, '\');
nameOriginalImage = pathOriginalImage(index(1,end)+1:end-1);
            
    