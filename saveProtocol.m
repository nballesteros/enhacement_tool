% Function to create a .txt with the protocol selected by the user.
%...................................................................................................................
% INPUT PARAMETERS
% protocol: code name of the processed image
% nameProtocol: chosen name for the protocol
% pathProtocol: chosen folder
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function saveProtocol (protocol, nameProtocol, pathProtocol)

M = { ['PROTOCOL�S NAME:' nameProtocol]
    ' '
    ['PROTOCOL:' protocol] };

 folder = [pathProtocol '\' nameProtocol];
 
 fid = fopen (folder, 'w');
 
[f,c]=size(M);
for i=1:f
    for j=1:c
    fprintf(fid,'%s\t\t',cell2mat(M(i,j)));
    end
    fprintf(fid,'\n');
end

fclose(fid);