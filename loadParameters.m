% Function to load the parameters of a processed image or protocol
%...................................................................................................................
% INPUT PARAMETERS
% nameImage: name of the loaded image (or protocol). The main window analyzes if the image
% has been processed before and, in this case, the parameters specified in
% its name will be placed in the interface automatically. In case of a
% protocol we follow the same procedure.
%...................................................................................................................
% OUTPUT PARAMETERS
% ROIcropped: radiated area and background threshold
% bp: body part (for the intensity correction)
% contrastFilter: algorithm to improve the contrast of the image
% percentageC: % of contrast improvement
% minGrey: threshold. Grey values below are not processed.
% cSoft, cDense, lSoft, lDense: curves and levels for the sigma function (sub-histogram intensity correction)
% gcSoft, gcDense, glSoft, glDense: curves and levels for the gamma function (sub-histogram intensity correction)
% wlSoft, wlDense, llSoft, llDense: windows and levels for the lineal by sections function (sub-histogram intensity correction)
% fSoft, fDense: factors for the soft and the dense images
% noiseFilter: denoising algorithm
% percentageN: % of noise reduction
% thresholdN: thresholding value (wavelet denoising and Perona&Malik filter)
% enhanceFilter: algorithm of edges and details enhancement
% percentageE: % of enhancement
% minSizeSE: min size of the structural element
% maxSizeSE: max size of the structural element
% radius: of the blurring filter for the USM algorithms
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function [ROIcropped, bp, contrastFilter, percentageC, minGrey, cSoft, cDense, lSoft, lDense, gcSoft, gcDense, glSoft, glDense, wlSoft, wlDense, llSoft, llDense, fSoft, fDense, aSoft, aDense, noiseFilter, percentageN, thresholdN, enhanceFilter, percentageE, minSizeSE, maxSizeSE, radius] = loadParameters (nameImage)

% Intensity correction
ROIcropped = [];
bp = [];

% Contrast improvement parameters
contrastFilter = [];
percentageC = [];
minGrey = [];
cSoft = [];
cDense = [];
lSoft = [];
lDense = [];
gcSoft = [];
gcDense = [];
glSoft = [];
glDense = [];
wlSoft = [];
wlDense = [];
llSoft = [];
llDense = [];
fSoft = [];
fDense = [];
aSoft = [];
aDense = [];

% Denoising parameters
noiseFilter = [];
percentageN = [];
thresholdN = [];

%Enhancing parameters
enhanceFilter = [];
percentageE = [];
minSizeSE = [];
maxSizeSE = [];
radius = [];

% If the nameImage corresponds to a previously processed image, it will
% contain brackets so we have to find them
ind = strfind(nameImage, '(');
if ~isempty (ind)

    auxName = nameImage(1,ind+1:end);
    clear nameImage
    nameImage = auxName;

    %---------------------------------------------------------------------------
    % INTENSITY CORRECTION
    %---------------------------------------------------------------------------
    % Find the cropped area from the image
    ix = strfind(nameImage,'&x');
    iy = strfind(nameImage,'&y');
    iw = strfind(nameImage,'&w');
    ih = strfind(nameImage,'&h');
    ib = strfind(nameImage,'&b');
    iend = ih+strfind(nameImage(ih:end),'_#');

    if ~isempty(ix)
        xROI = str2double(nameImage(ix+2:iy-1));
        yROI = str2double(nameImage(iy+2:iw-1));
        wROI = str2double(nameImage(iw+2:ih-1));
        hROI = str2double(nameImage(ih+2:ib-1));
        back = str2double(nameImage(ib+2:iend(1)-2));
        ROIcropped = [xROI yROI wROI hROI back];
    end

    % Find the body part
    index_bp = strfind(nameImage,'#');
    bp = nameImage (index_bp+1:index_bp+2);

    %---------------------------------------------------------------------------
    % CONTRAST ALGORITHM
    %---------------------------------------------------------------------------
    % Type of contrast filter
    C = strfind(nameImage, 'C') + 1;

    if ~isempty (C)
        cFilter = nameImage(1,C:C+1);
        if ~isempty(cFilter)
            switch (cFilter)
                case 'SH' %Subhistogram intensity correction
                    contrastFilter = 1;
                    %Subname
                    indSubname = strfind(nameImage, '^');
                    subname = nameImage(indSubname(1)+1:indSubname(2)-1);
                    i1 = strfind(subname, 'as')+2;
                    i2 = strfind(subname, 'ad')-1;
                    i3 = strfind(subname, 'cs')-1;
                    i4 = strfind(subname, 'cd')-1;
                    i5 = strfind(subname, 'ls')-1;
                    i6 = strfind(subname, 'ld')-1;
                    i7 = strfind(subname, 'cgs')-1;
                    i8 = strfind(subname, 'cgd')-1;
                    i9 = strfind(subname, 'lgs')-1;
                    i10 = strfind(subname, 'lgd')-1;
                    i11 = strfind(subname, 'wls')-1;
                    i12 = strfind(subname, 'wld')-1;
                    i13 = strfind(subname, 'lls')-1;
                    i14 = strfind(subname, 'lld')-1;
                    i15 = strfind(subname, 'fs')-1;
                    i16 = strfind(subname, 'fd')-1;
                    i17 = strfind(subname, '%')-1;
                    
                    aSoft = str2num(subname(i1:i2));
                    aDense = str2num(subname(i2+3:i3));
                    cSoft = str2num(subname(i3+3:i4));
                    cDense = str2num(subname(i4+3:i5));
                    lSoft = str2num(subname(i5+3:i6));
                    lDense = str2num(subname(i6+3:i7));
                    gcSoft = str2num(subname(i7+4:i8));
                    gcDense = str2num(subname(i8+4:i9));
                    glSoft = str2num(subname(i9+4:i10));
                    glDense = str2num(subname(i10+4:i11));
                    wlSoft = str2num(subname(i11+4:i12));
                    wlDense = str2num(subname(i12+4:i13));
                    llSoft = str2num(subname(i13+4:i14));
                    llDense = str2num(subname(i14+4:i15));
                    fSoft = str2num(subname(i15+3:i16));
                    fDense = str2num(subname(i16+3:i17));
                                         
                case 'Ge' %General Equalization
                    contrastFilter = 2;
                    % Min grey (eliminate background)
                    minGreyini = strfind(nameImage, 'min') + 3;
                    minGreyend = minGreyini+strfind(nameImage(1,minGreyini:end),'_')-2;
                    minGrey = str2double(nameImage(1,minGreyini:minGreyend));

                case 'Gm' %Gamma correction
                    contrastFilter= 3;
                    % Min grey (eliminate background)
                    minGreyini = strfind(nameImage, 'min') + 3;
                    minGreyend = minGreyini+strfind(nameImage(1,minGreyini:end),'_')-2;
                    minGrey = str2double(nameImage(1,minGreyini:minGreyend));
                    
                case 'No' %None
                    contrastFilter = 4;
            end
        end
    else
        contrastFilter = [];
    end
    
    %---------------------------------------------------------------------------
    % DENOISING ALGORITHM
    %---------------------------------------------------------------------------
    % Type of noise filter
    N = strfind(nameImage, '_N') + 1;

    if ~isempty (N)
        nFilter = nameImage(1,N+1:N+2);
        if ~isempty(nFilter)
            switch (nFilter)
                
                case 'Wa' %Wavelet denoising
                    noiseFilter = 1;
                    % threshold
                    th = strfind(nameImage, 'th') + 2;
                    thresholdN = str2double(nameImage(th:th+3));
                    % type of wavelet
                    wtype = strfind(nameImage,'*')+1;

                case 'Py' %Laplacian pyramid denoising
                    noiseFilter = 2;

                case 'Nl' %Non-local means
                    noiseFilter = 3;
 
                case 'No' %None
                    noiseFilter = 4;
            end
        end
    else
        noiseFilter = [];
    end

    %---------------------------------------------------------------------------
    % ENHANCEMENT ALGORITHM
    %---------------------------------------------------------------------------
    E = strfind(nameImage, 'E') + 1;
    if ~isempty (E)
        eFilter = nameImage(1,E:E+1);
        if ~isempty(eFilter)
            switch (eFilter)

                case 'Us' %USM spatial
                    enhanceFilter = 1;
                    % Radius of the USM filter
                    Ri = strfind(nameImage, '_R') + 1;
                    Rf = Ri+strfind(nameImage(Ri:end),'_%');
                    radius = nameImage(Ri+1:Rf-2);
                    
                case 'Uw' %USM wavelet
                    enhanceFilter = 2;
                    % Radius of the USM filter
                    Ri = strfind(nameImage, '_R') + 1;
                    Rf = Ri+strfind(nameImage(Ri:end),'_%');
                    radius = nameImage(Ri+1:Rf-2);

                case 'Mw' %Morpho wave
                    enhanceFilter = 3;
                    % Form of the SE
                    f = strfind(nameImage, 'f') + 1;

                    % Min size of the SE
                    sMini = strfind(nameImage, '_sMi') + 4;
                    sMinf = strfind(nameImage, '_sMa');
                    minSizeSE = nameImage(sMini:sMinf-1);
                    % Max size of the SE
                    sMaxi = sMinf + 4;
                    sMaxf = sMaxi+strfind(nameImage(sMaxi:end),'_%')-2;
                    maxSizeSE = nameImage(sMaxi:sMaxf);
                    
                case 'No' %None
                    enhanceFilter = 4;
            end
        end
    else
        enhanceFilter = [];
    end

    %---------------------------------------------------------------------------
    % PERCENTAGES
    %---------------------------------------------------------------------------    
    % Find percentages
    percentages = strfind(nameImage, '%');
    if ~isempty(percentages)
        PCi = percentages(1,1)+1;
        PNi = percentages(1,2)+1;
        PEi = percentages(1,3)+1;
    else
        PCi = [];
        PNi = [];
        PEi = [];
    end
    
    % 1) Contrast percentage
    finalPercentageC = strfind(nameImage(PCi:end), '_');
    if ~isempty(finalPercentageC)
        PCf = PCi+finalPercentageC(1,1) - 2;
    else
        PCf = [];
    end
    if ~isempty(PCi) && ~isempty(PCf)
        namePerc = nameImage(PCi:PCf);
        if strcmp(namePerc, '0^')
            percentageC = 0;
        else
            percentageC = str2num(namePerc);
        end
    else
        percentageC = [];
    end

    % 2) Denoising percentage
    finalPercentageN = strfind(nameImage(PNi:end), '_');
    if ~isempty(finalPercentageN)
        PNf = PNi+finalPercentageN(1,1) - 2;
    else
        PNf = [];
    end
    if ~isempty(PNi) && ~isempty(PNf)
        percentageN = str2double(nameImage(PNi:PNf));
    else
        percentageN = [];
    end

    % 3) Enhancement percentage
    finalPercentageE = strfind(nameImage(PEi:end), ')');
    if ~isempty(finalPercentageE)
        PEf = PEi+finalPercentageE(1,1) - 2;
    else
        PEf = [];
    end
    if ~isempty(PEi) && ~isempty(PEf)
        percentageE = str2double(nameImage(PEi:PEf));
    else
        percentageE = [];
    end
    
end