% Function to create a .txt with the comments that have been written by the user.
%...................................................................................................................
% INPUT PARAMETERS
% initImage: quality of the image we want to process
% improvement: level of the improvement after the processing
% comment: extra information
% author: person who has processed the image
% nameImage and path: of the processed image in order to be able to save the comments in the same folder of this image.
% originalFilePath: path of the original image
% checkProtocol: indicates if the image has been processed with a protocol or not
% protocol: from which the image has been processed (if checkProtocol == 1)
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function saveComments (initImage, improvement, comment, author, nameImage, path, originalFilePath, checkProtocol, protocol)

% If the user says the image has been processed with a protocol but the
% field protocol is empty
if (checkProtocol == 1)&&(isempty(protocol))
    protocol = 'There was not any loaded protocol to be applied';
end

% When the image ends with a point, we have to obviate this character
if (strfind(nameImage,'.') == size(nameImage,2)) 
    aux = nameImage(1,1:end-1);
    nameImage = aux;
end

% Filter the options selected by the user
initImageOption = find(initImage == 1);
improveOption = find(improvement == 1);
authorOption = find(author == 1);

if initImageOption == 1
    initQuality = '0 --> BAD';
elseif initImageOption == 2
    initQuality = '1 --> NORMAL';
else
    initQuality = '2 --> EXCELENT';
end

if improveOption == 1
    level = '0 --> NOTHING';
elseif improveOption == 2
    level = '1 --> IRRELEVANT IMPROVEMENT';
elseif improveOption == 3
    level = '2 --> SUBSTANTIAL IMPROVEMENT';
else
    level = '3 --> CLINICAL RELEVANCE';
end

if authorOption == 1
    theAuthor = '0 --> M. ISABEL GARCIA';
elseif authorOption == 2
    theAuthor = '1 --> ALICIA CARO';
else
    theAuthor = '2 --> JES�S RODR�GUEZ';
end

% We join the cells in case we have put an enter into the comments (in
% order to avoid problems in the characters of the text)
aux1 = [];
if size(comment,1) > 1
    com = cellstr(comment);
    for i = 1:size(com,1)
        aux1 = [aux1 com{i,1} ' '];
    end
    clear comment
    comment = aux1;
end

% If the image has not been processed with a protocol...
if checkProtocol == 0

    M={'FEEDBACK OF THE VETERINARY EXPERTS ABOUT THE PROCESSED IMAGE'
        ' '
        '****************************************************************'
        'Initial Image Quality (before processing):'
        ' '
        initQuality
        ' '
        '****************************************************************'
        'Improvement Level (after processing):'
        ' '
        level
        ' '
        '****************************************************************'
        'Comments'
        ' '
        comment
        ' '
        '****************************************************************'
        'Author of the processing:'
        ' '
        theAuthor
        ' '
        '****************************************************************'
        ' '
        ' '
        ' '
        'Path to the original image: '
        originalFilePath
        ' '};
    
else % If protocol exists...
    M={'FEEDBACK OF THE VETERINARY EXPERTS ABOUT THE PROCESSED IMAGE'
        ' '
        '****************************************************************'
        'Initial Image Quality (before processing):'
        ' '
        initQuality
        ' '
        '****************************************************************'
        'Improvement Level (after processing):'
        ' '
        level
        ' '
        '****************************************************************'
        'Comments'
        ' '
        comment
        ' '
         '****************************************************************'
        'Applied protocol�s name'
        ' '
        protocol
        ' '
        '****************************************************************'
        'Author of the processing:'
        ' '
        theAuthor
        ' '
        '****************************************************************'
        ' '
        ' '
        ' '
        'Path to the original image: '
        originalFilePath
        ' '};
end

% Path where the comments will be saved
mypath = [path nameImage '.txt'];

fid=fopen(mypath,'w');
[f,c]=size(M);
for i=1:f
    for j=1:c
        fprintf(fid,'%s\t\t',cell2mat(M(i,j)));
    end
    fprintf(fid,'\n');
end

fclose(fid);

