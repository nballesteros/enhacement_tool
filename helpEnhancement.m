function varargout = helpEnhancement(varargin)
% HELPENHANCEMENT MATLAB code for helpEnhancement.fig
%      HELPENHANCEMENT, by itself, creates a new HELPENHANCEMENT or raises the existing
%      singleton*.
%
%      H = HELPENHANCEMENT returns the handle to a new HELPENHANCEMENT or the handle to
%      the existing singleton*.
%
%      HELPENHANCEMENT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HELPENHANCEMENT.M with the given input arguments.
%
%      HELPENHANCEMENT('Property','Value',...) creates a new HELPENHANCEMENT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before helpEnhancement_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to helpEnhancement_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help helpEnhancement

% Last Modified by GUIDE v2.5 15-Sep-2015 16:14:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @helpEnhancement_OpeningFcn, ...
                   'gui_OutputFcn',  @helpEnhancement_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before helpEnhancement is made visible.
function helpEnhancement_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to helpEnhancement (see VARARGIN)

% Choose default command line output for helpEnhancement
handles.output = hObject;

% Default percentage 25%
handles.percentage = 25;
set(handles.radiobutton25,'Value',1)
popupmenu_algorithms_Callback(hObject, eventdata, handles)


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes helpEnhancement wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = helpEnhancement_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu_algorithms.
function popupmenu_algorithms_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_algorithms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_algorithms contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_algorithms

% Disable the option of closing the window until showing all the images
set(gcf, 'closerequestfcn', '');

% Original image is the same for all algorithms
originalIm = dicomread([pwd '\Example_images\enhanceOriginal']);

% We have to use the same window in order to compare the effects of the
% algorithms properly
windIm = [min(originalIm(:)), max(originalIm(:))];
perc = num2str(handles.percentage);

% Images and texts
processed1 = dicomread([pwd '\Example_images\enhanceUSMSpatial' perc]);
text1 = 'USM in Spatial Domain and radius 2.5';
processed2 = dicomread([pwd '\Example_images\enhanceUSMWavelet' perc]);
text2 = 'USM in Wavelet Domain and radius 2.5';
processed3 = dicomread([pwd '\Example_images\enhanceMorphoWavelet' perc '_min5max8']);
text3 = 'M.P. Wavelet Domain (s.e. diamond, minSize 5, maxSize 8)';

% Show the images with a scale of 0.5 to speed up the code
axes(handles.axes1)
imshow(imresize(originalIm,0.5),windIm)
axes(handles.axes2)
imshow(imresize(processed1,0.5),windIm)
set(handles.text2, 'String', text1)
axes(handles.axes3)
imshow(imresize(processed2,0.5),windIm)
set(handles.text3, 'String', text2)
axes(handles.axes4)
imshow(imresize(processed3,0.5),windIm)
set(handles.text4, 'String', text3)
 
% Linking the axes in order to be able to apply the zoom tool at once
h(1)=handles.axes1;
h(2)=handles.axes2;
h(3)=handles.axes3;
h(4)=handles.axes4;
linkaxes (h, 'xy')

% Enable the option of closing the window 
set(gcf, 'closerequestfcn', 'closereq');

% Update handles structure
guidata(handles.figure1,handles);
    
% --- Executes on button press in radiobutton25.
function radiobutton25_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton25

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)
end

% Save the percentage to show the correct images
handles.percentage = 25;
popupmenu_algorithms_Callback(hObject, eventdata, handles)

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in radiobutton50.
function radiobutton50_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton50 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton50
if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton100, 'Value') == 1)
    set(handles.radiobutton100, 'Value', 0)
end

% Save the percentage to show the correct images
handles.percentage = 50;
popupmenu_algorithms_Callback(hObject, eventdata, handles)

% Update handles structure
guidata(handles.figure1,handles);


% --- Executes on button press in radiobutton100.
function radiobutton100_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton100 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton100
if (get(handles.radiobutton25, 'Value') == 1)
    set(handles.radiobutton25, 'Value', 0)
end

if (get(handles.radiobutton50, 'Value') == 1)
    set(handles.radiobutton50, 'Value', 0)
end

% Save the percentage to show the correct images
handles.percentage = 100;
popupmenu_algorithms_Callback(hObject, eventdata, handles)

% Update handles structure
guidata(handles.figure1,handles);
