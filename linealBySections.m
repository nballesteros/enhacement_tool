function equalizedImage = linealBySections (image, high_in, window, level, lowOriginal, typeImage)

% Image information
lowOriginal = double(lowOriginal);
image = image - lowOriginal;
high_in = double(high_in-lowOriginal);
low_in = min(image(:));
[x,y]=size(image);
pte = 2;

windSize = window * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
if typeImage == 0 %soft
    level = (level/2)*(high_in-low_in)+low_in;
else
    level = ((level/2)+0.5)*(high_in-low_in)+low_in;
end
ind1 = round(level - 0.5*windSize);
ind2 = round(level + 0.5*windSize);
LUT1 = (0:ind1);
LUT2 = pte*(ind1+1:ind2);
if isempty (LUT1)
    maxLUT1 = 0;
else
    maxLUT1 = max(LUT1);
end
diff = min(LUT2) - maxLUT1;
LUT2 = LUT2 - diff;
LUT = [LUT1 LUT2];
LUT3 = ind2+1:high_in;
diff2 = max(LUT) - min(LUT3);
LUT3 = LUT3 + diff2;
LUT_final = [LUT LUT3];
LUT_norm = double(LUT_final)/double(max(LUT_final));
LUT = LUT_norm * high_in;

% We apply the LUT
equalizedImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = equalizedImage + lowOriginal;