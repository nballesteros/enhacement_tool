% SIGMA TRANSFORM
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% curve: degree of sigma curvature
% level: level (position of the center of the sigma)
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with better contrast
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = sigmaTransformold(image, curve, level, minIm, maxIm, lowOriginal)

% Image size
[x,y] = size(image);

% Maximum of the image
% maxIm = double(max(image(:)));
% minIm = double(min(image(:)));
maxIm = double(maxIm);
minIm = double(minIm);

% Create the SIGMA LUT
inRange = (minIm:maxIm)-minIm;
inRange = inRange/(maxIm-minIm);
image = image - minIm;

LUT = sigmf(inRange,[1+(9*curve) level]);
LUT = LUT - min(LUT);
LUT = (maxIm-minIm) * (LUT/max(LUT));

% Sigma image where we have applied the sigma LUT
sigmaImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = sigmaImage + minIm;
