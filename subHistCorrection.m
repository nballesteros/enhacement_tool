function varargout = subHistCorrection(varargin)
%SUBHISTCORRECTION M-file for subHistCorrection.fig
%      SUBHISTCORRECTION, by itself, creates a new SUBHISTCORRECTION or raises the existing
%      singleton*.
%
%      H = SUBHISTCORRECTION returns the handle to a new SUBHISTCORRECTION or the handle to
%      the existing singleton*.
%
%      SUBHISTCORRECTION('Property','Value',...) creates a new SUBHISTCORRECTION using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to subHistCorrection_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      SUBHISTCORRECTION('CALLBACK') and SUBHISTCORRECTION('CALLBACK',hObject,...) call the
%      local function named CALLBACK in SUBHISTCORRECTION.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help subHistCorrection

% Last Modified by GUIDE v2.5 26-Jul-2017 11:52:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @subHistCorrection_OpeningFcn, ...
                   'gui_OutputFcn',  @subHistCorrection_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before subHistCorrection is made visible.
function subHistCorrection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for subHistCorrection
handles.output = hObject;

% Get parameters from the main window
handles.image = getappdata(0,'image'); % Image
handles.equalizedImage = handles.image;

% Type of algorithm for the soft and the dense tissues
handles.algSoft = getappdata(0, 'algSoft');
handles.algDense = getappdata(0, 'algDense');

% Level-Curve sigma transform
handles.valueLevelSoft = getappdata(0,'valueLevelSoft'); 
handles.valueLevelDense = getappdata(0,'valueLevelDense'); 
handles.valueCurveSoft = getappdata(0, 'valueCurveSoft'); 
handles.valueCurveDense = getappdata(0, 'valueCurveDense'); 

% Level-Curve gamma transform
handles.valueCurveGammaSoft = getappdata(0, 'valueCurveGammaSoft');
handles.valueCurveGammaDense = getappdata(0, 'valueCurveGammaDense');
handles.valueLevelGammaSoft = getappdata(0, 'valueLevelGammaSoft');
handles.valueLevelGammaDense = getappdata(0, 'valueLevelGammaDense');

% Level-Window lineal transform
handles.valueLevelLinealSoft = getappdata(0,'valueLevelLinealSoft'); 
handles.valueLevelLinealDense = getappdata(0,'valueLevelLinealDense'); 
handles.valueWindowLinealSoft = getappdata(0, 'valueWindowLinealSoft'); 
handles.valueWindowLinealDense = getappdata(0, 'valueWindowLinealDense'); 
handles.pte = 2;

% Weighting
handles.factorSoft = getappdata(0, 'valueFactorSoft');
handles.factorDense = getappdata(0, 'valueFactorDense');

%Background
handles.background = getappdata(0, 'background');

% Set the parameters in the interface
if handles.algSoft == 1
    set(handles.curve_soft, 'Value', handles.valueCurveSoft)
    set(handles.level_soft, 'Value', handles.valueLevelSoft)
elseif handles.algSoft == 2
    set(handles.curve_soft, 'Value', handles.valueCurveGammaSoft)
    set(handles.level_soft, 'Value', handles.valueLevelGammaSoft)
else
    set(handles.curve_soft, 'Value', handles.valueWindowLinealSoft)
    set(handles.level_soft, 'Value', handles.valueLevelLinealSoft)
end

if handles.algDense == 1
    set(handles.curve_dense, 'Value', handles.valueCurveDense)
    set(handles.level_dense, 'Value', handles.valueLevelDense)
elseif handles.algDense == 2
    set(handles.curve_dense, 'Value', handles.valueCurveGammaDense)
    set(handles.level_dense, 'Value', handles.valueLevelGammaDense)
else
    set(handles.curve_dense, 'Value', handles.valueWindowLinealDense)
    set(handles.level_dense, 'Value', handles.valueLevelLinealDense)
end

if (handles.factorSoft == 0) && (handles.factorDense == 0)
    set(handles.factor_soft,'Enable','off')
    set(handles.factor_dense,'Enable','off')
    set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
    set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
else
    set(handles.factor_soft, 'Value', handles.factorSoft)
    set(handles.factor_dense, 'Value', handles.factorDense)
end
set(handles.popupmenu_soft, 'Value', handles.algSoft)
set(handles.popupmenu_dense, 'Value', handles.algDense)

% Maximum of the image
handles.maxIm = double(max(handles.image(:))); 
handles.minIm = double(min(handles.image(:))); 

% If the user has selected a background, we work only with the levels above

handles.minOriginal = handles.minIm;

if handles.background > handles.minIm
    handles.minIm = handles.background;
end

%--------------------------------------------------------------------------
% LUT SIGMA or GAMMA
%--------------------------------------------------------------------------
% Range
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

% Depending on the algorithm selected, the subimage is processed in a
% different way and the LUT that is going to be shown also changes
if handles.algSoft == 1
    low_in = double(handles.minIm);
    high_in = double(handles.maxIm);
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) handles.valueLevelSoft*0.5])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT3 = 0.75*length(LUTsigma)+1:length(LUTsigma);
    LUT = [LUT1 LUT2 LUT3-min(LUT3)+max(LUT2)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = sigmaTransform(handles.image, handles.valueCurveSoft, handles.valueLevelSoft, handles.minIm, handles.maxIm, handles.minOriginal, 1);
    set(handles.level_soft, 'Value', handles.valueLevelSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveSoft)
    set(handles.text_curve_soft, 'String', 'Slope')
elseif handles.algSoft == 2
    gamSoft = 1 -(handles.valueCurveGammaSoft*4/5);
    low_in = round(handles.minIm + handles.valueLevelGammaSoft*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);
    
    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamSoft)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT1 LUT3+max(LUT1)];
    LUTsoft = LUT/max(LUT);
    handles.softImage = gamma_Correction (handles.image, gamSoft, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_soft, 'Value', handles.valueLevelGammaSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveGammaSoft)
    set(handles.text_curve_soft, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealSoft * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealSoft*(high_in-low_in)+low_in;
    level = (handles.valueLevelLinealSoft/2)*(high_in-low_in)+low_in;
    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
%     handles.softImage = linealBySections (handles.image, handles.maxIm, handles.valueWindowLinealSoft, handles.valueLevelLinealSoft, handles.minOriginal);
    handles.softImage = linealBySections (handles.image, handles.maxIm, handles.valueWindowLinealSoft, handles.valueLevelLinealSoft, handles.minOriginal, 0);
    set(handles.level_soft, 'Value', handles.valueLevelLinealSoft)
    set(handles.curve_soft, 'Value', handles.valueWindowLinealSoft)
    set(handles.text_curve_soft, 'String', 'Window')
end

if handles.algDense == 1
    low_in = handles.minIm;
    high_in = handles.maxIm;
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) 0.5+(handles.valueLevelSoft*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT = [LUT1 LUT2-min(LUT2)+max(LUT1)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = sigmaTransform(handles.image, handles.valueCurveDense, handles.valueLevelDense, handles.minIm, handles.maxIm, handles.minOriginal, 0);
    set(handles.level_dense, 'Value', handles.valueLevelDense)
    set(handles.curve_dense, 'Value', handles.valueCurveDense)
    set(handles.text_curve_dense, 'String', 'Slope')
    
elseif handles.algDense == 2
    gamDense = (handles.valueCurveGammaDense * 4) + 1;
    low_in = round(handles.minIm + handles.valueLevelGammaDense*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);

    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamDense)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTdense = LUT/max(LUT);
    handles.denseImage = gamma_Correction (handles.image, gamDense, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_dense, 'Value', handles.valueLevelGammaDense)
    set(handles.curve_dense, 'Value', handles.valueCurveGammaDense)
    set(handles.text_curve_dense, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
 
    windSize = handles.valueWindowLinealDense * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealDense*(high_in-low_in)+low_in;
    level = ((handles.valueLevelLinealDense/2)+0.5)*(high_in-low_in)+low_in;
    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = linealBySections(handles.image, handles.maxIm, handles.valueWindowLinealDense, handles.valueLevelLinealDense, handles.minOriginal, 1);
    set(handles.level_dense, 'Value', handles.valueLevelLinealDense)
    set(handles.curve_dense, 'Value', handles.valueWindowLinealDense)
    set(handles.text_curve_dense, 'String', 'Window')
end

handles.LUTvisualSoft = LUTsoft - min(LUTsoft);
handles.LUTvisualDense = LUTdense - min(LUTdense);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[handles.binsOriginal,handles.histImageOriginal] = imhist(handles.image,2^16);
[binsSoft,histSoft] = imhist(handles.softImage,2^16);
[binsDense,histDense] = imhist(handles.denseImage,2^16);
binsSoft = handles.maxIm*(binsSoft/max(binsSoft(:)));
binsDense = handles.maxIm*(binsDense/max(binsDense(:)));
handles.binsOriginal = handles.maxIm*(handles.binsOriginal/max(handles.binsOriginal(:)));

%--------------------------------------------------------------------------
% SHOW IMAGES, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------
% Max an min of each image
minSoft = min(handles.softImage(:));
maxSoft = max(handles.softImage(:));
minDense = min(handles.denseImage(:));
maxDense = max(handles.denseImage(:));

% SOFT IMAGE/LUT VISUALIZATION
axes(handles.axes3);
% Soft Image Histogram
bar(histSoft(minSoft+2:maxSoft),binsSoft(minSoft+2:maxSoft), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightSoft = max(binsSoft(minSoft+2:maxSoft));
minGreySoft = minSoft+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on
%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreySoft,minGreyOr));
endLUT = length(handles.LUTvisualSoft);%double(max(maxSoft,handles.maxIm));
maxLUT = min(maxHeightOr,maxHeightSoft);
minLUT = handles.LUTvisualSoft(iniLUT);
LUTvisual = handles.LUTvisualSoft(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Soft Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Soft Image
axes(handles.axes4);
%[bins,histImage] = imhist(handles.softImage,2^16);
[minIm, maxIm] = imageRange(handles.softImage);
imshow(imresize(handles.softImage,0.25),[minIm maxIm])

% DENSE IMAGE/LUT VISUALIZATION
axes(handles.axes1);
% Dense Image Histogram
bar(histDense(minDense+2:maxDense),binsDense(minDense+2:maxDense), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightDense = max(binsDense(minDense+2:maxDense));
minGreyDense = minDense+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on

%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreyDense,minGreyOr));
endLUT = length(handles.LUTvisualDense);
maxLUT = min(maxHeightOr,maxHeightDense);
minLUT = handles.LUTvisualDense(iniLUT);
LUTvisual = handles.LUTvisualDense(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Dense Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Dense Image
axes(handles.axes2);
%[bins,histImage] = imhist(handles.denseImage,2^16);
[minIm, maxIm] = imageRange(handles.denseImage);
imshow(imresize(handles.denseImage,0.25),[minIm maxIm])

% FINAL IMAGE: show final image if factors have been selected before. If
% not, no image is going to be visualized until the user selects the
% weighting factors
if (handles.factorSoft ~= 0)&&(handles.factorDense ~= 0)
    axes(handles.axes5)
    % Equalized image.
    sImage = handles.softImage - min(handles.softImage(:));
    dImage = handles.denseImage - min(handles.denseImage(:));
    equalizedImage = (handles.factorSoft*sImage + handles.factorDense*dImage);
    diffIm = max(handles.image(:))-max(handles.equalizedImage(:));
    handles.equalizedImage = equalizedImage + diffIm;
    %[bins,histImage] = imhist(handles.equalizedImage,2^16);
    [minIm, maxIm] = imageRange(handles.equalizedImage);
    imshow(handles.equalizedImage,[minIm maxIm])
    set(handles.pushbutton_amplifyEq, 'Enable', 'on', 'BackgroundColor', [0.729 0.831 0.957], 'ForegroundColor', [0.204 0.302 0.494])
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes subHistCorrection wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = subHistCorrection_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on slider movement.
function curve_soft_Callback(hObject, eventdata, handles)
% hObject    handle to curve_soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%Take the algorithm from the menu and its parameters 
handles.algSoft = get(handles.popupmenu_soft,'Value');

if handles.algSoft == 1
    handles.valueCurveSoft = get(handles.curve_soft, 'Value');
elseif handles.algSoft == 2
    handles.valueCurveGammaSoft = get(handles.curve_soft, 'Value');
else
    handles.valueWindowLinealSoft = get(handles.curve_soft, 'Value');
end

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

% Soft LUT and image
if handles.algSoft == 1
    low_in = double(handles.minIm);
    high_in = double(handles.maxIm);
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) handles.valueLevelSoft*0.5])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT3 = 0.75*length(LUTsigma)+1:length(LUTsigma);
    LUT = [LUT1 LUT2 LUT3-min(LUT3)+max(LUT2)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = sigmaTransform(handles.image, handles.valueCurveSoft, handles.valueLevelSoft, handles.minIm, handles.maxIm, handles.minOriginal, 1);
    set(handles.level_soft, 'Value', handles.valueLevelSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveSoft)
    set(handles.text_curve_soft, 'String', 'Slope')
elseif handles.algSoft == 2
    gamSoft = 1 -(handles.valueCurveGammaSoft*4/5);
    low_in = round(handles.minIm + handles.valueLevelGammaSoft*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);
    
    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamSoft)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTsoft = LUT/max(LUT);
    handles.softImage = gamma_Correction (handles.image, gamSoft, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_soft, 'Value', handles.valueLevelGammaSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveGammaSoft)
    set(handles.text_curve_soft, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealSoft * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealSoft*(high_in-low_in)+low_in;
    level = (handles.valueLevelLinealSoft/2)*(high_in-low_in)+low_in;

    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = linealBySections (handles.image, handles.maxIm, handles.valueWindowLinealSoft, handles.valueLevelLinealSoft, handles.minOriginal, 0);
    set(handles.level_soft, 'Value', handles.valueLevelLinealSoft)
    set(handles.curve_soft, 'Value', handles.valueWindowLinealSoft)
    set(handles.text_curve_soft, 'String', 'Window')
end

handles.LUTvisualSoft = LUTsoft - min(LUTsoft);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsSoft,histSoft] = imhist(handles.softImage,2^16);
binsSoft = handles.maxIm*(binsSoft/max(binsSoft(:)));

% Max an min of each image
minSoft = min(handles.softImage(:));
maxSoft = max(handles.softImage(:));

% SOFT IMAGE/LUT VISUALIZATION
axes(handles.axes3);
% Soft Image Histogram
bar(histSoft(minSoft+2:maxSoft),binsSoft(minSoft+2:maxSoft), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightSoft = max(binsSoft(minSoft+2:maxSoft));
minGreySoft = minSoft+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on
%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreySoft,minGreyOr));
endLUT = length(handles.LUTvisualSoft);%double(max(maxSoft,handles.maxIm));
maxLUT = min(maxHeightOr,maxHeightSoft);
minLUT = handles.LUTvisualSoft(iniLUT);
LUTvisual = handles.LUTvisualSoft(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Soft Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Soft Image
axes(handles.axes4);
%[bins,histImage] = imhist(handles.softImage,2^16);
[minIm, maxIm] = imageRange(handles.softImage);
imshow(imresize(handles.softImage,0.25),[minIm maxIm])

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Clear processed image
axes(handles.axes5)
cla

% Update handles structure
guidata(hObject, handles);

% --- Executes on slider movement.
function level_soft_Callback(hObject, eventdata, handles)
% hObject    handle to level_soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%Take the algorithm from the menu and its parameters
handles.algSoft = get(handles.popupmenu_soft,'Value');

if handles.algSoft == 1
    handles.valueLevelSoft = get(handles.level_soft, 'Value');
elseif handles.algSoft == 2
    handles.valueLevelGammaSoft = get(handles.level_soft, 'Value');
else
    handles.valueLevelLinealSoft = get(handles.level_soft, 'Value');
end

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);      

% Soft LUT and image
if handles.algSoft == 1
    low_in = double(handles.minIm);
    high_in = double(handles.maxIm);
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) handles.valueLevelSoft*0.5])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT3 = 0.75*length(LUTsigma)+1:length(LUTsigma);
    LUT = [LUT1 LUT2 LUT3-min(LUT3)+max(LUT2)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = sigmaTransform(handles.image, handles.valueCurveSoft, handles.valueLevelSoft, handles.minIm, handles.maxIm, handles.minOriginal, 1);
    set(handles.level_soft, 'Value', handles.valueLevelSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveSoft)
    set(handles.text_curve_soft, 'String', 'Slope')
elseif handles.algSoft == 2
    gamSoft = 1 -(handles.valueCurveGammaSoft*4/5);
    low_in = round(handles.minIm + handles.valueLevelGammaSoft*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);
    
    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamSoft)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTsoft = LUT/max(LUT);
    handles.softImage = gamma_Correction (handles.image, gamSoft, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_soft, 'Value', handles.valueLevelGammaSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveGammaSoft)
    set(handles.text_curve_soft, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealSoft * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealSoft*(high_in-low_in)+low_in;
    level = (handles.valueLevelLinealSoft/2)*(high_in-low_in)+low_in;

    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = linealBySections (handles.image, handles.maxIm, handles.valueWindowLinealSoft, handles.valueLevelLinealSoft, handles.minOriginal, 0);
    set(handles.level_soft, 'Value', handles.valueLevelLinealSoft)
    set(handles.curve_soft, 'Value', handles.valueWindowLinealSoft)
    set(handles.text_curve_soft, 'String', 'Window')
end

handles.LUTvisualSoft = LUTsoft - min(LUTsoft);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsSoft,histSoft] = imhist(handles.softImage,2^16);
binsSoft = handles.maxIm*(binsSoft/max(binsSoft(:)));

% Max an min of each image
minSoft = min(handles.softImage(:));
maxSoft = max(handles.softImage(:));

% SOFT IMAGE/LUT VISUALIZATION
axes(handles.axes3);
% Soft Image Histogram
bar(histSoft(minSoft+2:maxSoft),binsSoft(minSoft+2:maxSoft), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightSoft = max(binsSoft(minSoft+2:maxSoft));
minGreySoft = minSoft+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on
%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreySoft,minGreyOr));
endLUT = length(handles.LUTvisualSoft);%double(max(maxSoft,handles.maxIm));
maxLUT = min(maxHeightOr,maxHeightSoft);
minLUT = handles.LUTvisualSoft(iniLUT);
LUTvisual = handles.LUTvisualSoft(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Soft Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Soft Image
hold off
axes(handles.axes4);
%[bins,histImage] = imhist(handles.softImage,2^16);
[minIm, maxIm] = imageRange(handles.softImage);
imshow(imresize(handles.softImage,0.25),[minIm maxIm])

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Clear processed image
axes(handles.axes5)
cla

% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in popupmenu_soft.
function popupmenu_soft_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_soft contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_soft

%Take the algorithm from the menu 
handles.algSoft = get(handles.popupmenu_soft,'Value');

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

% Soft LUT and image
if handles.algSoft == 1
    low_in = double(handles.minIm);
    high_in = double(handles.maxIm);
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) handles.valueLevelSoft*0.5])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT2 = LUT2-min(LUT2)+max(LUT1);
    LUT3 = 0.75*length(LUTsigma)+1:length(LUTsigma);
    LUT = [LUT1 LUT2 LUT3-min(LUT3)+max(LUT2)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = sigmaTransform(handles.image, handles.valueCurveSoft, handles.valueLevelSoft, handles.minIm, handles.maxIm, handles.minOriginal, 1);
    set(handles.level_soft, 'Value', handles.valueLevelSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveSoft)
    set(handles.text_curve_soft, 'String', 'Slope')
elseif handles.algSoft == 2
    gamSoft = 1 -(handles.valueCurveGammaSoft*4/5);
    low_in = round(handles.minIm + handles.valueLevelGammaSoft*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);
    
    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamSoft)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTsoft = LUT/max(LUT);
    handles.softImage = gamma_Correction (handles.image, gamSoft, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_soft, 'Value', handles.valueLevelGammaSoft)
    set(handles.curve_soft, 'Value', handles.valueCurveGammaSoft)
    set(handles.text_curve_soft, 'String', 'Curve')

else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealSoft * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealSoft*(high_in-low_in)+low_in;
    level = (handles.valueLevelLinealSoft/2)*(high_in-low_in)+low_in;

    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTsoft = LUT/max(LUT);
    handles.softImage = linealBySections (handles.image, handles.maxIm, handles.valueWindowLinealSoft, handles.valueLevelLinealSoft, handles.minOriginal, 0);
    set(handles.level_soft, 'Value', handles.valueLevelLinealSoft)
    set(handles.curve_soft, 'Value', handles.valueWindowLinealSoft)
    set(handles.text_curve_soft, 'String', 'Window')
end
handles.LUTvisualSoft = LUTsoft - min(LUTsoft);
%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsSoft,histSoft] = imhist(handles.softImage,2^16);
binsSoft = handles.maxIm*(binsSoft/max(binsSoft(:)));

% Max an min of each image
minSoft = min(handles.softImage(:));
maxSoft = max(handles.softImage(:));

% SOFT IMAGE/LUT VISUALIZATION
axes(handles.axes3);
% Soft Image Histogram
bar(histSoft(minSoft+2:maxSoft),binsSoft(minSoft+2:maxSoft), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightSoft = max(binsSoft(minSoft+2:maxSoft));
minGreySoft = minSoft+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on
%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreySoft,minGreyOr));
endLUT = length(handles.LUTvisualSoft);%double(max(maxSoft,handles.maxIm));
maxLUT = min(maxHeightOr,maxHeightSoft);
minLUT = handles.LUTvisualSoft(iniLUT);
LUTvisual = handles.LUTvisualSoft(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Soft Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Soft Image
hold off
axes(handles.axes4);
%[bins,histImage] = imhist(handles.softImage,2^16);
[minIm, maxIm] = imageRange(handles.softImage);
imshow(imresize(handles.softImage,0.25),[minIm maxIm])

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Clear processed image
axes(handles.axes5)
cla

% Update handles structure
guidata(hObject, handles);

% --- Executes on slider movement.
function level_dense_Callback(hObject, eventdata, handles)
% hObject    handle to level_dense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
% Curve of the sigma function

%Take the algorithm from the menu and its parameters 
handles.algDense = get(handles.popupmenu_dense,'Value');

if handles.algDense == 1
    handles.valueLevelDense = get(handles.level_dense, 'Value');
elseif handles.algDense == 2
    handles.valueLevelGammaDense = get(handles.level_dense, 'Value');
else
    handles.valueLevelLinealDense = get(handles.level_dense, 'Value');
end

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

if handles.algDense == 1
    low_in = handles.minIm;
    high_in = handles.maxIm;
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) 0.5+(handles.valueLevelSoft*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT = [LUT1 LUT2-min(LUT2)+max(LUT1)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = sigmaTransform(handles.image, handles.valueCurveDense, handles.valueLevelDense, handles.minIm, handles.maxIm, handles.minOriginal, 0);
    set(handles.level_dense, 'Value', handles.valueLevelDense)
    set(handles.curve_dense, 'Value', handles.valueCurveDense)
    set(handles.text_curve_dense, 'String', 'Slope')
elseif handles.algDense == 2
    gamDense = (handles.valueCurveGammaDense * 4) + 1;
    low_in = round(handles.minIm + handles.valueLevelGammaDense*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);

    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamDense)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTdense = LUT/max(LUT);
    handles.denseImage = gamma_Correction (handles.image, gamDense, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_dense, 'Value', handles.valueLevelGammaDense)
    set(handles.curve_dense, 'Value', handles.valueCurveGammaDense)
    set(handles.text_curve_dense, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealDense * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealDense*(high_in-low_in)+low_in;
    level = ((handles.valueLevelLinealDense/2)+0.5)*(high_in-low_in)+low_in;
    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = linealBySections(handles.image, handles.maxIm, handles.valueWindowLinealDense, handles.valueLevelLinealDense, handles.minOriginal, 1);
    set(handles.level_dense, 'Value', handles.valueLevelLinealDense)
    set(handles.curve_dense, 'Value', handles.valueWindowLinealDense)
    set(handles.text_curve_dense, 'String', 'Window')
end
handles.LUTvisualDense = LUTdense - min(LUTdense);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsDense,histDense] = imhist(handles.denseImage,2^16);
binsDense = handles.maxIm*(binsDense/max(binsDense(:)));

% Max an min of each image
minDense = min(handles.denseImage(:));
maxDense = max(handles.denseImage(:));

% DENSE IMAGE/LUT VISUALIZATION
axes(handles.axes1);
% Dense Image Histogram
bar(histDense(minDense+2:maxDense),binsDense(minDense+2:maxDense), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightDense = max(binsDense(minDense+2:maxDense));
minGreyDense = minDense+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on

%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreyDense,minGreyOr));
endLUT = length(handles.LUTvisualDense);
maxLUT = min(maxHeightOr,maxHeightDense);
minLUT = handles.LUTvisualDense(iniLUT);
LUTvisual = handles.LUTvisualDense(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Dense Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Dense Image
hold off
axes(handles.axes2);
%[bins,histImage] = imhist(handles.denseImage,2^16);
[minIm, maxIm] = imageRange(handles.denseImage);
imshow(imresize(handles.denseImage,0.25),[minIm maxIm])

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Clear processed image
axes(handles.axes5)
cla

% Update handles structure
guidata(hObject, handles);

% --- Executes on slider movement.
function curve_dense_Callback(hObject, eventdata, handles)
% hObject    handle to curve_dense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%Take the algorithm from the menu and its parameters 
handles.algDense = get(handles.popupmenu_dense,'Value');

if handles.algDense == 1
    handles.valueCurveDense = get(handles.curve_dense, 'Value');
elseif handles.algDense == 2
    handles.valueCurveGammaDense = get(handles.curve_dense, 'Value');
else
    handles.valueWindowLinealDense = get(handles.curve_dense, 'Value');
end

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

if handles.algDense == 1
    low_in = handles.minIm;
    high_in = handles.maxIm;
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) 0.5+(handles.valueLevelSoft*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT = [LUT1 LUT2-min(LUT2)+max(LUT1)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = sigmaTransform(handles.image, handles.valueCurveDense, handles.valueLevelDense, handles.minIm, handles.maxIm, handles.minOriginal, 0);
    set(handles.level_dense, 'Value', handles.valueLevelDense)
    set(handles.curve_dense, 'Value', handles.valueCurveDense)
    set(handles.text_curve_dense, 'String', 'Slope')
elseif handles.algDense == 2
    gamDense = (handles.valueCurveGammaDense * 4) + 1;
    low_in = round(handles.minIm + handles.valueLevelGammaDense*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);

    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamDense)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTdense = LUT/max(LUT);
    handles.denseImage = gamma_Correction (handles.image, gamDense, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_dense, 'Value', handles.valueLevelGammaDense)
    set(handles.curve_dense, 'Value', handles.valueCurveGammaDense)
    set(handles.text_curve_dense, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealDense * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealDense*(high_in-low_in)+low_in;
    level = ((handles.valueLevelLinealDense/2)+0.5)*(high_in-low_in)+low_in;
    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = linealBySections(handles.image, handles.maxIm, handles.valueWindowLinealDense, handles.valueLevelLinealDense, handles.minOriginal, 1);
    set(handles.level_dense, 'Value', handles.valueLevelLinealDense)
    set(handles.curve_dense, 'Value', handles.valueWindowLinealDense)
    set(handles.text_curve_dense, 'String', 'Window')
end
handles.LUTvisualDense = LUTdense - min(LUTdense);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsDense,histDense] = imhist(handles.denseImage,2^16);
binsDense = handles.maxIm*(binsDense/max(binsDense(:)));

% Max an min of each image
minDense = min(handles.denseImage(:));
maxDense = max(handles.denseImage(:));

% DENSE IMAGE/LUT VISUALIZATION
axes(handles.axes1);
% Dense Image Histogram
bar(histDense(minDense+2:maxDense),binsDense(minDense+2:maxDense), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightDense = max(binsDense(minDense+2:maxDense));
minGreyDense = minDense+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on

%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreyDense,minGreyOr));
endLUT = length(handles.LUTvisualDense);
maxLUT = min(maxHeightOr,maxHeightDense);
minLUT = handles.LUTvisualDense(iniLUT);
LUTvisual = handles.LUTvisualDense(iniLUT:endLUT)-minLUT;
LUTvisual = LUTvisual/max(LUTvisual);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Dense Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Dense Image
hold off
axes(handles.axes2);
%[bins,histImage] = imhist(handles.denseImage,2^16);
[minIm, maxIm] = imageRange(handles.denseImage);
imshow(imresize(handles.denseImage,0.25),[minIm maxIm])

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Clear processed image
axes(handles.axes5)
cla

% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in popupmenu_dense.
function popupmenu_dense_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_dense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_dense contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_dense

%Take the algorithm from the menu 
handles.algDense = get(handles.popupmenu_dense,'Value');

% Update the LUTs
inRange = handles.minIm:handles.maxIm;
handles.inRange = (inRange-handles.minIm)/(handles.maxIm-handles.minIm);

if handles.algDense == 1
    low_in = handles.minIm;
    high_in = handles.maxIm;
    
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_in = double(low_in-lowOriginal);
    
    % Create the SIGMA LUT
    LUT1 = (0:low_in);
    inRange = (0:high_in)/high_in;
    LUTsigma = sigmf(inRange,[1+(handles.valueCurveSoft*9) 0.5+(handles.valueLevelSoft*0.5)])*(high_in-low_in);
    LUT2 = LUTsigma(low_in+1:length(LUTsigma));
    LUT = [LUT1 LUT2-min(LUT2)+max(LUT1)];
    LUTnorm = LUT/max(LUT);
    LUT = LUTnorm*high_in;
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = sigmaTransform(handles.image, handles.valueCurveDense, handles.valueLevelDense, handles.minIm, handles.maxIm, handles.minOriginal, 0);
    set(handles.level_dense, 'Value', handles.valueLevelDense)
    set(handles.curve_dense, 'Value', handles.valueCurveDense)
    set(handles.text_curve_dense, 'String', 'Slope')
elseif handles.algDense == 2 
    gamDense = (handles.valueCurveGammaDense * 4) + 1;
    low_in = round(handles.minIm + handles.valueLevelGammaDense*(handles.maxIm - handles.minIm));
    high_in = handles.maxIm;
    
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(high_in-lowOriginal);
    low_inLUT = double(low_in-lowOriginal);

    % Look Up Table
    LUT1 = (0:low_inLUT);
    inRange = (0:high_in)/(high_in);
    LUT2 = (inRange.^gamDense)*(high_in);
    LUT_rec = LUT2(low_inLUT+1:end);
    LUT_rect = LUT_rec - min(LUT_rec);
    LUT_rectNorm = LUT_rect/max(LUT_rect);
    LUT3 = LUT_rectNorm*(high_in-max(LUT1));
    LUTini = 0:lowOriginal;
    LUTini(1:end) = 0;
    LUT = [LUTini LUT1 LUT3+max(LUT1)];
    LUTdense = LUT/max(LUT);
    handles.denseImage = gamma_Correction (handles.image, gamDense, low_in, handles.maxIm, handles.minOriginal);
    set(handles.level_dense, 'Value', handles.valueLevelGammaDense)
    set(handles.curve_dense, 'Value', handles.valueCurveGammaDense)
    set(handles.text_curve_dense, 'String', 'Curve')
else
    % Image information
    lowOriginal = double(handles.minOriginal);
    high_in = double(handles.maxIm-lowOriginal);
    low_in = handles.minIm - lowOriginal;
  
    windSize = handles.valueWindowLinealDense * (0.4*high_in) + 0.1*high_in; % window size from 10% to 90% of the maximum grey value
%     level = handles.valueLevelLinealDense*(high_in-low_in)+low_in;
    level = ((handles.valueLevelLinealDense/2)+0.5)*(high_in-low_in)+low_in;
    ind1 = round(level - 0.5*windSize);
    ind2 = round(level + 0.5*windSize);
    LUT1 = (0:ind1);
    LUT2 = handles.pte*(ind1+1:ind2);
    if isempty (LUT1)
        maxLUT1 = 0;
    else
        maxLUT1 = max(LUT1);
    end
    diff = min(LUT2) - maxLUT1;
    LUT2 = LUT2 - diff;
    LUT = [LUT1 LUT2];
    LUT3 = ind2+1:high_in;
    diff2 = max(LUT) - min(LUT3);
    LUT3 = LUT3 + diff2;
    LUT = [LUT LUT3];
    LUTzeros = 0:lowOriginal;
    LUTzeros(1:end) = 0;
    LUT = [LUTzeros LUT];
    LUTdense = LUT/max(LUT);
    handles.denseImage = linealBySections(handles.image, handles.maxIm, handles.valueWindowLinealDense, handles.valueLevelLinealDense, handles.minOriginal, 1);
    set(handles.level_dense, 'Value', handles.valueLevelLinealDense)
    set(handles.curve_dense, 'Value', handles.valueWindowLinealDense)
    set(handles.text_curve_dense, 'String', 'Window')
end

handles.LUTvisualDense = LUTdense - min(LUTdense);

%--------------------------------------------------------------------------
% HISTOGRAMS
%--------------------------------------------------------------------------
% Show differences in histograms after applying the sigma/gamma function
[binsDense,histDense] = imhist(handles.denseImage,2^16);
binsDense = handles.maxIm*(binsDense/max(binsDense(:)));

% Max an min of each image
minDense = min(handles.denseImage(:));
maxDense = max(handles.denseImage(:));

% DENSE IMAGE/LUT VISUALIZATION
axes(handles.axes1);
% Dense Image Histogram
bar(histDense(minDense+2:maxDense),binsDense(minDense+2:maxDense), 'facecolor',[0.1 0.8 1], 'EdgeColor', [0.1 0.8 1])
maxHeightDense = max(binsDense(minDense+2:maxDense));
minGreyDense = minDense+2;
hold on
% Original Image Histogram
bar(handles.histImageOriginal(handles.minOriginal+2:handles.maxIm),handles.binsOriginal(handles.minOriginal+2:handles.maxIm),'b','EdgeColor', 'b')
maxHeightOr = max(handles.binsOriginal(handles.minOriginal+2:handles.maxIm));
minGreyOr = handles.minIm+2;
hold on

%LUT (hights and widths adapted according to the histograms)
iniLUT = double(min(minGreyDense,minGreyOr));
endLUT = length(handles.LUTvisualDense);
maxLUT = min(maxHeightOr,maxHeightDense);
% minLUT = handles.LUTvisualDense(iniLUT);
LUTvisual = LUTdense(iniLUT:endLUT);
plot(iniLUT:endLUT, (LUTvisual)*maxLUT,'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
% Information
legend ('Dense Histogram', 'Original Histogram', 'Selected Function')
set(gca, 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Dense Image
hold off
axes(handles.axes2);
%[bins,histImage] = imhist(handles.denseImage,2^16);
[minIm, maxIm] = imageRange(handles.denseImage);
imshow(imresize(handles.denseImage,0.25),[minIm maxIm])

% Clear processed image
axes(handles.axes5)
cla

% Update interface
set(handles.factor_soft, 'Enable', 'off', 'Visible', 'on')
set(handles.factor_dense, 'Enable', 'off', 'Visible', 'on')
set(handles.pushbutton_ok, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])
set(handles.pushbutton_amplifyEq, 'Enable', 'off', 'BackgroundColor', [0.941 0.941 0.941], 'ForegroundColor', [0 0 0])

% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function factor_soft_Callback(hObject, eventdata, handles)
% hObject    handle to factor_soft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Get the factor for the soft image
handles.factorSoft = get(handles.factor_soft, 'Value');

if (handles.factorDense == 0)&&(handles.factorSoft == 0)
    uiwait(msgbox('One of the factors should be greater than 0', 'Info'))
    handles.factorSoft = 0.1;
    set(hObject,'Value',0.1)
end

% Update visualization with the new factor
updatehandles(handles)

% Update handles structure
guidata(hObject, handles);

% --- Executes on slider movement.
function factor_dense_Callback(hObject, eventdata, handles)
% hObject    handle to factor_dense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Get the factor for the dense image
handles.factorDense = get(handles.factor_dense, 'Value');

if (handles.factorDense == 0)&&(handles.factorSoft == 0)
    uiwait(msgbox('One of the factors should be greater than 0', 'Info'))
    handles.factorDense = 0.1;
    set(hObject,'Value',0.1)
end

% Update visualization with the new factor
updatehandles(handles)

% Update handles structure
guidata(hObject, handles);


% Function to update the visualization with the new information
function updatehandles(handles)

% Take the soft/dense algorithms
handles.algSoft = get(handles.popupmenu_soft,'Value');
handles.algDense = get(handles.popupmenu_dense,'Value');

%--------------------------------------------------------------------------
% EQUALIZED IMAGE AND HISTOGRAMS
%--------------------------------------------------------------------------
% Equalized image.
sImage = handles.softImage - min(handles.softImage(:));
dImage = handles.denseImage - min(handles.denseImage(:));
equalizedImage = (handles.factorSoft*sImage + handles.factorDense*dImage);
diffIm = max(handles.image(:))-max(handles.equalizedImage(:));
handles.equalizedImage = equalizedImage + diffIm;

% Show differences in histograms after applying the sigma/gamma function
[handles.binsOriginal,handles.histImageOriginal] = imhist(handles.image,2^16);
handles.binsOriginal = handles.maxIm*(handles.binsOriginal/max(handles.binsOriginal(:)));

%--------------------------------------------------------------------------
% SHOW IMAGES, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------
% Final image
if (handles.factorSoft ~= -1) && (handles.factorDense ~= -1)
    axes(handles.axes5)
    %[bins,histImage] = imhist(handles.equalizedImage,2^16);
    [minIm, maxIm] = imageRange(handles.equalizedImage);
    imshow(handles.equalizedImage,[minIm maxIm])
end

% Update handles structure
guidata(handles.figure1, handles);


% --- Executes on button press in pushbutton_ok.
function pushbutton_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata (0,'valueCurveSoft',handles.valueCurveSoft)
setappdata (0,'valueCurveDense',handles.valueCurveDense)
setappdata (0,'valueLevelSoft',handles.valueLevelSoft)
setappdata (0,'valueLevelDense',handles.valueLevelDense)
setappdata (0,'valueCurveGammaSoft',handles.valueCurveGammaSoft)
setappdata (0,'valueCurveGammaDense',handles.valueCurveGammaDense)
setappdata (0,'valueLevelGammaSoft',handles.valueLevelGammaSoft)
setappdata (0,'valueLevelGammaDense',handles.valueLevelGammaDense)
setappdata (0,'valueLevelLinealDense',handles.valueLevelLinealDense)
setappdata (0,'valueLevelLinealSoft',handles.valueLevelLinealSoft)
setappdata (0,'valueWindowLinealSoft',handles.valueWindowLinealSoft)
setappdata (0,'valueWindowLinealDense',handles.valueWindowLinealDense)
setappdata (0,'valueFactorSoft',handles.factorSoft)
setappdata (0,'valueFactorDense',handles.factorDense)
setappdata (0,'algSoft',handles.algSoft)
setappdata (0,'algDense',handles.algDense)
setappdata (0,'eqIm', 1)

close


% --- Executes on button press in pushbutton_optimize.
function pushbutton_optimize_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_optimize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% In order to fix our attention only on the region of interest, the
% user has to select it and we will only maximize the entropy in this
% local zone instead of doing it in a global way.
figure;
imshow(handles.image,[])
title ('Please, select a mixture of soft and dense tissue')
try
    rect = getrect(gcf);
catch me
    return
end

% Correct the ROI in case the user has clicked outside the box that
% contains the image.
if rect(1) <= 0
    rect(1) = 1;
end

if rect(2) <= 0
    rect(2) = 1;
end

if rect(1)+rect(3) > size(handles.image,2)
    rect(3) = size(handles.image,2)-rect(1);
end

if rect(2)+rect(4) > size(handles.image,1)
    rect(4) = size(handles.image,1)-rect(2);
end

% Close the figure once the ROI has been selected
close 

% Inform we are optimizing the factors according to the entropy
set(handles.pushbutton_optimize, 'String', 'Optimizing', 'BackgroundColor', 'r', 'ForegroundColor', [0 0 0])
pause(0.2)

rangeAlpha = [0 1];
toleranceAlpha = (rangeAlpha(end)-rangeAlpha(1))/10;
ro = (3-sqrt(5))/2;
eq1 = handles.softImage-min(handles.softImage(:));
eq2 = handles.denseImage-min(handles.denseImage(:));

% First limit. Find the optimum image for this alpha
alpha1 = rangeAlpha(1) + ro*(rangeAlpha(end)-rangeAlpha(1));
eqImage1 = alpha1*eq1+(1-alpha1)*eq2;
maxIm1 = max(eqImage1(:));
eqImageN1 = double(eqImage1)/double(maxIm1);
entropy1 = entropy(eqImageN1(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3)));
    
% Second limit. Find the optimum image for this alpha
alpha2 = rangeAlpha(1) + (1-ro)*(rangeAlpha(end)-rangeAlpha(1));
eqImage2 = alpha2*eq1+(1-alpha2)*eq2;
maxIm2 = max(eqImage2(:));
eqImageN2 = double(eqImage2)/double(maxIm2);
entropy2 = entropy(eqImageN2(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3))); 
    
% ITERATIVE STAGE
while (rangeAlpha(2)-rangeAlpha(1)) > toleranceAlpha
     if entropy2 > entropy1

        % Narrow the range
        rangeAlpha = [alpha1 rangeAlpha(end)];

        % The second limit becomes the first limit
        alpha1 = alpha2;
        eqImageN1 = eqImageN2;

        % Calculate again the second limit
        alpha2 = rangeAlpha(1) + (1-ro)*(rangeAlpha(end)-rangeAlpha(1));
        eqImage2 = alpha2*eq1+(1-alpha2)*eq2;
        maxIm2 = max(eqImage2(:));
        eqImageN2 = double(eqImage2)/double(maxIm2);
        entropy2 = entropy(eqImageN2(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3)));

        % Update the final variables according to the maximum entropy
        if entropy2 > entropy1
            factorSoft = alpha2;
            factorDense = 1-factorSoft;
        else
            factorSoft = alpha1;
            factorDense = 1-factorSoft;
        end
     else

        % Narrow the range
        rangeAlpha = [rangeAlpha(1) alpha2];

        % The first limit becomes the second limit
        alpha2 = alpha1;
        eqImageN2 = eqImageN1;

        % Calculate again the first limit
        alpha1 = rangeAlpha(1) + ro*(rangeAlpha(end)-rangeAlpha(1));
        eqImage1 = alpha1*eq1+(1-alpha1)*eq2;
        maxIm1 = max(eqImage1(:));
        eqImageN1 = double(eqImage1)/double(maxIm1);
        entropy1 = entropy(eqImageN1(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3)));

        % Update the final variables according to the maximum entropy
        if entropy1 > entropy2
            factorSoft = alpha1;
            factorDense = 1-factorSoft;
        else
            factorSoft = alpha2;
            factorDense = 1-factorSoft;
        end
     end
end

% Update interface elements
handles.factorSoft = factorSoft;
handles.factorDense = factorDense;
set(handles.factor_soft, 'Visible', 'on','Enable', 'on', 'Value', handles.factorSoft)
set(handles.factor_dense, 'Visible', 'on','Enable', 'on', 'Value', handles.factorDense)
set(handles.pushbutton_optimize, 'String', 'Optimize factors', 'BackgroundColor', [0.729 0.831 0.957], 'ForegroundColor', [0.204 0.302 0.494])
set(handles.pushbutton_ok, 'Enable', 'on', 'BackgroundColor', [0.729 0.831 0.957], 'ForegroundColor', [0.204 0.302 0.494])
set(handles.pushbutton_amplifyEq, 'Enable', 'on', 'BackgroundColor', [0.729 0.831 0.957], 'ForegroundColor', [0.204 0.302 0.494])

% Update processed image visualization
updatehandles(handles)

% Update handles structure
guidata(handles.figure1, handles);
     

% --- Executes on button press in pushbutton_amplifyEq.
function pushbutton_amplifyEq_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_amplifyEq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'image',getimage(handles.axes5))
zoomImage

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_amplifySoft.
function pushbutton_amplifySoft_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_amplifySoft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'image',getimage(handles.axes4))
zoomImage

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_amplifyDense.
function pushbutton_amplifyDense_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_amplifyDense (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'image',getimage(handles.axes2))
zoomImage

% Update handles structure
guidata(handles.figure1,handles);
