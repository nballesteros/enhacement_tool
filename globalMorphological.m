% Auxiliar function to apply an enhancement based on morphological
% processing
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be enhanced
% enhancement_factor: to control the level of enhancement
% size_ini: initial size of the structural element
% size_final: final size of the structural element
% se: type of structural element ('disk')
%...................................................................................................................
% OUTPUT PARAMETERS
% enhancedImage: resulting image from the algorithm
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function enhancedImage = globalMorphological (image, enhancement_factor, size_ini, size_final, se)

image = uint16(image);

if enhancement_factor == 0
    enhancedImage =image; % If the user has selected a factor of 0, we don't have to apply any processing
else
    % Variables initialization
    [rows,columns] = size(image);
    num_images = size_final - size_ini + 1;
    bankTopHat = uint16(zeros(rows,columns,num_images));
    bankBotHat = uint16(zeros(rows,columns,num_images));

    %---------- TOPHAT MORPHOLOGICAL PROCESSING ----------------------------%

    % We calculate the bright details of different sizes (the bank is composed
    % by the elements of the images that are smaller than the s.e. but bigger
    % than the s.e. of the previous step).
        
    % First, we begin with a lower size of the s.e. in order to subtract
    % the result of performing a tophat with it and with the minimum s.e.
    % (Thanks to this, we will obtain the details with a size that is exactly
    % the minimum one)
    
    if size_ini ~= 1
        sizeTopHat = size_ini - 1;
        imageAux = tophat(image,sizeTopHat,se);
        sizeTopHat = sizeTopHat + 1;
    
        for i = 1:num_images
            if i == 1
                bankTopHat(:,:,i) = tophat(image,sizeTopHat,se) - imageAux; % If the size_ini is not 1, we need the imageAux to perform the substraction
            else
                bankTopHat(:,:,i) = tophat(image,sizeTopHat,se) - bankTopHat(:,:,i-1);
            end
            sizeTopHat = sizeTopHat + 1;
        end
        
    else
        sizeTopHat = size_ini;
        for i = 1:num_images
            if i == 1
                bankTopHat(:,:,i) = tophat(image,sizeTopHat,se); %In case we want to begin with 1, the first image will be the tophat image itself
            else
                bankTopHat(:,:,i) = tophat(image,sizeTopHat,se) - bankTopHat(:,:,i-1);
            end
            sizeTopHat = sizeTopHat + 1;
        end
    end
   

%---------- BOTTOMHAT MORPHOLOGICAL PROCESSING -------------------------%

    % We calculate the dark details of different sizes (the bank is composed
    % by the elements of the images that are smaller than the s.e. but bigger
    % than the s.e. of the previous step).
    
    % First, we begin with a lower size of the s.e. in order to subtract
    % the result of performing a bottomhat with this and with the minimum s.e.
    % (Thanks to it, we will obtain the details with a size that is exactly
    % the minimum one)
    
    if size_ini ~= 1
        sizeBotHat = size_ini - 1;
        imageAux2 = bottomhat(image,sizeTopHat,se);
        sizeBotHat = sizeBotHat + 1;
    
        for i = 1:num_images
            if i == 1
                bankBotHat(:,:,i) = bottomhat(image,sizeBotHat,se) - imageAux2; % If the size_ini is not 1, we need the imageAux to perform the substraction
            else
                bankBotHat(:,:,i) = bottomhat(image,sizeBotHat,se) - bankBotHat(:,:,i-1);
            end
            sizeBotHat = sizeBotHat + 1;
        end
        
    else
        sizeBotHat = size_ini;
        for i = 1:num_images
            if i == 1
                bankBotHat(:,:,i) = bottomhat(image,sizeBotHat,se); %In case we want to begin with 1, the first image will be the tophat image itself
            else
                bankBotHat(:,:,i) = bottomhat(image,sizeBotHat,se) - bankBotHat(:,:,i-1);
            end
            sizeBotHat = sizeBotHat + 1;
        end
    end

%-------------------------- ENHANCED IMAGE -----------------------------%
    
    % We give more weight to the smaller details and we decrease the value
    % for the greater ones.
    brightDetails = uint16(zeros(rows,columns));
    darkDetails = uint16(zeros(rows,columns));
    
    for i = 1:num_images
        brightDetails = (enhancement_factor/i) * bankTopHat (:,:,i) + brightDetails;
        darkDetails = (enhancement_factor/i)* bankBotHat (:,:,i) + darkDetails;
    end

    enhancedImage = image + brightDetails - darkDetails;
end




