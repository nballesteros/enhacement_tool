% Histogram equalization algorithm with bins clipping
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be equalized
% clip: parameter that controls the contrast
% low_in: minimum grey value of the range where we are going to apply the equalization
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: contrast improved image
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = clipHistEq(image, clip, low_value)

if clip == 0
    equalizedImage = image;
else
    % Image information
    low_in = double(low_value);
    high_in = double(max(image(:)));
    equalizedImage = imEqualization(image, clip, low_in, high_in);
end



