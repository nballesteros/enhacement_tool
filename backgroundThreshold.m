function varargout = backgroundThreshold(varargin)
% BACKGROUNDTHRESHOLD MATLAB code for backgroundThreshold.fig
%      BACKGROUNDTHRESHOLD, by itself, creates a new BACKGROUNDTHRESHOLD or raises the existing
%      singleton*.
%
%      H = BACKGROUNDTHRESHOLD returns the handle to a new BACKGROUNDTHRESHOLD or the handle to
%      the existing singleton*.
%
%      BACKGROUNDTHRESHOLD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BACKGROUNDTHRESHOLD.M with the given input arguments.
%
%      BACKGROUNDTHRESHOLD('Property','Value',...) creates a new BACKGROUNDTHRESHOLD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before backgroundThreshold_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to backgroundThreshold_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help backgroundThreshold

% Last Modified by GUIDE v2.5 14-Sep-2016 11:29:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @backgroundThreshold_OpeningFcn, ...
                   'gui_OutputFcn',  @backgroundThreshold_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before backgroundThreshold is made visible.
function backgroundThreshold_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to backgroundThreshold (see VARARGIN)

% Choose default command line output for backgroundThreshold
handles.output = hObject;

% We take the image, the current min grey level, the type of contrast
% improvement algorithm and the percentage of contrast improvement
handles.image = getappdata(0,'image');
handles.minRelative = getappdata (0,'minHis');
handles.type = getappdata(0,'type');
handles.sliderContrast = getappdata (0,'slider');

% The absolute max and min values comes from the initial image
handles.minGrey = double(min(handles.image(:)));
handles.maxGrey = double(max(handles.image(:)));

% Set data on the interface
set(handles.minGL,'String',num2str(handles.minRelative))
set(handles.slider_min, 'Value', (handles.minRelative-handles.minGrey)/(handles.maxGrey-handles.minGrey)); % min grey value
set(handles.slider_value, 'Value', handles.sliderContrast); % percentage of contrast

%---------------------------------------------------------------------------------------------
% GAMMA CORRECTION
%---------------------------------------------------------------------------------------------
if handles.type == 3
    
    % The bar means something different if it is below or above the center
    set(handles.text5, 'String', 'Soft       <-----     TISSUE      ----->        Dense') 
    if handles.sliderContrast >= 0.5 
        percGamma = (handles.sliderContrast-0.5)*2*100;
        gammaData = (handles.sliderContrast-0.5) * 8 + 1;
        if handles.sliderContrast > 0.5 % Dense tissues better visualization
            set(handles.text_percentage, 'String', [num2str(round(percGamma)) '% Dense']);
        else % Linear LUT (no contrast modification)
            set(handles.text_percentage, 'String', 'No contrast modification');
        end
    else % Soft tissues better visualization
        percGamma = (1-handles.sliderContrast*2)*100;
        gammaData = (handles.sliderContrast*8)/5 + 1/5;
        set(handles.text_percentage, 'String', [num2str(round(percGamma)) '% Soft']);
    end
    
        
    % Processed image
    equalizedImage = gamma_Correction (handles.image, gammaData, handles.minRelative, handles.maxGrey, handles.minGrey);  
    
%---------------------------------------------------------------------------------------------
% GENERAL EQUALIZATION ALGORITHM
%---------------------------------------------------------------------------------------------
    
else
    set(handles.text5, 'String', '-              IMAGE�S CONTRAST              +') 
    percContrast = (handles.sliderContrast)*100;
    set(handles.text_percentage, 'String', [num2str(round(percContrast)) ' %']);
    
    % The percentage of contrast is defined by clipping the bins of the
    % histogram
    clip = handles.sliderContrast;
    equalizedImage = clipHistEq(handles.image, clip, handles.minRelative);
    
end

%---------------------------------------------------------------------------------------------
% LUT
%---------------------------------------------------------------------------------------------
inRange = handles.minGrey:handles.maxGrey;
%Normalization
LUT = (inRange-handles.minGrey)/(handles.maxGrey-handles.minGrey);
% Limit of the LUT
ind1 = find(LUT==(handles.minRelative-handles.minGrey)/(handles.maxGrey-handles.minGrey));
if ind1 ~= 1
    LUT(1,ind1:end) = 0.5;
    % Heights correcction
    ratio = 0.5/LUT(1,ind1-1);
    LUT(1,1:ind1-1) = LUT(1,1:ind1-1)  * ratio;
else
    LUT(1,ind1:end) = 0.5;
end

%--------------------------------------------------------------------------
% SHOW IMAGE, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------

% Image histogram (we impose 2^15 bins instead of 2^16 because too much
% resolution provokes some holes in the histogram that can confuse us
[bins,histImage] = imhist(equalizedImage,2^16);

% Max of the histogram (avoiding 0) to scale the LUT
maxHist = max(bins(2:end));

% We show the histogram
axes(handles.axes1);
if handles.minGrey > 2
    bar(histImage(handles.minGrey:handles.maxGrey),bins(handles.minGrey:handles.maxGrey),'b')
else
    bar(histImage(2:handles.maxGrey),bins(2:handles.maxGrey),'b')
end

%Plot the LUT
hold on
plot (handles.minGrey:handles.maxGrey,LUT*maxHist*0.9, 'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
set(gca, 'xcolor', 'b', 'ycolor', 'b', 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Show the correct image (preview)
axes(handles.axes3)
colorBackground (handles,equalizedImage)

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = backgroundThreshold_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function minGL_Callback(hObject, eventdata, handles)
% hObject    handle to minGL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minGL as text
%        str2double(get(hObject,'String')) returns contents of minGL as a double

% Update the values when we modify the edit 'min grey value'
minRelative = get(hObject,'String');

% Check if the user has selected a valid grey value
if isempty(minRelative)
    uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'))
    set(hObject,'String','0')
    handles.minRelative = 0;
elseif ~isempty (strfind(minRelative, ','));
    uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'))
    set(hObject,'String','0')
    handles.minRelative = 0;
else
     minRel = str2double(minRelative);
     if isnan(minRel)
        uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
        set(hObject,'String','0')
        handles.minRelative = 0;
     else
        integerTest=~mod(minRel,1);
        if isempty (minRel)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
            set(hObject,'String','0')
            handles.minRelative = 0;
        elseif (minRel < 0) || (integerTest == 0)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value)', 'Info'));
            set(hObject,'String','0')
            handles.minRelative = 0;
        elseif (minRel >= handles.maxGrey)
            uiwait(msgbox('Please, select a valid threshold (integer minimum gray value). It cannot be greater than the maximum histogram value', 'Info'));
            set(hObject,'String','0')
            handles.minRelative = 0;
        else
            handles.minRelative = minRel;
        end
     end
end

set (handles.slider_min, 'Value', (handles.minRelative/handles.maxGrey))

% Update the visualization with the new threshold
updatehandles(handles)

% Update handles structure
guidata(handles.figure1,handles)


% --- Executes on slider movement.
function slider_min_Callback(hObject, eventdata, handles)
% hObject    handle to slider_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Update the values when we modify the slider 'min grey value'
minBar = get(handles.slider_min,'Value');
handles.minRelative = round(handles.minGrey + minBar * (handles.maxGrey-handles.minGrey));
if handles.minRelative < handles.minGrey
    handles.minRelative = handles.minGrey;
end

set (handles.minGL, 'String', num2str(handles.minRelative))

% Update the visualization with the new threshold
updatehandles (handles)

% Update handles structure
guidata(handles.figure1,handles)

% --- Executes on slider movement.
function slider_value_Callback(hObject, eventdata, handles)
% hObject    handle to slider_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Percentage of contrast
handles.sliderContrast = get(handles.slider_value, 'Value');

% Different meanings depending on the type of contrast algorithm
if handles.type == 3 % GAMMA CORRECTION
    if handles.sliderContrast >= 0.5 % DENSE TISSUES
            percGamma = (handles.sliderContrast-0.5)*2*100;
        if handles.sliderContrast > 0.5
            set(handles.text_percentage, 'String', [num2str(round(percGamma)) '% Dense']);
        else
            set(handles.text_percentage, 'String', 'No contrast modification');
        end
    else % SOFT TISSUES
        percGamma = (1-handles.sliderContrast*2)*100;
        set(handles.text_percentage, 'String', [num2str(round(percGamma)) '% Soft']);
    end
    
else % EQUALIZATION ALGORITHMS
    percContrast = (handles.sliderContrast)*100;
    set(handles.text_percentage, 'String', [num2str(round(percContrast)) ' %']);
end

% Update the visualization with the new threshold
updatehandles (handles)

% Update handles structure
guidata(handles.figure1,handles)

% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% We send the new min grey value and the percentage of contrast to the main window
setappdata (0,'min',handles.minRelative)
setappdata (0, 'slider', handles.sliderContrast)

close

function updatehandles (handles)

% Different meanings depending on the type of contrast algorithm

%---------------------------------------------------------------------------------------------
% GAMMA CORRECTION
%---------------------------------------------------------------------------------------------

if handles.type == 3 % GAMMA CORRECTION
    if handles.sliderContrast >= 0.5 % DENSE
        gammaData = (handles.sliderContrast-0.5) * 8 + 1;
    else % SOFT
        gammaData = (handles.sliderContrast*8)/5 + 1/5;
    end
    
    equalizedImage = gamma_Correction (handles.image, gammaData, handles.minRelative, handles.maxGrey, handles.minGrey);

%---------------------------------------------------------------------------------------------
% GENERAL EQUALIZATION ALGORITHM
%---------------------------------------------------------------------------------------------

else % EQUALIZATION ALGORITHMS
    
    % Percentage of contrast is definded by the clipping factor
    clip = handles.sliderContrast;    
    equalizedImage = clipHistEq(handles.image, clip, handles.minRelative);
end

%---------------------------------------------------------------------------------------------
% LUT
%---------------------------------------------------------------------------------------------
inRange = handles.minGrey:handles.maxGrey;
%Normalization
LUT = (inRange-handles.minGrey)/(handles.maxGrey-handles.minGrey);
% Limit of the LUT
ind1 = find(LUT==(handles.minRelative-handles.minGrey)/(handles.maxGrey-handles.minGrey));
if ind1 ~= 1
    LUT(1,ind1:end) = 0.5;
    % Heights correcction
    ratio = 0.5/LUT(1,ind1-1);
    LUT(1,1:ind1-1) = LUT(1,1:ind1-1)  * ratio;
else
    LUT(1,ind1:end) = 0.5;
end



%--------------------------------------------------------------------------
% SHOW IMAGE, HISTOGRAMS AND LUT
%--------------------------------------------------------------------------
cla (handles.axes1);

% Image histogram (we impose 2^15 bins instead of 2^16 because too much
% resolution provokes some holes in the histogram that can confuse us
[bins,histImage] = imhist(equalizedImage,2^16);

% Max of the histogram (avoiding 0) to scale the LUT
maxHist = max(bins(2:end));

% Plot the histogram
axes(handles.axes1);
if handles.minGrey > 2
    bar(histImage(handles.minGrey:handles.maxGrey),bins(handles.minGrey:handles.maxGrey),'b')
else
    bar(histImage(2:handles.maxGrey),bins(2:handles.maxGrey),'b')
end

%Plot the LUT
hold on
plot (handles.minGrey:handles.maxGrey,LUT*maxHist*0.9, 'Color', [0.7 0.8 0], 'linewidth', 1.5, 'LineStyle', '--')
set(gca, 'xcolor', 'b', 'ycolor', 'b', 'Color', [0.871 0.922 0.98], 'TickDir', 'out')
hold off

% Preview image
axes(handles.axes3)
colorBackground (handles,equalizedImage)

% Update handles
guidata(handles.figure1,handles)


function colorBackground (handles, equalizedImage)

% Reduce the size of the images to speed up the code
imSmall = imresize(handles.image,0.25);
eqSmall = imresize(equalizedImage,0.25);

% Create a image with all pixels in red
redChannel = ones(size(imSmall))*0.1;
greenChannel = ones(size(imSmall))*0.3;
blueChannel = ones(size(imSmall));
coloredImage = cat(3, redChannel, greenChannel, blueChannel);

% Create a mask with transparency in the pixels where the image is below
% the threshold
mask = true(size(imSmall));
axes(handles.axes3)
mask(imSmall<handles.minRelative)=false;

% Show both images and the mask
imshow(coloredImage,[])
hold on;
%[bins,histImage] = imhist(eqSmall,2^16);
[minIm, maxIm] = imageRange(eqSmall);
h = imshow(eqSmall,[minIm maxIm]);
set(h,'AlphaData',mask);
hold off

% Update handles
guidata(handles.figure1,handles)
