function varargout = saveImages(varargin)
% SAVEIMAGES MATLAB code for saveImages.fig
%      SAVEIMAGES, by itself, creates a new SAVEIMAGES or raises the existing
%      singleton*.
%
%      H = SAVEIMAGES returns the handle to a new SAVEIMAGES or the handle to
%      the existing singleton*.
%
%      SAVEIMAGES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SAVEIMAGES.M with the given input arguments.
%
%      SAVEIMAGES('Property','Value',...) creates a new SAVEIMAGES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before saveImages_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to saveImages_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help saveImages

% Last Modified by GUIDE v2.5 10-Jan-2017 10:44:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @saveImages_OpeningFcn, ...
                   'gui_OutputFcn',  @saveImages_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before saveImages is made visible.
function saveImages_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to saveImages (see VARARGIN)

% Choose default command line output for saveImages
handles.output = hObject;

%List of processed images
handles.processedImages = getappdata(0,'processedImages');
handles.indexProcImages = getappdata(0,'indexProcImages');
handles.nProcImages = getappdata(0,'nProcImages');

%Default index (in case we close the window without saving)
setappdata(0,'index',[])

% Modify the text if we want to save a protocol (not the image)
if getappdata(0, 'protocol') == 1
    set (handles.text1, 'String', 'Select the protocol you want to save')
end

% Buttons with picture
arrow = imread('arrowRight.jpg');
arrowSmall=arrow(1:8:end,1:8:end,:);
set(handles.pushbutton_next,'CData',arrowSmall)

arrow2 = imread('arrowLeft.jpg');
arrowSmall2=arrow2(1:8:end,1:8:end,:);
set(handles.pushbutton_previous,'CData',arrowSmall2)

% Show the new processed image            
axes(handles.axes1);
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(handles.text_nIm,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes saveImages wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = saveImages_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'index',handles.indexProcImages)
close

% --- Executes on button press in pushbutton_previous.
function pushbutton_previous_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_previous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.indexProcImages = handles.indexProcImages - 1;

if handles.indexProcImages < 1 
    handles.indexProcImages = handles.nProcImages;
end

% Show the new processed image            
axes(handles.axes1);
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(handles.text_nIm,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])

% Update handles structure
guidata(handles.figure1,handles);

% --- Executes on button press in pushbutton_next.
function pushbutton_next_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Index increases
handles.indexProcImages = handles.indexProcImages + 1;
% if the index is the last one, it should be updated as the first one
if handles.indexProcImages > handles.nProcImages
    handles.indexProcImages = 1;
end

% Show the new processed image            
axes(handles.axes1);
%[bins,~] = imhist(uint16(handles.processedImages(:,:,handles.indexProcImages)),2^16);
[minIm, maxIm] = imageRange(uint16(handles.processedImages(:,:,handles.indexProcImages)));
imshow(handles.processedImages(:,:,handles.indexProcImages),[minIm maxIm])
set(handles.text_nIm,'String', [num2str(handles.indexProcImages) '/' num2str(handles.nProcImages)])

% Update handles structure
guidata(handles.figure1,handles);
