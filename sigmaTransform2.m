% SIGMA TRANSFORM
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% curve: degree of sigma curvature
% level: level (position of the center of the sigma)
%...................................................................................................................
% OUTPUT PARAMETERS
% equalizedImage: image with better contrast
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function equalizedImage = sigmaTransform(image, curve, level, minIm, maxIm, lowOriginal)

% Image information
lowOriginal = double(lowOriginal);
image = image - lowOriginal;
high_in = double(maxIm-lowOriginal);
low_in = round(minIm + level*(maxIm - minIm));
low_in = double(low_in-lowOriginal);
[x,y] = size(image);

% Create the SIGMA LUT
LUT1 = (0:low_in);
inRange = (low_in+1:high_in)/(high_in-low_in);
LUT2 = sigmf(inRange,[1+(9*curve) level])*high_in;
% LUT_rec = LUT2(low_in+1:end);
% LUT_rect = LUT_rec - min(LUT_rec);
LUT_rect = LUT2 - min(LUT2);
LUT_rectNorm = LUT_rect/max(LUT_rect);
LUT3 = LUT_rectNorm*(high_in-max(LUT1));
LUT = [LUT1 LUT3+max(LUT1)];

% Sigma image where we have applied the sigma LUT
equalizedImage = uint16(reshape(LUT(image(:)+1), [x,y]));
equalizedImage = equalizedImage + lowOriginal;

