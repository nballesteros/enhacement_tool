% MORPHOLOGICAL PROCESSING
% Bottom-hat --> this function stands out dark details whose size depends on the s.e.
%...................................................................................................................
% INPUT PARAMETERS
% image: image to be processed
% size: of the structural element
% se: shape of structural element
%...................................................................................................................
% OUTPUT PARAMETERS
% morphologicalImage: image of dark details with a shape that looks like the structural element of a specific size
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function morphologicalImage = bottomhat (image, size, se)

se = strel (se, size); % structural element
morphologicalImage = imbothat (image, se); % bottom-hat
