% Function to read the .txt where the user saved the protocol
%...................................................................................................................
% INPUT PARAMETERS
% savedProtocol: .txt file 
%...................................................................................................................
% OUTPUT PARAMETERS
% protocol:code name with the algorithms and parameters used during the processing
%...................................................................................................................
% In�s Garc�a Barquero: UC3M, HGGM 2016

function protocol = readProtocol (savedProtocol)

% Read the .txt to find the protocol
myCell = textread (savedProtocol, '%s') ;
myString = myCell{end,1};

index = strfind(myString,'(');
protocol = myString(1,index:end);
            
